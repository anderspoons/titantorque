<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Auth::routes();

// Added to routes/api.php, this won't let you make more than 60 requests in a single minute
Route::get('/testrate', function () {
    return 'test';
});

Route::get('/tv/U5AGH4V2rk86ukejwNkf', 'TvController@index');
Route::get('/cron', 'CronController@index');

Route::group(['middleware' => ['auth', 'web', 'active']], function () {

    Route::get('/home', 'HomeController@index');
    Route::get('/', 'HomeController@index');

	//Get's
	Route::get('/file-serve/{dir}/{file}', 'FileController@serve');
	Route::get('/finance', 'FinanceController@index');
	Route::get('/reports', 'ReportController@index');
	Route::get('/bookings', 'BookingController@index');
	Route::get('/bookings/create', 'BookingController@create');
    Route::get('/tv', 'TvController@index');
    Route::get('/jobs/documents/{id}', 'JobController@documents');
    Route::get('/job/stats', 'ReportController@statistics');

    //Select 2 routes
    Route::get('/contact/select2', 'ContactController@select2');
    Route::get('/customer/select2', 'CustomerController@select2');
    Route::get('/rig/select2', 'RigController@select2');
    Route::get('/well/select2', 'WellController@select2');
    Route::get('/inspector/select2', 'InspectorController@select2');
    Route::get('/machine/select2', 'MachineController@select2');
    Route::get('/manager/select2', 'UserController@select2');
    Route::get('/sling/select2', 'SlingController@select2');
    Route::get('/basket/select2', 'BasketController@select2');
    Route::get('/protector/select2', 'ProtectorController@select2');
    Route::get('/status/select2', 'StatusController@select2');
    Route::get('/job/select2', 'JobController@select2');

    //Post's
    Route::post('/reports/generate', 'ReportController@generate');
    Route::post('/job/add_basket', 'JobController@add_basket');
    Route::post('/job/add_sling', 'JobController@add_sling');
    Route::post('/job/remove_sling', 'JobController@remove_sling');
    Route::post('/job/remove_basket', 'JobController@remove_basket');
    Route::post('/job/due_date', 'JobController@due_date');
    Route::post('/job/remove_protector', 'JobController@remove_protector');
    Route::post('/job/cancel', 'JobController@cancelJob');
    Route::post('/job/close', 'JobController@closeJob');

    //	Resources
	Route::resource('customers', 'CustomerController');
	Route::resource('rigs', 'RigController');
	Route::resource('contacts', 'ContactController');
	Route::resource('inspectors', 'InspectorController');
	Route::resource('baskets', 'BasketController');
	Route::resource('slings', 'SlingController');
	Route::resource('statuses', 'StatusController');
    Route::resource('protectors', 'ProtectorController');
    Route::resource('jobs', 'JobController');
    Route::resource('machines', 'MachineController');
    Route::resource('wells', 'WellController');
    Route::resource('options', 'OptionsController');
    Route::resource('bookings', 'BookingController');


});

