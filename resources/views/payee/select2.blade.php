<select name="payee_id" class="form-control" id="payee-select">
    @if(isset($job->payee))
        <option value="{{ $job->payee->id }}">{{ $job->payee->name }}</option>
    @endif
</select>

@push('scripts')
<script type="text/javascript">
    (function (window, $) {
        jQuery(document).ready(function ($) {
            var last_results = [];
            $("#payee-select").select2({
                ajax: {
                    url: "{!! url('customer/select2') !!}",
                    dataType: "json",
                    data: function (term) {
                        return {query: term.term }
                    },
                    results: function (data) {
                        return {
                            results: data.results
                        };
                    }
                },
                createSearchChoice: function (term, data) {
                    if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                        return {
                            id: term,
                            text: term
                        };
                    }
                }
            });
        });
    })(window, jQuery);
</script>
@endpush