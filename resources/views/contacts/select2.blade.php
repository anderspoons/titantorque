<select name="contact_id" class="form-control" id="contact-select">
    @if(isset($job->contact))
        <option value="{{ $job->contact->id }}">{{ $job->contact->name }}</option>
    @endif
</select>

@push('scripts')
<script type="text/javascript">
    (function (window, $) {
        jQuery(document).ready(function ($) {
            var cust_id = '';
            $('body').on('change', '#customer-select', function(e){
                cust_id = $('#customer-select').val();
            });
            $("#contact-select").select2({
                ajax: {
                    url: "{!! url('contact/select2') !!}",
                    dataType: "json",
                    data: function (term) {
                        return {
                            query: term.term,
                            customer_id: cust_id
                        }
                    },
                    results: function (data) {
                        return {
                            results: data.results
                        };
                    }
                },
                createSearchChoice: function (term, data) {
                    if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                        return {
                            id: term,
                            text: term
                        };
                    }
                }
            });

        });
    })(window, jQuery);
</script>
@endpush