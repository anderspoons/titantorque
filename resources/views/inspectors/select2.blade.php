<select name="inspector_id" class="form-control" id="inspector-select">
    @if(isset($job->inspector))
        <option value="{{ $job->inspector->id }}">{{ $job->inspector->name }}</option>
    @endif
</select>

@push('scripts')
<script type="text/javascript">
    (function (window, $) {
        jQuery(document).ready(function ($) {
            $("#inspector-select").select2({
                ajax: {
                    url: "{!! url('inspector/select2') !!}",
                    dataType: "json",
                    data: function (term) {
                        return {
                            query: term.term
                        }
                    },
                    results: function (data) {
                        return {
                            results: data.results
                        };
                    }
                },
                createSearchChoice: function (term, data) {
                    if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                        return {
                            id: term,
                            text: term
                        };
                    }
                }
            });
        });
    })(window, jQuery);
</script>
@endpush