@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Inspector
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($inspector, ['route' => ['inspectors.update', $inspector->id], 'method' => 'patch']) !!}

                        @include('inspectors.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection