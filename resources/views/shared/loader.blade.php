<div id="loader-wrapper">
    <div id="loader"></div>
</div>

@push('styles')
<style type="text/css">
    #loader-wrapper {
        display: none;
        position: absolute;
        z-index: 100000;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.4);
    }

    #loader {
        background-image: url("{{ asset('img/loader.gif') }}");
        position: absolute;
        width: 100px;
        height: 100px;
        top: 50%;
        left: 50%;
        margin-left: -50px; /* margin is -0.5 * dimension */
        margin-top: -50px;
    }    ​
</style>
@endpush


@push('scripts')
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('body').on('click', '.loader-trigger', function (e) {
            console.log('Loader Triggered');
            $('#loader-wrapper').fadeIn();
        });
    });
</script>
@endpush