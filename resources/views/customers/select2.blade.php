<select
        @if(Request::is('wells*'))
        name="directional_id"
        @else
        name="customer_id"
        @endif
        class="form-control" id="customer-select">
    @if(isset($job->customer))
        <option selected value="{{ $job->customer->id }}">{{ $job->customer->name }}</option>
    @endif

    @if(isset($booking->customer_id))
        <option selected value="{{ $booking->customer_id }}"> {{ $booking->customer->name }}</option>
    @endif

    @if(isset($contact->customer_id))
        <option selected value="{{ $contact->customer_id }}"> {{ $contact->customer->name }}</option>
    @endif

    @if(isset($directional))
       <option selected value="{{ $directional->id }}">{{ $directional->name }}</option>
    @endif

</select>


@push('scripts')
<script type="text/javascript">
    (function (window, $) {
        jQuery(document).ready(function ($) {
            var last_results = [];
            $("#customer-select").select2({
                ajax: {
                    url: "{!! url('customer/select2') !!}",
                    dataType: "json",
                    data: function (term) {
                        return {query: term.term }
                    },
                    results: function (data) {
                        return {
                            results: data.results
                        };
                    }
                },
                createSearchChoice: function (term, data) {
                    if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                        return {
                            id: term,
                            text: term
                        };
                    }
                }
            });
        });
    })(window, jQuery);
</script>
@endpush