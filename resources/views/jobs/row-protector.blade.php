@foreach($job->protectors as $protector)
    <div class="form-group clearfix">
        {{ Form::hidden('protector[][id]', $protector->id ) }}

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('basket[][identifier]', 'Identifier') }}
            {{ Form::hidden('basket[][identifier]', $protector->identifier) }}
            {{ Form::text('v', $protector->identifier, array('class' => 'form-control', 'readonly' => 'readonly')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('basket[][sizing]', 'Sizing') }}
            {{ Form::hidden('basket[][sizing]', $protector->sizing) }}
            {{ Form::text('v', $protector->sizing, array('class' => 'form-control', 'readonly' => 'readonly')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('protector[][days_out]', 'Days Out:') }}
            {{ Form::number('protector[][days_out]', $protector->pivot->days_out, array('class' => 'form-control')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('protector[][date_out]', 'Date Out:') }}
            @if($protector->pivot->date_out != null)
                {{ Form::date('protector[][date_out]', Carbon\Carbon::parse($protector->pivot->date_out)->format('Y-m-d'), array('class' => 'form-control')) }}
            @else
                {{ Form::date('protector[][date_out]', null, array('class' => 'form-control')) }}
            @endif
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('protector[][date_in]', 'Date In:') }}
            @if($protector->pivot->date_in != null)
                {{ Form::date('protector[][date_in]', Carbon\Carbon::parse($protector->pivot->date_in)->format('Y-m-d'), array('class' => 'form-control')) }}
            @else
                {{ Form::date('protector[][date_in]', null, array('class' => 'form-control')) }}
            @endif
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('', 'Remove:') }}
            <br/>
            {{ Form::button('<i class="fa fa-times"></i>', array('class' => 'remove-protector btn btn-sm btn-primary')) }}
        </div>
    </div>
@endforeach