@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Job
        </h1>
    </section>
    <div class="content">


        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#job-info" data-toggle="tab" aria-expanded="true">Information</a></li>
                <li class=""><a href="#basket-sling" data-toggle="tab" aria-expanded="false">Baskets & Slings</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="job-info">
                    @include('jobs.show_fields')
                </div>

                <div class="tab-pane" id="basket-sling">
                    @include('jobs.show_basket_slings')
                </div>
            </div>
        </div>



        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">

                    <a href="{!! route('jobs.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
