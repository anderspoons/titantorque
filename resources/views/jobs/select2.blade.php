<select name="job_id" class="form-control" id="job-select">
</select>

@push('scripts')
<script type="text/javascript">
    (function (window, $) {
        jQuery(document).ready(function ($) {
            init_slings();

            function init_slings(){
                $("#job-select").select2({
                    ajax: {
                        url: "{!! url('job/select2') !!}",
                        dataType: "json",
                        data: function (term) {
                            return {query: term.term }
                        },
                        results: function (data) {
                            return {
                                results: data.results
                            };
                        }
                    },
                    createSearchChoice: function (term, data) {
                        if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                            return {
                                id: term,
                                text: term
                            };
                        }
                    },
                    width: '100%',
                });
            }
        });
    })(window, jQuery);
</script>
@endpush