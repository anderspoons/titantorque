@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Job
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')

        {!! Form::open(['route' => 'jobs.store']) !!}

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#job-info" data-toggle="tab" aria-expanded="true">Information</a></li>
                <li class=""><a href="#work-scope" data-toggle="tab" aria-expanded="false">Work Scope</a></li>
                <li class=""><a href="#basket-sling" data-toggle="tab" aria-expanded="false">Baskets &amp; Slings</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="job-info">
                    @include('jobs.create_information')
                </div>

                <div class="tab-pane" id="work-scope">
                    @include('jobs.create_work_scope')
                </div>

                <div class="tab-pane" id="basket-sling">
                    @include('jobs.create_basket_sling')
                </div>
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="margin-left: 10px; margin-right: 10px;">
                    <!-- Submit Field -->
                    <div class="pull-left">
                        {!! Form::submit('Add', ['class' => 'loader-trigger btn btn-primary']) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="pull-right">
                        {!! Form::open(['url' => 'job/cancel', $job_number]) !!}
                        {!! Form::hidden('number_torque', $job_number) !!}
                        {!! Form::submit('Cancel', ['class' => 'btn btn-default']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('shared.loader')
@endsection
