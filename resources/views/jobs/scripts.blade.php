@push('scripts')
<script type="text/javascript">
    (function (window, $) {

        if($("#dataTableBuilder").length > 0){
            window.LaravelDataTables = window.LaravelDataTables || {};
            window.LaravelDataTables["dataTableBuilder"] = $("#dataTableBuilder").DataTable({
                "pageLength": 50,
                "serverSide": true,
                "processing": true,
                "ajax": {
                    url: '/jobs',
                    dataType: 'json',
                    cache:false,
                    type: 'GET',
                    data: function (d) {
                        if(debug){
                            console.log(anyFilters);
                        }
                        $.each( anyFilters, function( key, value ) {
                            d[key] = value;
                        });
                    }
                },
                "columns": [{
                    "name": "number_torque",
                    "data": "number_torque",
                    "title": "Number Torque",
                    "orderable": true,
                    "searchable": true
                }, {
                    "name": "customer.name",
                    "data": "customer.name",
                    "defaultContent": " ",
                    "searchable": false,
                    "title": "Customer",
                    "orderable": true
                }, {
                    "name": "rig.name",
                    "data": "rig.name",
                    "defaultContent": " ",
                    "searchable": false,
                    "title": "Rig",
                    "orderable": true
                }, {
                    "name": "assembly",
                    "data": "assembly",
                    "title": "Assembly",
                    "orderable": true,
                    "searchable": true
                }, {
                    "name": "well.name",
                    "data": "well.name",
                    "defaultContent": " ",
                    "searchable": false,
                    "title": "Well",
                    "orderable": true
                }, {
                    "name": "status",
                    "data": "status",
                    "defaultContent": " ",
                    "searchable": false,
                    "title": "Status",
                    "orderable": false
                }, {
                    "name": "date_format",
                    "data": "date_format",
                    "orderable": true,
                    "searchable": false,
                    "title": "Due Date"
                }, {
                    "name": "note",
                    "data": "note",
                    "title": "Note",
                    "orderable": false,
                    "searchable": true
                }, {
                    "defaultContent": "",
                    "data": "action",
                    "name": "action",
                    "title": "Action",
                    "render": null,
                    "orderable": false,
                    "searchable": false,
                    "width": "10%"
                }],
                "dom": "Bfrtip",
                "scrollX": false,
                "buttons": ["print", "reset", "reload", {
                    "extend": "collection",
                    "text": "<i class=\"fa fa-download\"><\/i> Export",
                    "buttons": ["csv", "excel", "pdf"]
                }, "colvis"],
                "rowCallback": function( row, data ) {
                    $(row).css('background-color', data.colour);
                }
            });

        }

        var debug = true;
        var anyFilters = {};

        $('body').on('click', '.filter-expand', function (e) {
            if (debug) {
                console.log('expand filters clicked ken min');
            }
            var btn = $(this);
            expandFilters(btn, 'toggle');
        });

        function expandFilters(btn, open) {
            if (open == 'toggle') {
                if (btn.children('i').hasClass('glyphicon-plus')) {
                    btn.parent().parent().find('ul').slideDown('fast', function (e) {
                        btn.children('i').removeClass('glyphicon-plus');
                        btn.children('i').addClass('glyphicon-minus');
                    });
                } else {
                    btn.parent().parent().find('ul').slideUp('fast', function (e) {
                        btn.children('i').removeClass('glyphicon-minus');
                        btn.children('i').addClass('glyphicon-plus');
                    });
                }
            } else {
                if (btn.children('i').hasClass('glyphicon-plus')) {
                    btn.parent().parent().find('ul').slideDown('fast', function (e) {
                        btn.children('i').removeClass('glyphicon-plus');
                        btn.children('i').addClass('glyphicon-minus');
                    });
                }
            }
        }

        $('body').on('click', '.filter-clear', function (e) {
            if (debug) {
                console.log('Clear filters clicked ken min');
            }
            var btn = $(this);
            $(this).parent().parent().find('ul').each(function (i) {
                $(this).find('input').each(function (i) {
                    $(this).attr('checked', false);
                });
            })
            $('.search-filters').each(function (e) {
                $(this).val('');
                searchText($(this));
            })
            btn.addClass('hidden');
            doFilters();
        });

        $('body').on('change', '.filter-set input[type="checkbox"]', function (e) {
            if (debug) {
                console.log('OMG min! a filters been checked ... ken fit lyk.');
            }
            doFilters();
        });

        $('body').on('click', '.buttons-reset', function (e) {
            anyFilters = {};
            $('.filter-set ul').each(function (i) {
                $(this).find('input:checked').each(function (i) {
                    $(this).attr('checked', false);
                });
            })
            $('.buttons-reload').click();
        });

        $('body').on('click', '.filter', function (e) {
            setTimeout(function () {
                $('a[data-dt-idx="1"]').click().delay(800);
            }, 1000);
            $('.buttons-reload').click();
        });

        $('body').on('click', '.buttons-reset', function (e) {
            anyFilters = {};
            $('.filter-set ul').each(function (i) {
                $(this).find('input:checked').each(function (i) {
                    $(this).attr('checked', false);
                });
            })
            $('.buttons-reload').click();
        });

        $('body').on('keyup', '.search-filters', function (e) {
            searchText($(this));
        });

        function searchText(input) {
            var term = input.val();
            var btn = input.parent().parent().find('.filter-expand');
            if (term.length > 0) {
                expandFilters(btn, 'open');
            }
            if (debug) {
                console.log('Filter search text: ' + term + ' length: ' + term.length);
            }
            input.parent().parent().parent().find('ul li').each(function (i) {
                if ($(this).hasClass('hidden')) {
                    $(this).removeClass('hidden');
                }
                var text = $(this).text();
                if (text.toLowerCase().search(term.toLowerCase()) < 0 && $(this).find('input').prop('checked') == false) {
                    $(this).addClass('hidden');
                }
            })
        }

        function doFilters() {
            anyFilters = {};
            $('.filter-set ul').each(function (i) {
                var tmp = [];
                var name = '';
                $(this).find('input:checked').each(function (i) {
                    if ($(this).parent().parent().parent().find('.filter-clear').hasClass('hidden')) {
                        $(this).parent().parent().parent().find('.filter-clear').removeClass('hidden');
                    }
                    name = $(this).attr('name');
                    tmp.push($(this).val());
                });
                if ($(this).find('input:checked').length < 1) {
                    $(this).parent().find('.filter-clear').addClass('hidden')
                }
                if (tmp.length > 0) {
                    anyFilters[name] = tmp;
                }
            })
            if (debug) {
                console.log(anyFilters);
            }
        }


        index_baskets();
        index_protectors();

        function index_baskets() {
            var baskets = $('#job-baskets .form-group').length;

            var count = 0;
            $('#job-baskets > .form-group').each(function () {
                $(this).find('input').each(function () {
                    $(this).attr('name', $(this).attr('name').replace('[][', '[' + count + ']['));
                })
                count++;
            });
        }

        function index_protectors() {
            var protectors = $('#job-protectors .form-group').length;

            var count = 0;
            $('#job-protectors > .form-group').each(function () {
                $(this).find('input').each(function () {
                    $(this).attr('name', $(this).attr('name').replace('[][', '[' + count + ']['));
                })
                count++;
            });
        }


        $('.add-basket-confirm').click(function (e) {
            e.preventDefault();
            console.log('Adding Basket');
            //collect info from modal
            var data = $('#basket-modal #basket-select').select2('data');
            data = data[0];
            console.log(data);
            var id = data.id;
            var length = data.basket_length;
            var asset = data.asset;
            var note = $('#basket-modal #basket-note').val();

            var row = $('.basket-template .form-group').first().clone();
            $(row).find('.id-basket').val(id);
            $(row).find('.asset-basket').val(asset);
            $(row).find('.length-basket').val(length);
            $(row).find('.note-basket').val(note);

            $('#job-baskets').append(row);

            $('#basket-modal .close').click();
            index_baskets();
        });

        $('.add-sling-confirm').click(function (e) {
            e.preventDefault();
            console.log('Adding Sling');
            //collect info from modal
            var data = $('#sling-modal #sling-select').select2('data');
            data = data[0];
            var id = data.id;
            var capacity = data.capacity;
            var serial = data.serial;
            var quantity = data.quantity;
            var user_quantity = $('#sling-modal #sling-quantity').val();

            if (user_quantity > quantity) {
                $('#sling-modal .sling-warning').removeClass('hidden');
                $('#sling-modal .sling-warning').html('Quantity entered is invalid.');
            } else {
                $('#sling-modal .loader').removeClass('hidden');

                $.ajax({
                    type: "POST",
                    url: '/job/add_sling',
                    data: {
                        id: id,
                        job_id: $('#job-id').val(),
                        quantity: user_quantity
                    },
                    dataType: JSON
                }).always(function(response) {
                    var data = $.parseJSON(response.responseText);
                    if(data.success == true){
                        $('#sling-modal .loader').addClass('hidden');
                        $('#sling-modal .sling-warning').addClass('hidden');

                        var row = $('.sling-template .form-group').first().clone();
                        $(row).find('.id-sling').val(id);
                        $(row).find('.serial-sling').val(serial);
                        $(row).find('.quantity-sling').val(user_quantity);
                        $(row).find('.capacity-sling').val(capacity);
                        $('#job-slings').append(row);
                        $('#sling-modal .close').click();
                        $('#sling-modal .close').click();
                    } else {
                        $('#sling-modal .sling-warning').removeClass('hidden');
                        $('#sling-modal .sling-warning').html(data.message);
                    }
                });
            }
        });

        $('.add-protector-confirm').click(function (e) {
            e.preventDefault();
            console.log('Adding Protector');
            //collect info from modal
            var data = $('#protector-modal #protector-select').select2('data');
            data = data[0];
            var id = data.id;
            var sizing = data.sizing;
            var identifier = data.identifier;

            $('#protector-modal .protector-warning').addClass('hidden');
            var row = $('.protector-template .form-group').first().clone();
            $(row).find('.id-protector').val(id);
            $(row).find('.identifier-protector').val(identifier);
            $(row).find('.sizing-protector').val(sizing);
            $('#job-protectors').append(row);

            $('#protector-modal .close').click();
            index_protectors();
        });

        $('body').on('click', '.remove-sling', function(e){
            var id = $(this).parent().parent().find('.id-sling').val();
            var button = $(this);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })

            $.ajax({
                type: "POST",
                url: '/job/remove_sling',
                data: {
                    id: id,
                    job_id: $('#job-id').val()
                },
                dataType: JSON
            }).always(function(response) {
                var data = $.parseJSON(response.responseText);
                if(data.success == true){
                    $(button).parent().parent().remove();
                } else {
                    alert('Issue removing sling please leave this window open and contact boxportable and ask for Sam, as this should never happen.')
                }
            });
        });

        $('body').on('click', '.remove-protector', function(e){
            var id = $(this).parent().parent().find('.id-protector').val();
            var button = $(this);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                type: "POST",
                url: '/job/remove_protector',
                data: {
                    id: id,
                    job_id: $('#job-id').val()
                },
                dataType: JSON
            }).always(function(response) {
                var data = $.parseJSON(response.responseText);
                if(data.success == true){
                    $(button).parent().parent().remove();
                } else {
                    alert('Issue removing protector please leave this window open and contact boxportable and ask for Sam, as this should never happen.')
                }
            });
        });

        $('body').on('click', '.remove-basket', function(e){
            var id = $(this).parent().parent().find('.id-basket').val();
            var button = $(this);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                type: "POST",
                url: '/job/remove_basket',
                data: {
                    id: id,
                    job_id: $('#job-id').val()
                },
                dataType: JSON
            }).always(function(response) {
                var data = $.parseJSON(response.responseText);
                if(data.success == true){
                    $(button).parent().parent().remove();
                } else {
                    alert('Issue removing basket please leave this window open and contact boxportable and ask for Sam, as this should never happen.')
                }
            });
        });

        $('body').on('click', '.remove-row', function(e){
            console.log('Removing row');
            $(this).parent().parent().remove();
            index_baskets();
            index_protectors();
        });

        $("body").on("click", ".due_date_datepicker", function(){
            var $value = $(this).text();
            var $job_id = +$(this).attr("id").split("_")[1];
            $(this).replaceWith("<input type=\"text\" name=\"due_date_"+$job_id+"\" id=\"due_date_"+$job_id+"\" class=\"\" value=\""+$value+"\">").datetimepicker('show');
            var $dateinput = $("#due_date_"+$job_id);
            $dateinput.datetimepicker({
                format: 'd/m/Y',
                timepicker: false,
                todayButton: true,
                closeOnDateSelect: true,
                // closeOnWithoutClick: false,
                onClose:function(dp,$i){
                    console.log(dp,$i);
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: '/job/due_date',
                        data: {
                            due_date: $dateinput.val(),
                            job_id: $job_id
                        },
                        dataType: JSON
                    }).always(function(response) {
                        var data = $.parseJSON(response.responseText);
                        console.log(data);
                        if(data.success == true){
                            // alert("Due date successfully updated");
                            $dateinput.replaceWith("<span class=\"due_date_datepicker\" id=\"due_date_"+$job_id+"\">"+$dateinput.val()+"</span>");
                        } else {
                            alert('Error updating due date')
                        }
                    });
                    $dateinput.datetimepicker("destroy");
                }
            });
            $dateinput.datetimepicker('show');            

        });


        $("body").on("click", ".jobs-close-job-button", function(e){
            e.preventDefault();
            var $sure = confirm("Are you sure you want to close this job?");
            var $job_id = $(this).data("job-id");
            console.log($sure, $job_id);
            if ($sure) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: '/job/close',
                    data: {
                        job_id: $job_id
                    },
                    dataType: JSON
                }).always(function(response) {
                    var data = $.parseJSON(response.responseText);
                    console.log(data);
                    if(!data.success){
                        alert('Error closing job ID:'+$job_id)
                    } else {
                        // alert("SUCCESS");
                        $('.buttons-reload').click();
                    }
                });
            }

        });

    })(window, jQuery);
</script>
@endpush