@foreach($job->baskets as $basket)
    <div class="form-group clearfix">
        {{ Form::hidden('basket[][id]', $basket->id ) }}
        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('basket[][asset]', 'Asset Number:') }}
            {{ Form::text('basket[][asset]', $basket->asset_num, array('class' => 'form-control', 'readonly' => 'readonly')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('basket[][length]', 'Length:') }}
            {{ Form::text('basket[][length]', $basket->length, array('class' => 'form-control', 'readonly' => 'readonly')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('basket[][note]', 'Note:') }}

            @if($basket->pivot->note != null)
                {{ Form::text('basket[][note]', $basket->pivot->note, array('class' => 'form-control')) }}
            @else
                {{ Form::text('basket[][note]', '', array('class' => 'form-control')) }}
            @endif
            </div>

            <div class="form-group col-sm-12 col-md-4">
                {{ Form::label('basket[][days_out]', 'Days Out:') }}
            @if($basket->pivot->days_out != null)
                {{ Form::number('basket[][days_out]', $basket->pivot->days_out, array('class' => 'form-control')) }}
            @else
                {{ Form::number('basket[][days_out]', '0', array('class' => 'form-control')) }}
            @endif
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('basket[][date_out]', 'Date Out:') }}
            @if($basket->pivot->date_out != null)
                {{ Form::date('basket[][date_out]', Carbon\Carbon::parse($basket->pivot->date_out)->format('Y-m-d'), array('class' => 'form-control')) }}
            @else
                {{ Form::date('basket[][date_out]', null, array('class' => 'form-control')) }}
            @endif
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('basket[][date_in]', 'Date In:') }}
            @if($basket->pivot->date_in != null)
                {{ Form::date('basket[][date_in]', Carbon\Carbon::parse($basket->pivot->date_in)->format('Y-m-d'), array('class' => 'form-control')) }}
            @else
                {{ Form::date('basket[][date_in]', null, array('class' => 'form-control')) }}
            @endif
        </div>

        <div class="form-group col-sm-6 col-md-4">
            @if($basket->pivot->loaded == 1)
                {{ Form::checkbox('basket[][loaded]', '1', true, ['class' => 'icheck']) }}
            @else
                {{ Form::checkbox('basket[][loaded]', '0', false, ['class' => 'icheck']) }}
            @endif
            {{ Form::label(' Loaded?') }}
        </div>

        <div class="form-group col-sm-6 col-md-4">
            @if($basket->pivot->unloaded == 1)
                {{ Form::checkbox('basket[][unloaded]', '1', true, ['class' => 'icheck']) }}
            @else
                {{ Form::checkbox('basket[][unloaded]', '0', false, ['class' => 'icheck']) }}
            @endif
            {{ Form::label(' Unloaded?') }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('', '&nbsp;') }}
            <br/>
            {{ Form::button('<i class="fa fa-times"></i>', array('class' => 'remove-row btn btn-sm btn-primary add-basket-btn')) }}
        </div>
    </div>
@endforeach