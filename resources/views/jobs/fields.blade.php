<!-- Number Torque Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('number_torque', 'Titan Torque Number:') !!}

    {!! Form::hidden('number_torque', $job->number_torque) !!}
    {!! Form::text('', $job->number_torque, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
</div><!-- Number Torque Field -->

<!-- Customer Id Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('manager_id', 'Project Manager:') !!}
    @include('users.select2')
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('customer_id', 'Customer:') !!}
    @include('customers.select2')
</div>

<!-- Contact Id Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('contact_id', 'Contact:') !!}
    @include('contacts.select2')
</div>

<!-- Operator Id Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('operator_id', 'Operator:') !!}
    @include('operators.select2')
</div>

<!-- Rig Id Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('rig_id', 'Rig:') !!}
    @include('rigs.select2')
</div>

<!-- Well Id Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('well_id', 'Well:') !!}
    @include('wells.select2')
</div>

<!-- Inspector Id Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('inspector_id', 'Inspector:') !!}
    @include('inspectors.select2')
</div>

<!-- Po Number Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('po_number', 'Po Number:') !!}
    {!! Form::text('po_number', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('quote_number', 'Titan Torque Quote Number:') !!}

    @if(isset($job->quote_number))
        {!! Form::hidden('quote_number', $job->quote_number) !!}
        {!! Form::text('quote_number', $job->quote_number, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
    @else
        {!! Form::text('quote_number', null, ['class' => 'form-control']) !!}
    @endif
</div>

<!-- Number Tools Field -->
<div class="form-group col-sm-12  col-md-4">
    {!! Form::label('number_tools', 'Titan Tools Number:') !!}
    {!! Form::text('number_tools', null, ['class' => 'form-control']) !!}
</div>

<!-- Due Date Field -->
<div class="form-group col-sm-12  col-md-4">
    {!! Form::label('due_date', 'Due Date:') !!}
    {!! Form::date('due_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Workshop Field -->
<div class="form-group col-sm-12  col-md-4">
    {!! Form::label('hours_workshop', 'Workshop Hours:') !!}
    {!! Form::number('hours_workshop', null, ['class' => 'form-control']) !!}
</div>

<!-- Out of Hours Field -->
<div class="form-group col-sm-12  col-md-4">
    {!! Form::label('hours_out_of', 'Out of Hours:') !!}
    {!! Form::number('hours_out_of', null, ['class' => 'form-control']) !!}
</div>

<!-- Out of Hours Field -->
<div class="form-group col-sm-12  col-md-4">
    {!! Form::label('witnesses', 'Witnesses:') !!}
    {!! Form::text('witnesses', null, ['class' => 'form-control']) !!}
</div>

<!-- Out of Hours Field -->
<div class="form-group col-sm-12  col-md-4">
    {!! Form::label('', 'Status:') !!}<br>
    {!! Form::radio('status[]', 2, '', ['class' => 'icheck','id'=>'statusClosed']) !!} 
    {!! Form::label('statusClosed', 'Closed&nbsp;&nbsp;') !!}
    {!! Form::radio('status[]', 1, '', ['class' => 'icheck','id'=>'statusOpen']) !!}
    {!! Form::label('statusOpen', 'Open') !!}
    {{-- @include('statuses.select2') --}}

</div>

<hr/>

<!-- Cargo Summary -->
<div class="form-group col-sm-12 col-md-4">

    <div class="form-group col-sm-12 col-md-6">
        @if($job->cargo_summary == 1)
            {{ Form::checkbox('cargo_summary', '1', true, ['class' => 'icheck']) }}&nbsp;&nbsp;
        @else
            {{ Form::checkbox('cargo_summary', '1', false, ['class' => 'icheck']) }}&nbsp;&nbsp;
        @endif
        {!! Form::label('cargo_summary', 'Cargo Summary?') !!}
    </div>

    <div class="form-group col-sm-12 col-md-6">
        <select class="form-control" name="cargo_export_temporary">
            <option @if($job->cargo_export_temporary == 1) selected @endif value="1">Temporary</option>
            <option @if($job->cargo_export_temporary == 0) selected @endif value="0">Permanent</option>
        </select>
    </div>

</div>

<!-- Cargo Summary -->
<div class="form-group col-sm-12 col-md-4">
    @if($job->commercial_invoice == 1)
        {{ Form::checkbox('commercial_invoice', '1', true, ['class' => 'icheck']) }}&nbsp;&nbsp;
    @else
        {{ Form::checkbox('commercial_invoice', '1', false, ['class' => 'icheck']) }}&nbsp;&nbsp;
    @endif
    {!! Form::label('commercial_invoice', 'Commercial Invoice?') !!}
</div>

<!-- Cargo Summary -->
<div class="form-group col-sm-12 col-md-4">
    @if($job->email_contact == 1)
        {{ Form::checkbox('email_contact', '1', true, ['class' => 'icheck']) }}&nbsp;&nbsp;
    @else
        {{ Form::checkbox('email_contact', '1', false, ['class' => 'icheck']) }}&nbsp;&nbsp;
    @endif&nbsp;
    {!! Form::label('email_contact', 'Email Contact on Completion?') !!}
</div>


@if(Auth::user()->finance == 1)
    <div class="form-group">
        <hr/>
        <div class="form-group col-sm-12">
            {!! Form::label('', 'Finance:') !!}
        </div>

        <!-- Invoice Number Field -->
        <div class="form-group col-sm-6  col-md-4">
            {!! Form::label('invoice_number', 'Invoice Number:') !!}
            {!! Form::text('invoice_number', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Value Field -->
        <div class="form-group col-sm-6  col-md-4">
            {!! Form::label('value', 'Job Value:') !!}
            {!! Form::text('value', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Payee Id Field -->
        <div class="form-group col-sm-6  col-md-4">
            {!! Form::label('payee_id', 'Payee:') !!}
            @include('payee.select2')
        </div>
    </div>
@endif

<div class="clearfix"></div>