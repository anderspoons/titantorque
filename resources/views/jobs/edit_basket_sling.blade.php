<div class="form-group">
    {{ Form::hidden('', $job->id, array('id' => 'job-id')) }}
    <div class="form-group col-sm-12">
        {!! Form::label('', 'Ratchets & Blocks:') !!}
    </div>
    <!-- Ratchet Used Field -->
    <div class="form-group col-sm-6  col-md-3">
        {!! Form::label('ratchet_used', 'Ratchet Used:') !!}
        {!! Form::number('ratchet_used', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Ratchet Returned Field -->
    <div class="form-group col-sm-6  col-md-3">
        {!! Form::label('ratchet_returned', 'Ratchet Returned:') !!}
        {!! Form::number('ratchet_returned', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Blocks Used Field -->
    <div class="form-group col-sm-6  col-md-3">
        {!! Form::label('blocks_used', 'Blocks Used:') !!}
        {!! Form::number('blocks_used', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Blocks Returned Field -->
    <div class="form-group col-sm-6  col-md-3">
        {!! Form::label('blocks_returned', 'Blocks Returned:') !!}
        {!! Form::number('blocks_returned', null, ['class' => 'form-control']) !!}
    </div>
</div>

<hr/>

<div class="form-group" id="job-baskets">
    <div class="form-group col-sm-12">
        <div class="pull-left">
            {!! Form::label('', 'Baskets:') !!}
        </div>
        <div class="pull-right">
            {{ Form::button('Add', array('class' => 'btn btn-sm btn-primary add-basket-btn', 'data-toggle' => 'modal', 'data-target' => '#basket-modal')) }}
        </div>
    </div>
    <div class="clearfix"></div>


    @include('jobs.row-basket')
</div>

<hr/>

<div class="form-group" id="job-slings">
    <div class="form-group col-sm-12">
        <div class="pull-left">
            {!! Form::label('', 'Slings:') !!}
        </div>
        <div class="pull-right">
            {{ Form::button('Add', array('class' => 'btn btn-sm btn-primary add-sling-btn', 'data-toggle' => 'modal', 'data-target' => '#sling-modal')) }}
        </div>
    </div>
    <div class="clearfix"></div>

    @include('jobs.row-sling')
</div>

<hr/>

<div class="form-group" id="job-protectors">
    <div class="form-group col-sm-12">
        <div class="pull-left">
        {!! Form::label('', 'Protectors:') !!}
        </div>
        <div class="pull-right">
            {{ Form::button('Add', array('class' => 'btn btn-sm btn-primary add-protector-btn', 'data-toggle' => 'modal', 'data-target' => '#protector-modal')) }}
        </div>
    </div>
    <div class="clearfix"></div>


    @include('jobs.row-protector')
</div>

<div class="clearfix"></div>


