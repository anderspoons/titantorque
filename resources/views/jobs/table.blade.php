@section('css')
    @include('layouts.datatables_css')
@endsection

{!! $dataTable->table(['width' => '100%']) !!}

@push('scripts')
    @include('layouts.datatables_js')
    @include('jobs.scripts')
    {{--{!! $dataTable->scripts() !!}--}}
@endpush