@foreach($job->slings as $sling)
    <div class="row clearfix">
        <div class="col-md-12">
            {{ Form::hidden('', $sling->id, array('class' => 'id-sling')) }}
            <div class="form-group col-sm-12 col-md-3">
                {{ Form::label('', 'Serial:') }}
                {{ Form::text('', $sling->serial, array('class' => 'form-control serial-sling', 'readonly' => 'readonly')) }}
            </div>

            <div class="form-group col-sm-12 col-md-3">
                {{ Form::label('', 'Quantity:') }}
                {{ Form::text('', $sling->pivot->quantity, array('class' => 'form-control quantity-sling', 'readonly' => 'readonly')) }}
            </div>

            <div class="form-group col-sm-12 col-md-3">
                {{ Form::label('', 'Capacity:') }}
                {{ Form::text('', $sling->capacity->capacity, array('class' => 'form-control capacity-sling', 'readonly' => 'readonly')) }}
            </div>

            <div class="form-group col-sm-12 col-md-3">
                {{ Form::label('', '&nbsp;') }}
                <br/>
                {{ Form::button('<i class="fa fa-times"></i>', array('class' => 'remove-sling btn btn-sm btn-primary')) }}
            </div>
        </div>
    </div>
@endforeach