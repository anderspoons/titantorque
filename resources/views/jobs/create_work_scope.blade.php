<!-- Assembly Field -->
<div class="form-group col-sm-6  col-md-4">
    {!! Form::label('assembly', 'Assembly:') !!}
    {!! Form::text('assembly', '', ['class' => 'form-control']) !!}
</div>

<!-- Bhas Field -->
<div class="form-group col-sm-6  col-md-4">
    {!! Form::label('bhas', 'Bhas:') !!}
    {!! Form::number('bhas', 0, ['class' => 'form-control']) !!}
</div>

<hr>

<!-- Api Connections -->
<div class="form-group col-sm-12 col-md-4">
    {{ Form::checkbox('api_chck', '1', false, ['class' => 'icheck api-check']) }}&nbsp;&nbsp;
    {!! Form::label('api_chck', 'Api Connections?') !!}
</div>

<!-- Premium Connections -->
<div class="form-group col-sm-12 col-md-4">
    {{ Form::checkbox('premium_chck', '1', false, ['class' => 'icheck premium-check']) }}&nbsp;&nbsp;
    {!! Form::label('premium_chck', 'Premium Connections?') !!}
</div>
<hr>
<div class="clearfix"></div>

{{-- API Shite --}}
<div class="hidden api-connections">

    <!-- Makes Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('makes_api', 'API Makes:') !!}
        {!! Form::number('makes_api', 0, ['class' => 'form-control']) !!}
    </div>

    <!-- Breaks Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('breaks_api', 'API Breaks:') !!}
        {!! Form::number('breaks_api', 0, ['class' => 'form-control']) !!}
    </div>

    <hr>
    <div class="clearfix"></div>
</div>

{{-- Premium Shite --}}
<div class="hidden premium-connections">
    <!-- Makes Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('makes_premium', 'Premium Makes:') !!}
        {!! Form::number('makes_premium', 0, ['class' => 'form-control']) !!}
    </div>

    <!-- Breaks Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('breaks_premium', 'Premium Breaks:') !!}
        {!! Form::number('breaks_premium', 0, ['class' => 'form-control']) !!}
    </div>

    <!-- Stenciled Connections Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('stenciled_connections', 'Stenciled Connections:') !!}
        {!! Form::number('stenciled_connections', 0, ['class' => 'form-control']) !!}
    </div>

    <!-- Bakerlocked Connections Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('bakerlocked_connections', 'Bakerlocked Connections:') !!}
        {!! Form::number('bakerlocked_connections', 0, ['class' => 'form-control']) !!}
    </div>

    <!-- Stenciled Connections Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('drift', 'Drift:') !!}
        {!! Form::number('drift', 0, ['class' => 'form-control']) !!}
    </div>

    <!-- Centralizers Fitted Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('centralizers_fitted', 'Centralizers Fitted:') !!}
        {!! Form::number('centralizers_fitted', 0, ['class' => 'form-control']) !!}
    </div>

    <hr>
    <div class="clearfix"></div>
</div>

<!-- Note Field -->
<div class="form-group col-sm-6  col-md-4">
    {!! Form::label('note', 'Note:') !!}
    {!! Form::text('note', '', ['class' => 'form-control']) !!}
</div>

<!-- Insertion Field -->
<div class="form-group col-sm-6  col-md-4">
    {!! Form::label('insertion', 'Insertion:') !!}
    {!! Form::number('insertion', 0, ['class' => 'form-control']) !!}
</div>

<!-- Extraction Field -->
<div class="form-group col-sm-6  col-md-4">
    {!! Form::label('extraction', 'Extraction:') !!}
    {!! Form::number('extraction', 0, ['class' => 'form-control']) !!}
</div>

<div class="clearfix"></div>