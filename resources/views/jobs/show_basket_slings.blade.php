
<!-- Ratchet Used Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('ratchet_used', 'Ratchet Used:') !!}
    <p>{!! $job->ratchet_used !!}</p>
</div>

<!-- Ratchet Returned Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('ratchet_returned', 'Ratchet Returned:') !!}
    <p>{!! $job->ratchet_returned !!}</p>
</div>

<!-- Blocks Used Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('blocks_used', 'Blocks Used:') !!}
    <p>{!! $job->blocks_used !!}</p>
</div>

<!-- Blocks Returned Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('blocks_returned', 'Blocks Returned:') !!}
    <p>{!! $job->blocks_returned !!}</p>
</div>

<!-- Basket Loaded Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('basket_loaded', 'Baskets Loaded:') !!}
    <p>{!! $job->basket_loaded !!}</p>
</div>

<!-- Basket Unloaded Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('basket_unloaded', 'Baskets Unloaded:') !!}
    <p>{!! $job->basket_unloaded !!}</p>
</div>

<div class="clearfix"></div>
