@if(isset($customers))
    <div class="filter-set">
        <div>
            <div class="pull-left" style="padding:0 !important;">
                {!! Form::text('customer', '', array('class' => 'search-filters', 'placeholder' => 'Customer')) !!}
            </div>
            <button style="right:20px; width:auto;" class="btn btn-xs pull-left filter-clear hidden">Clear</button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($customers as $customer)
                <li>
                    {{ Form::checkbox('customer_filter', $customer->id, false, ["id" => "customer_".$customer->id]) }}
                    {{ Form::label("customer_".$customer->id, $customer->name) }}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($inspectors))
    <div class="filter-set">
        <div class="header-bar">
            <div class="pull-left" style="padding:0 !important;">
                {!! Form::text('inspector', '', array('class' => 'search-filters', 'placeholder' => 'Inspector')) !!}
            </div>
            <button style="right:20px; width:auto;" class="btn btn-xs pull-left filter-clear hidden">Clear</button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($inspectors as $inspector)
                <li>
                    {{ Form::checkbox('inspector_filter', $inspector->id, false, ["id" => "inspector_".$inspector->id]) }}
                    {{ Form::label("inspector_".$inspector->id, $inspector->name) }}
                    {{-- {!! $inspector->name !!} --}}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($wells))
    <div class="filter-set">
        <div class="header-bar">
            <div class="pull-left" style="padding:0 !important;">
                {!! Form::text('well', '', array('class' => 'search-filters', 'placeholder' => 'Well')) !!}
            </div>
            <button style="right:20px; width:auto;" class="btn btn-xs pull-left filter-clear hidden">Clear</button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($wells as $well)
                <li>
                    {!! Form::checkbox('well_filter', $well->id, false, ["id"=>"well_".$well->id]) !!}
                    {{ Form::label("well_".$well->id, $well->name) }}
                    {{-- {!! $well->name !!} --}}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($rigs))
    <div class="filter-set">
        <div class="header-bar">
            <div class="pull-left" style="padding:0 !important;">
                {!! Form::text('rig', '', array('class' => 'search-filters', 'placeholder' => 'Rig')) !!}
            </div>
            <button style="right:20px; width:auto;" class="btn btn-xs pull-left filter-clear hidden">Clear</button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($rigs as $rig)
                <li>
                    {!! Form::checkbox('rig_filter', $rig->id, false,["id"=>"rig_".$rig->id]) !!}
                    {{ Form::label("rig_".$rig->id, $rig->name) }}
                    {{-- {!! $rig->name !!} --}}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($contacts))
    <div class="filter-set">
        <div class="header-bar">
            <div class="pull-left" style="padding:0 !important;">
                {!! Form::text('contact', '', array('class' => 'search-filters', 'placeholder' => 'Contact')) !!}
            </div>
            <button style="right:20px; width:auto;" class="btn btn-xs pull-left filter-clear hidden">Clear</button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($contacts as $contact)
                <li>
                    {!! Form::checkbox('contact_filter', $contact->id, false, ["id"=>"contact_".$contact->id]) !!}
                    {{ Form::label("contact_".$contact->id, $contact->name) }}
                    {{-- {!! $contact->name !!} --}}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($statuses))
    <div class="filter-set">
        <div class="header-bar">
            <div class="pull-left" style="padding:0 !important;">
                {!! Form::text('status', '', array('class' => 'search-filters', 'placeholder' => 'Status')) !!}
            </div>
            <button style="right:20px; width:auto;" class="btn btn-xs pull-left filter-clear hidden">Clear</button>
            <button class="btn btn-xs pull-right filter-expand"><i class="glyphicon glyphicon-plus"></i></button>
        </div>
        <div class="clearfix"></div>
        <ul>
            @foreach($statuses as $status)
                <li>
                    {!! Form::checkbox('status_filter', $status->id, false, ["id"=>"status_".$status->id]) !!}
                    {{ Form::label("status_".$status->id, $status->name) }}
                    {{-- {!! $status->name !!} --}}
                </li>
            @endforeach
        </ul>
    </div>
@endif

@if(isset($types) || isset($sizes) || isset($sexes) || isset($grades) || isset($materials) || isset($suppliers) || isset($statuses) || isset($stores))
    <br/>
    <div class="clearfix"></div>
    {{ Form::button('Filter', array('class' => 'col-md-12 filter')) }}
    {{ Form::button('Reset Filters', array('class' => 'col-md-12 buttons-reset', 'style' => 'margin-top:10px;')) }}
@endif



