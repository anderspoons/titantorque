
<div class="form-group col-sm-12">
    {!! Form::label('', 'Ratchets & Blocks:') !!}
</div>

<!-- Ratchet Used Field -->
<div class="form-group col-sm-6  col-md-3">
    {!! Form::label('ratchet_used', 'Ratchet Used:') !!}
    {!! Form::number('ratchet_used', '0', ['class' => 'form-control']) !!}
</div>

<!-- Ratchet Returned Field -->
<div class="form-group col-sm-6  col-md-3">
    {!! Form::label('ratchet_returned', 'Ratchet Returned:') !!}
    {!! Form::number('ratchet_returned', '0', ['class' => 'form-control']) !!}
</div>

<!-- Blocks Used Field -->
<div class="form-group col-sm-6  col-md-3">
    {!! Form::label('blocks_used', 'Blocks Used:') !!}
    {!! Form::number('blocks_used', '0', ['class' => 'form-control']) !!}
</div>

<!-- Blocks Returned Field -->
<div class="form-group col-sm-6  col-md-3">
    {!! Form::label('blocks_returned', 'Blocks Returned:') !!}
    {!! Form::number('blocks_returned', '0', ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-12">
    {!! Form::label('', 'You can add Baskets, Slings, Protectors only once a job has been added.', array('style' => 'color:#AA3939;')) !!}
</div>

<div class="clearfix"></div>