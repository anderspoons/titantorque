


<!-- Number Torque Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('number_torque', 'Job Number:') !!}
    <p>{!! $job->number_torque !!}</p>
</div>

<!-- Number Tools Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('number_tools', 'Tools Number:') !!}
    <p>{!! $job->number_tools !!}</p>
</div>

<!-- Due Date Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('due_date', 'Due Date:') !!}
    <p>{!! $job->due_date !!}</p>
</div>


@if($job->imported == 1)
    <hr/>
    <div class="form-group col-sm-12 col-md-12">
        {!! Form::label('imported', 'Imported from Old System, Obsolete fields:') !!}
    </div>

    <!-- Custom Connections Field -->
    <div class="form-group col-sm-12 col-md-4">
        {!! Form::label('custom_connections', 'Custom Connections:') !!}
        <p>{!! $job->custom_connections !!}</p>
    </div>

    <!-- Basket Note Field -->
    <div class="form-group col-sm-12 col-md-4">
        {!! Form::label('basket_note', 'Basket Note:') !!}
        <p>{!! $job->basket_note !!}</p>
    </div>
    <!-- Make Break Custom Field -->
    <div class="form-group col-sm-12 col-md-4">
        {!! Form::label('make_break_custom', 'Make Break Custom:') !!}
        <p>{!! $job->make_break_custom !!}</p>
    </div>
    <hr/>
@endif

<!-- Assembly Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('assembly', 'Assembly:') !!}
    <p>{!! $job->assembly !!}</p>
</div>

<!-- Quote Number Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('quote_number', 'Quote Number:') !!}
    <p>{!! $job->quote_number !!}</p>
</div>

<!-- Po Number Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('po_number', 'Po Number:') !!}
    <p>{!! $job->po_number !!}</p>
</div>

<hr/>


<!-- API Connections Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('api_connections', 'API Connections:') !!}
    <p>{!! $job->api_connections !!}</p>
</div>

<!-- Premium Connections Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('premium_connections', 'Premium Connections:') !!}
    <p>{!! $job->premium_connections !!}</p>
</div>

<!-- Stenciled Connections Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('stenciled_connections', 'Stenciled Connections:') !!}
    <p>{!! $job->stenciled_connections !!}</p>
</div>

<!-- Bakerlocked Connections Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('bakerlocked_connections', 'Bakerlocked Connections:') !!}
    <p>{!! $job->bakerlocked_connections !!}</p>
</div>

<!-- Centralizers Fitted Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('centralizers_fitted', 'Centralizers Fitted:') !!}
    <p>{!! $job->centralizers_fitted !!}</p>
</div>


<!-- Updated At Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $job->updated_at !!}</p>
</div>

<!-- Note Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('note', 'Note:') !!}
    <p>{!! $job->note !!}</p>
</div>


<!-- Makes Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('makes', 'Makes:') !!}
    <p>{!! $job->makes !!}</p>
</div>

<!-- Breaks Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('breaks', 'Breaks:') !!}
    <p>{!! $job->breaks !!}</p>
</div>

<!-- Bhas Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('bhas', 'Bhas:') !!}
    <p>{!! $job->bhas !!}</p>
</div>

<!-- Insertion Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('insertion', 'Insertion:') !!}
    <p>{!! $job->insertion !!}</p>
</div>

<!-- Extraction Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('extraction', 'Extraction:') !!}
    <p>{!! $job->extraction !!}</p>
</div>

<hr/>
<!-- Customer Id Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('customer_id', 'Customer:') !!}
    @if(isset($job->customer->name))
    <p>{!! $job->customer->name !!}</p>
    @endif
</div>

<!-- Contact Id Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('contact_id', 'Contact:') !!}
    @if(isset($job->contact->name))
    <p>{!! $job->contact->name !!}</p>
    @endif
</div>

<!-- Operator Id Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('operator_id', 'Operator:') !!}
    @if(isset($job->operator->name))
    <p>{!! $job->operator->name !!}</p>
    @endif
</div>

<!-- Rig Id Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('rig_id', 'Rig:') !!}
    @if(isset($job->rig->name))
        <p>{!! $job->rig->name !!}</p>
    @endif
</div>

<!-- Inspector Id Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('inspector_id', 'Inspector:') !!}
    @if(isset($job->inspector->name))
    <p>{!! $job->inspector->name !!}</p>
    @endif
</div>

<!-- Payee Id Field -->
<div class="form-group col-sm-12 col-md-4">
    {!! Form::label('payee_id', 'Payee:') !!}
    @if(isset($job->payee->name))
    <p>{!! $job->payee->name !!}</p>
    @endif
</div>


<hr/>

<div class="form-group">
    <!-- Email Contact Field -->
    <div class="form-group col-sm-12 col-md-4">
        {!! Form::label('email_contact', 'Email Contact:') !!}
        @if($job->email_contact  == 1)
            <i style="color:#00a157" class="fa fa-check"></i>
        @else
            <i style="color:#CC0000" class="fa fa-times"></i>
        @endif
    </div>

    <!-- Cargo Summary Field -->
    <div class="form-group col-sm-12 col-md-4">
        {!! Form::label('cargo_summary', 'Cargo Summary:') !!}
        @if($job->cargo_summary  == 1)
            <i style="color:#00a157" class="fa fa-check"></i>
        @else
            <i style="color:#CC0000" class="fa fa-times"></i>
        @endif
    </div>

    <!-- Commercial Invoice Field -->
    <div class="form-group col-sm-12 col-md-4">
        {!! Form::label('commercial_invoice', 'Commercial Invoice:') !!}
        @if($job->commercial_invoice  == 1)
            <i style="color:#00a157" class="fa fa-check"></i>
        @else
            <i style="color:#CC0000" class="fa fa-times"></i>
        @endif
    </div>
</div>



@if(Auth::user()->finance == 1)

    <!-- Value Field -->
    <div class="form-group col-sm-12 col-md-4">
        {!! Form::label('value', 'Value:') !!}
        <p>{!! $job->value !!}</p>
    </div>

    <!-- Invoice Number Field -->
    <div class="form-group col-sm-12 col-md-4">
        {!! Form::label('invoice_number', 'Invoice Number:') !!}
        <p>{!! $job->invoice_number !!}</p>
    </div>

@endif

<div class="clearfix"></div>
