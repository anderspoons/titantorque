@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Job
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        @include('flash::message')

        {!! Form::model($job, ['route' => ['jobs.update', $job->id], 'method' => 'patch']) !!}



        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#job-info" data-toggle="tab" aria-expanded="true">Information</a></li>
                <li class=""><a href="#work-scope" data-toggle="tab" aria-expanded="false">Work Scope</a></li>
                <li class=""><a href="#basket-sling" data-toggle="tab" aria-expanded="false">Baskets & Slings</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="job-info">
                    @include('jobs.fields')
                </div>

                <div class="tab-pane" id="work-scope">
                    @include('jobs.edit_work_scope')
                </div>

                <div class="tab-pane" id="basket-sling">
                    @include('jobs.edit_basket_sling')
                </div>
            </div>
        </div>


        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="margin-left: 10px; margin-right: 10px;">
                    <!-- Submit Field -->
                    <div class="pull-left">
                        {!! Form::submit('Save', ['class' => 'loader-trigger btn btn-primary']) !!}
                        <a href="{!! route('jobs.index') !!}" class="btn btn-default">Cancel</a>
                    </div>
                    <div class="pull-right">
                        @if($job->folder_path != null)
                        <a href="file://\\\\TIT01\\Titan Torque\\Jobs\\'{{ $job->folder_path }}" class="btn btn-default">
                            <i class="fa fa-folder-open"></i> Open Folder
                        </a>
                        @endif
                        <a href="{!! url('/jobs/documents/'.$job->id) !!}" class="loader-trigger btn btn-default">
                            <i class="fa fa-cogs"></i> Re-create Documents
                        </a>
                    </div>
                </div>
            </div>
        </div>

        {!! Form::close() !!}

        @include('jobs.scripts')
        @include('jobs.modals.add-sling')
        @include('jobs.modals.add-basket')
        @include('jobs.modals.add-protector')
    </div>

    @include('shared.loader')
@endsection
