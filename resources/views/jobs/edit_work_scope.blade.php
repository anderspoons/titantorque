@php
$apis = false;
$prems = false;

if($job->makes_api > 0 || $job->breaks_api > 0){
    $apis = true;
}

if($job->makes_premium > 0 || $job->breaks_premium > 0 || $job->stenciled_connections > 0 || $job->bakerlocked_connections > 0 || $job->centralizers_fitted > 0 || $job->drift_connections > 0){
    $prems = true;
}

@endphp

<!-- Assembly Field -->
<div class="form-group col-sm-6  col-md-4">
    {!! Form::label('assembly', 'Assembly:') !!}
    {!! Form::text('assembly', null, ['class' => 'form-control']) !!}
</div>

<!-- Bhas Field -->
<div class="form-group col-sm-6  col-md-4">
    {!! Form::label('bhas', 'Bhas:') !!}
    {!! Form::number('bhas', null, ['class' => 'form-control']) !!}
</div>

<hr>

<!-- Api Connections -->
<div class="form-group col-sm-12 col-md-4">
    @if($apis == true)
        {{ Form::checkbox('api_chck', '1', true, ['class' => 'icheck api-check']) }}&nbsp;&nbsp;
    @else
        {{ Form::checkbox('api_chck', '1', false, ['class' => 'icheck api-check']) }}&nbsp;&nbsp;
    @endif
    {!! Form::label('api_chck', 'Api Connections?') !!}
</div>

<!-- Premium Connections -->
<div class="form-group col-sm-12 col-md-4">
    @if($prems == true)
        {{ Form::checkbox('premium_chck', '1', true, ['class' => 'icheck premium-check']) }}&nbsp;&nbsp;
    @else
        {{ Form::checkbox('premium_chck', '1', false, ['class' => 'icheck premium-check']) }}&nbsp;&nbsp;
    @endif
    {!! Form::label('premium_chck', 'Premium Connections?') !!}
</div>

<hr>

<div class="clearfix"></div>
{{-- API Shite --}}
<div class="hidden api-connections">
    <!-- Makes Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('makes_api', 'API Makes:') !!}
        {!! Form::number('makes_api', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Breaks Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('breaks_api', 'API Breaks:') !!}
        {!! Form::number('breaks_api', null, ['class' => 'form-control']) !!}
    </div>
    <hr>
    <div class="clearfix"></div>
</div>

{{-- Premium Shite --}}
<div class="hidden premium-connections">

    <!-- Makes Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('makes_premium', 'Premium Makes:') !!}
        {!! Form::number('makes_premium', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Breaks Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('breaks_premium', 'Premium Breaks:') !!}
        {!! Form::number('breaks_premium', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Stenciled Connections Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('stenciled_connections', 'Stenciled Connections:') !!}
        {!! Form::number('stenciled_connections', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Bakerlocked Connections Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('bakerlocked_connections', 'Bakerlocked Connections:') !!}
        {!! Form::number('bakerlocked_connections', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Stenciled Connections Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('drift', 'Drift:') !!}
        {!! Form::number('drift', null, ['class' => 'form-control']) !!}
    </div>

    <!-- Centralizers Fitted Field -->
    <div class="form-group col-sm-6  col-md-4">
        {!! Form::label('centralizers_fitted', 'Centralizers Fitted:') !!}
        {!! Form::number('centralizers_fitted', null, ['class' => 'form-control']) !!}
    </div>

    <hr>
    <div class="clearfix"></div>
</div>

<!-- Note Field -->
<div class="form-group col-sm-6  col-md-4">
    {!! Form::label('note', 'Note:') !!}
    {!! Form::text('note', null, ['class' => 'form-control']) !!}
</div>

<!-- Insertion Field -->
<div class="form-group col-sm-6  col-md-4">
    {!! Form::label('insertion', 'Insertion:') !!}
    {!! Form::number('insertion', null, ['class' => 'form-control']) !!}
</div>

<!-- Extraction Field -->
<div class="form-group col-sm-6  col-md-4">
    {!! Form::label('extraction', 'Extraction:') !!}
    {!! Form::number('extraction', null, ['class' => 'form-control']) !!}
</div>

<div class="clearfix"></div>