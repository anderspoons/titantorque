<div class="modal fade" tabindex="-1" role="dialog" id="basket-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Basket</h4>
            </div>
            <div class="modal-body">
                {{ Form::label('basket_id', 'Basket:') }}
                @include('baskets.select2')
                {{ Form::label('basket_note', 'Note for this basket:') }}
                {{ Form::text('basket_note', '', array('class' => 'form-control', 'id' => 'basket-note')) }}
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="add-basket-confirm btn btn-primary">Add Basket</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="hidden basket-template">
    <div class="form-group">

        {{ Form::hidden('basket[][id]', '', array('class' => 'id-basket')) }}
        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('basket[][asset]', 'Basket:') }}
            {{ Form::text('basket[][asset]', '', array('class' => 'form-control asset-basket', 'readonly' => 'readonly')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('basket[][length]', 'Length:') }}
            {{ Form::text('basket[][length]', '', array('class' => 'form-control length-basket', 'readonly' => 'readonly')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('basket[][note]', 'Note:') }}
            {{ Form::text('basket[][note]', '', array('class' => 'form-control note-basket')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('basket[][days_out]', 'Days Out:') }}
            {{ Form::number('basket[][days_out]', '0', array('class' => 'form-control')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('basket[][date_out]', 'Date Out:') }}
            {{ Form::date('basket[][date_out]', '', array('class' => 'form-control')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('basket[][date_in]', 'Date In:') }}
            {{ Form::date('basket[][date_in]', '', array('class' => 'form-control')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::checkbox('basket[][loaded]', '1', false, ['class' => 'icheck loaded-chk']) }}
            {{ Form::label(' Loaded?') }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::checkbox('basket[][unloaded]', '1', false, ['class' => 'icheck loaded-chk']) }}
            {{ Form::label(' Unloaded?') }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('', '&nbsp;') }}
            <br/>
            {{ Form::button('<i class="fa fa-times"></i>', array('class' => 'remove-basket btn btn-sm btn-primary')) }}
        </div>

    </div>
</div>

