<div class="modal fade" tabindex="-1" role="dialog" id="sling-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content box">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Sling</h4>
            </div>
            <div class="modal-body">
                {{ Form::label('sling_id', 'Sling:') }}
                @include('slings.select2')
                {{ Form::label('sling_qty', 'Quantity to add:') }}
                <p class="sling-warning hidden" style="color:#CC0000">Quantity entered is invalid.</p>
                {{ Form::number('', '', array('class' => 'form-control', 'id' => 'sling-quantity')) }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="add-sling-confirm btn btn-primary">Add Sling</button>
            </div>
            <div class="overlay loader hidden">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="hidden sling-template">
    <div class="form-group">
        {{ Form::hidden('sling[][id]', '', array('class' => 'id-sling')) }}
        <div class="form-group col-sm-12 col-md-3">
            {{ Form::label('sling[][serial]', 'Serial:') }}
            {{ Form::text('sling[][serial]', '', array('class' => 'form-control serial-sling', 'readonly' => 'readonly')) }}
        </div>

        <div class="form-group col-sm-12 col-md-3">
            {{ Form::label('sling[][quantity]', 'Quantity:') }}
            {{ Form::text('sling[][quantity]', '', array('class' => 'form-control quantity-sling', 'readonly' => 'readonly')) }}
        </div>

        <div class="form-group col-sm-12 col-md-3">
            {{ Form::label('sling[][capacity]', 'Capacity:') }}
            {{ Form::text('sling[][capacity]', '', array('class' => 'form-control capacity-sling', 'readonly' => 'readonly')) }}
        </div>

        <div class="form-group col-sm-12 col-md-3">
            {{ Form::label('', '&nbsp;') }}
            <br/>
            {{ Form::button('<i class="fa fa-times"></i>', array('class' => 'remove-sling btn btn-sm btn-primary')) }}
        </div>
    </div>

</div>
