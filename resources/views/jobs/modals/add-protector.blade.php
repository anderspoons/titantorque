<div class="modal fade" tabindex="-1" role="dialog" id="protector-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Protector</h4>
            </div>
            <div class="modal-body">
                {{ Form::label('protector_id', 'Protector:') }}
                @include('protectors.select2')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="add-protector-confirm btn btn-primary">Add Protector</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="hidden protector-template">
    <div class="form-group">
        {{ Form::hidden('protector[][id]', null, array('class' => 'id-protector')) }}
        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('protector[][identifier]', 'Identifier') }}
            {{ Form::text('protector[][identifier]', null, array('class' => 'identifier-protector form-control', 'readonly' => 'readonly')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('protector[][sizing]', 'Sizing') }}
            {{ Form::text('protector[][sizing]', null, array('class' => 'sizing-protector form-control', 'readonly' => 'readonly')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('protector[][days_out]', 'Days Out:') }}
            {{ Form::number('protector[][days_out]', '0', array('class' => 'form-control')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('protector[][date_out]', 'Date Out:') }}
            {{ Form::date('protector[][date_out]', '', array('class' => 'form-control')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('protector[][date_in]', 'Date In:') }}
            {{ Form::date('protector[][date_in]', '', array('class' => 'form-control')) }}
        </div>

        <div class="form-group col-sm-12 col-md-4">
            {{ Form::label('', 'Remove:') }}
            <br/>
            {{ Form::button('<i class="fa fa-times"></i>', array('class' => 'remove-protector btn btn-sm btn-primary')) }}
        </div>
    </div>
</div>