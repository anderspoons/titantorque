<select name="status[]" multiple="multiple" class="form-control" id="status-select">
    @if(isset($job->statuses))
        @foreach($job->statuses as $status)
            <option selected class="{{ $status->style }}" value="{{ $status->id }}">{{ $status->name }}</option>
        @endforeach
        {{--<div class="hidden">--}}
            {{--{{ print_r($job->statuses) }}--}}
        {{--</div>--}}
    @endif
</select>

@push('scripts')
<script type="text/javascript">
    (function (window, $) {
        jQuery(document).ready(function ($) {
            $("#status-select").select2({
                multiple: true,
                tags: true,
                ajax: {
                    url: "{!! url('status/select2') !!}",
                    dataType: "json",
                    data: function (term) {
                        return {query: term.term }
                    },
                    results: function (data) {
                        return {
                            results: data.results
                        };
                    }
                },
                createSearchChoice: function (term, data) {
                    if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                        return {
                            id: term,
                            text: term
                        };
                    }
                },
                width: '100%',
            });
        });
    })(window, jQuery);
</script>
@endpush