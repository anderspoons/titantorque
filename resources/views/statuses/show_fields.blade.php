<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $status->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $status->name !!}</p>
</div>

<!-- Job Field -->
<div class="form-group">
    {!! Form::label('job', 'Job:') !!}
    <p>{!! $status->job !!}</p>
</div>

<!-- Basket Field -->
<div class="form-group">
    {!! Form::label('basket', 'Basket:') !!}
    <p>{!! $status->basket !!}</p>
</div>

<!-- Sling Field -->
<div class="form-group">
    {!! Form::label('sling', 'Sling:') !!}
    <p>{!! $status->sling !!}</p>
</div>

<!-- Protector Field -->
<div class="form-group">
    {!! Form::label('protector', 'Protector:') !!}
    <p>{!! $status->protector !!}</p>
</div>

<!-- Automatic Field -->
<div class="form-group">
    {!! Form::label('automatic', 'Automatic:') !!}
    <p>{!! $status->automatic !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $status->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $status->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $status->updated_at !!}</p>
</div>

