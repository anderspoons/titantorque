<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Job Status -->
<div class="form-group col-sm-6">
    {!! Form::label('job', 'Job Status?') !!}
    <div class="form-group">
    @if(isset($status) && $status->job == 1)
        {{ Form::checkbox('job', '1', true, ['class' => 'icheck']) }}
    @else
        {{ Form::checkbox('job', '1', false, ['class' => 'icheck']) }}
    @endif
    </div>
</div>

<!-- Basket Status -->
<div class="form-group col-sm-6">
    {!! Form::label('basket', 'Basket Status?') !!}
    <div class="form-group">
        @if(isset($status) && $status->basket == 1)
            {{ Form::checkbox('basket', '1', true, ['class' => 'icheck']) }}
        @else
            {{ Form::checkbox('basket', '1', false, ['class' => 'icheck']) }}
        @endif
    </div>
</div>

<!-- Protector Status -->
<div class="form-group col-sm-6">
    {!! Form::label('protector', 'Protector Status?') !!}
    <div class="form-group">
        @if(isset($status) && $status->protector == 1)
            {{ Form::checkbox('protector', '1', true, ['class' => 'icheck']) }}
        @else
            {{ Form::checkbox('protector', '1', false, ['class' => 'icheck']) }}
        @endif
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('statuses.index') !!}" class="btn btn-default">Cancel</a>
</div>
