<select name="machine_id" class="form-control" id="machine-select">
    @if(isset($booking->machine_id))
        <option selected value="{{ $booking->machine_id }}"> {{ $booking->machine->name }}</option>
    @endif
</select>

@push('scripts')
<script type="text/javascript">
    (function (window, $) {
        jQuery(document).ready(function ($) {
            $("#machine-select").select2({
                ajax: {
                    url: "{!! url('machine/select2') !!}",
                    dataType: "json",
                    data: function (term) {
                        return {
                            query: term.term
                        }
                    },
                    results: function (data) {
                        return {
                            results: data.results
                        };
                    }
                },
                createSearchChoice: function (term, data) {
                    if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                        return {
                            id: term,
                            text: term
                        };
                    }
                }
            });
        });
    })(window, jQuery);
</script>
@endpush