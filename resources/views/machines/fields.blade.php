<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Colour Field -->
<div class="form-group col-sm-6">
    {!! Form::label('colour', 'Colour:') !!}
    {!! Form::text('colour', null, ['class' => 'form-control jscolor']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('machines.index') !!}" class="btn btn-default">Cancel</a>
</div>
