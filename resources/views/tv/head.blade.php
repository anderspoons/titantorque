<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Titan Torque - Operations System TV Display</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/skins/_all-skins.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ url('css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/style.css') }}">
    @yield('css')
</head>

<body class="skin-black-light sidebar-mini">
<div class="wrapper">
    <!-- Main Header -->
    <header class="main-header">
        <!-- Logo -->
        <a href="{{ url('/') }}" class="logo">
            <img src="{{ url('/img/logo_small.png') }}"/>
        </a>

        <nav class="navbar navbar-static-top" role="navigation">
            <div class="pull-right">
                <div id="clock" class="clock">loading ...</div>
            </div>
        </nav>

    </header>

    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </div>
</div>
</body>

<!-- jQuery 2.1.4 -->
<script src="{{ url('/js/jquery.min.js') }}"></script>
<script src="{{ url('/js/jquery.jkit.1.2.16.min.js') }}"></script>
<script src="{{ url('/js/bootstrap.min.js') }}"></script>
{{-- Custom Shits --}}
<script src="{{ url('/js/custom.js') }}"></script>
<script src="{{ url('/js/moment.js') }}"></script>
<script src="{{ url('/js/fullcalendar.min.js') }}"></script>

@stack('scripts')
</body>
</html>