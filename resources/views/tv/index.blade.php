@extends('tv.head')
@section('content')
    <table class="tvtable">
        <thead>
            <th class="tvcol1 tvth tvrborder">Torque #</th>
            <th class="tvcol2 tvth tvrborder">Tools #</th>
            <th class="tvcol3 tvth tvrborder">Customer</th>
            <th class="tvcol4 tvth tvrborder">Contact</th>
            <th class="tvcol5 tvth tvrborder">Rig</th>
            <th class="tvcol6 tvth tvrborder">Well</th>
            <th class="tvcol7 tvth tvrborder">BHAS</th>
            <th class="tvcol8 tvth tvrborder">Assembly</th>
            <th class="tvcol9 tvth tvrborder">Due Date</th>
            <th class="tvcol10 tvth tvrborder">Basket</th>
            <th class="tvcol11 tvth tvrborder">Lift Subs</th>
            <th class="tvcol12 tvth ">Notes</th>
        </thead>
        <tbody id="table-body">
        @foreach($jobs as $job)
            @php
                $class = 'odd';
                if($loop->iteration % 2 == 0) {
                    $class = 'even';
                }
                if($job->statuses->contains(6)){
                    $class .= ' overdue';
                }
                $makes = false;
                $breaks = false;
                $color = "#FFF";
                if($job->makes_api > 0 || $job->makes_premium > 0){
                    $makes = true;
                }
                if($job->breaks_api > 0 || $job->breaks_premium > 0){
                    $breaks = true;
                }
                if($breaks && $makes){
                    $color = $colors['both'];
                }
                if($breaks && !$makes){
                    $color = $colors['breaks'];
                }
                if(!$breaks && $makes){
                    $color = $colors['makes'];
                }
            @endphp
            <tr class="{{ $class }}" style="background-color:{{$color}};">
                <td class="tvtd tvcol1 tvrborder">
                    @if(isset($job->number_torque))
                    @endif
                        {{ $job->number_torque }}
                </td>
                <td class="tvtd tvcol2 tvrborder">
                    @if(isset($job->number_tools))
                        {{ $job->number_tools }}
                    @endif
                </td>
                <td class="tvtd tvcol3 tvrborder">
                    @if(isset($job->customer->name))
                        {{ $job->customer->name }}
                    @endif
                </td>
                <td class="tvtd tvcol4 tvrborder">
                    @if(isset($job->contact->name))
                        {{ $job->contact->name }}
                    @endif
                </td>
                <td class="tvtd tvcol5 tvrborder">
                    @if(isset($job->rig->name))
                        {{ $job->rig->name }}
                    @endif
                </td>
                <td class="tvtd tvcol6 tvrborder">
                    @if(isset($job->well->name))
                        {{ $job->well->name }}
                    @endif
                </td>
                <td class="tvtd tvcol7 tvrborder">
                    @if(isset($job->bhas))
                        {{ $job->bhas }}
                    @endif
                </td>
                <td class="tvtd tvcol8 tvrborder">
                    @if(isset($job->assembly))
                        {{ $job->assembly }}
                    @endif
                </td>
                <td class="tvtd tvcol9 tvrborder">
                    @if(isset($job->due_date))
                        {{ date('d/m/y', strtotime($job->due_date)) }}
                    @endif
                </td>
                <td class="tvtd tvcol10 tvrborder">
                    @foreach($job->baskets as $basket)
                        {{ $basket->asset }} <br/>
                    @endforeach
                </td>
                <td class="tvtd tvcol11 tvrborder">
                    @if(isset($job->lift_subs))
                        {{ $job->lift_subs }}
                    @endif
                </td>
                <td class="tvtd tvcol12 tvrborder">
                    @if(isset($job->note))
                        {{ $job->note }}
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="">

    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            if ($('#table-body').height()>980) {
                $('#table-body').jKit('paginate',
                    {
                        'limit': '980',
                        'by': 'height',
                        'animation': 'fade'
                    }
                );

                var total = 0, current = 0;

                var page = function(n){
                    if(n > total - 1){
                        current = 0;
                        n = 0;
                    }
                    $('.jkit-pagination li').eq(n).click();
                    current = n;
                }

                if($('.jkit-pagination').length > 0){
                    total = $('.jkit-pagination li').length;
                    setInterval(function() {
                        console.log('Should call page');
                        page(current + 1)
                    },15000);
                }

                setTimeout(function() {
                    console.log('Add changed ajax check');
                },(5000));
        
            }
        });
    </script>
@endpush