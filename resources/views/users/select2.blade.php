<select name="manager_id" class="form-control" id="manager-select">
    @if(isset($user))
        <option selected value="{{ $user->id }}">{{ $user->name }}</option>
    @endif

    @if(isset($job->manager))
        <option selected value="{{ $job->manager_id }}">{{ $job->manager->name }}</option>
    @endif
</select>

@push('scripts')
<script type="text/javascript">
    (function (window, $) {
        jQuery(document).ready(function ($) {
            $("#manager-select").select2({
                ajax: {
                    url: "{!! url('manager/select2') !!}",
                    dataType: "json",
                    data: function (term) {
                        return {
                            query: term.term
                        }
                    },
                    results: function (data) {
                        return {
                            results: data.results
                        };
                    }
                },
                createSearchChoice: function (term, data) {
                    if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                        return {
                            id: term,
                            text: term
                        };
                    }
                }
            });
        });
    })(window, jQuery);
</script>
@endpush