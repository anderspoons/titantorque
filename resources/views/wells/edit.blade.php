@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Well
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($well, ['route' => ['wells.update', $well->id], 'method' => 'patch']) !!}

                        @include('wells.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection