<select name="well_id" class="form-control" id="well-select">
    @if(isset($job->well))
        <option value="{{ $job->well->id }}">{{ $job->well->name }}</option>
    @endif
</select>

@push('scripts')
<script type="text/javascript">
    (function (window, $) {
        jQuery(document).ready(function ($) {
            $("#well-select").select2({
                ajax: {
                    url: "{!! url('well/select2') !!}",
                    dataType: "json",
                    data: function (term) {
                        return {
                            query: term.term
                        }
                    },
                    results: function (data) {
                        return {
                            results: data.results
                        };
                    }
                },
                createSearchChoice: function (term, data) {
                    if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                        return {
                            id: term,
                            text: term
                        };
                    }
                }
            });
        });
    })(window, jQuery);
</script>
@endpush