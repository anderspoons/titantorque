<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $options->id !!}</p>
</div>

<!-- Option Name Field -->
<div class="form-group">
    {!! Form::label('option_name', 'Option Name:') !!}
    <p>{!! $options->option_name !!}</p>
</div>

<!-- Option Value Field -->
<div class="form-group">
    {!! Form::label('option_value', 'Option Value:') !!}
    <p>{!! $options->option_value !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $options->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $options->updated_at !!}</p>
</div>

