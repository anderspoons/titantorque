<!-- Option Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('option_name', 'Option Name:') !!}
    {!! Form::text('option_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Option Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('option_value', 'Option Value:') !!}
    {!! Form::text('option_value', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('options.index') !!}" class="btn btn-default">Cancel</a>
</div>
