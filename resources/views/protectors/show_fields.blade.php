<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $protector->id !!}</p>
</div>

<!-- Identifier Field -->
<div class="form-group">
    {!! Form::label('identifier', 'Identifier:') !!}
    <p>{!! $protector->identifier !!}</p>
</div>

<!-- Sizing Field -->
<div class="form-group">
    {!! Form::label('sizing', 'Sizing:') !!}
    <p>{!! $protector->sizing !!}</p>
</div>

<!-- Available Field -->
<div class="form-group">
    {!! Form::label('available', 'Available:') !!}
    <p>{!! $protector->available !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $protector->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $protector->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $protector->updated_at !!}</p>
</div>

