@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Protector
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($protector, ['route' => ['protectors.update', $protector->id], 'method' => 'patch']) !!}

                        @include('protectors.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection