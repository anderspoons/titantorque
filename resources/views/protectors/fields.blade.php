<!-- Identifier Field -->
<div class="form-group col-sm-6">
    {!! Form::label('identifier', 'Identifier:') !!}
    {!! Form::text('identifier', null, ['class' => 'form-control']) !!}
</div>

<!-- Sizing Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sizing', 'Sizing:') !!}
    {!! Form::text('sizing', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('protectors.index') !!}" class="btn btn-default">Cancel</a>
</div>
