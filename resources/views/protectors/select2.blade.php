<select name="" class="form-control col-sm-12" id="protector-select">
</select>

@push('scripts')
<script type="text/javascript">
    (function (window, $) {
        jQuery(document).ready(function ($) {
            init_protectors();

            function init_protectors(){
                $("#protector-select").select2({
                    ajax: {
                        url: "{!! url('protector/select2') !!}",
                        dataType: "json",
                        data: function (term) {
                            return {query: term.term }
                        },
                        results: function (data) {
                            return {
                                results: data.results
                            };
                        }
                    },
                    createSearchChoice: function (term, data) {
                        if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                            return {
                                id: term,
                                text: term
                            };
                        }
                    },
                    width: '100%',
                });
            }

            $('.add-protector-btn').click(function(e){
                init_protectors();
            })
        });
    })(window, jQuery);
</script>
@endpush