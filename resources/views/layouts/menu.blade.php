<li class="{{ Request::is('jobs*') ? 'active' : '' }}">
    <a href="{!! route('jobs.index') !!}"><i class="fa fa-cogs"></i><span>Jobs</span></a>
</li>

<li class="{{ Request::is('booking*') ? 'active' : '' }}">
    <a href="{!! url('bookings') !!}"><i class="fa fa-calendar"></i><span>Bookings</span></a>
</li>

<li class="{{ Request::is('customers*') ? 'active' : '' }}">
    <a href="{!! route('customers.index') !!}"><i class="fa fa-users"></i><span>Customers / Operators</span></a>
</li>

<li class="{{ Request::is('rigs*') ? 'active' : '' }}">
    <a href="{!! route('rigs.index') !!}"><i class="glyphicon glyphicon-oil"></i><span>Rigs</span></a>
</li>

<li class="{{ Request::is('wells*') ? 'active' : '' }}">
    <a href="{!! route('wells.index') !!}"><i class="fa fa-fire"></i><span>Wells</span></a>
</li>

<li class="{{ Request::is('inspectors*') ? 'active' : '' }}">
    <a href="{!! route('inspectors.index') !!}"><i class="glyphicon glyphicon-ok"></i><span>Inspectors</span></a>
</li>

<li class="{{ Request::is('contacts*') ? 'active' : '' }}">
    <a href="{!! route('contacts.index') !!}"><i class="glyphicon glyphicon-phone-alt"></i><span>Contacts</span></a>

</li><li class="{{ Request::is('baskets*') ? 'active' : '' }}">
    <a href="{!! route('baskets.index') !!}"><i class="fa-shopping-basket fa"></i><span>Baskets</span></a>
</li>

<li class="{{ Request::is('slings*') ? 'active' : '' }}">
    <a href="{!! route('slings.index') !!}"><i class="fa fa-ils"></i><span>Slings</span></a>
</li>

<li class="{{ Request::is('protectors*') ? 'active' : '' }}">
    <a href="{!! route('protectors.index') !!}"><i class="fa fa-diamond"></i><span>Protectors</span></a>
</li>

<li class="{{ Request::is('reports*') ? 'active' : '' }}">
    <a href="{!! url('reports') !!}"><i class="fa fa-line-chart"></i><span>Reports</span></a>
</li>

<li class="{{ Request::is('job/stats*') ? 'active' : '' }}">
    <a href="{!! url('job/stats') !!}"><i class="fa fa-pie-chart"></i><span>Job Statistics</span></a>
</li>

<li class="{{ Request::is('statuses*') ? 'active' : '' }}">
    <a href="{!! route('statuses.index') !!}"><i class="fa fa-commenting"></i><span>Statuses</span></a>
</li>

<li class="{{ Request::is('machines*') ? 'active' : '' }}">
    <a href="{!! route('machines.index') !!}"><i class="fa fa-tachometer"></i><span>Machines</span></a>
</li>

<li class="{{ Request::is('options*') ? 'active' : '' }}">
    <a href="{!! route('options.index') !!}"><i class="fa fa-check-square"></i><span>Options</span></a>
</li>

<li class="{{ Request::is('tv*') ? 'active' : '' }}">
    <a href="{!! url('/tv') !!}"><i class="fa fa-tv"></i><span>TV View</span></a>
</li>
