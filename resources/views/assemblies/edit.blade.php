@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Assembly
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($assembly, ['route' => ['assemblies.update', $assembly->id], 'method' => 'patch']) !!}

                        @include('assemblies.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection