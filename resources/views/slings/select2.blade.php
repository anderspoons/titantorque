<select name="" class="form-control" id="sling-select">
</select>

@push('scripts')
<script type="text/javascript">
    (function (window, $) {
        jQuery(document).ready(function ($) {
            init_slings();

            function init_slings(){
                $("#sling-select").select2({
                    ajax: {
                        url: "{!! url('sling/select2') !!}",
                        dataType: "json",
                        data: function (term) {
                            return {query: term.term }
                        },
                        results: function (data) {
                            return {
                                results: data.results
                            };
                        }
                    },
                    createSearchChoice: function (term, data) {
                        if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                            return {
                                id: term,
                                text: term
                            };
                        }
                    },
                    width: '100%',
                });
            }

            $("#sling-select").on('select2:select', function (evt) {
                var expired_date = evt.params.data.expired;
                if (expired_date !== true) {
                    $("#sling-select").html('').select2({data: [{id: '', text: ''}]});
                    init_slings();
                    warning('These slings are over 6 months old now and can not be used!')
                    disable_add(true);
                } else {
                    disable_add(false);
                }
            })

            function disable_add(yes_no) {
                if ($('.add-sling-confirm').length > 0) {
                    var attr = $('.add-sling-confirm').attr('disabled');
                    if (yes_no) {
                        //disable on true;
                        // For some browsers, `attr` is undefined; for others,
                        // `attr` is false.  Check for both.
                        if (typeof attr == typeof undefined || attr == false) {
                            $('.add-sling-confirm').attr('disabled', 'disabled');
                        }
                    } else {
                        if (typeof attr !== typeof undefined || attr !== false) {
                            $('.add-sling-confirm').removeAttr('disabled');
                        }
                    }
                }
            }

            function warning(text) {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }

                toastr["warning"](text, "Warning");
            }

        });
    })(window, jQuery);
</script>
@endpush