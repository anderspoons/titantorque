<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $sling->id !!}</p>
</div>

<!-- Capacity Id Field -->
<div class="form-group">
    {!! Form::label('capacity_id', 'Capacity Id:') !!}
    <p>{!! $sling->capacity_id !!}</p>
</div>

<!-- Bespoke Field -->
<div class="form-group">
    {!! Form::label('bespoke', 'Bespoke:') !!}
    <p>{!! $sling->bespoke !!}</p>
</div>

<!-- Quantity Field -->
<div class="form-group">
    {!! Form::label('quantity', 'Quantity:') !!}
    <p>{!! $sling->quantity !!}</p>
</div>

<!-- Serial Field -->
<div class="form-group">
    {!! Form::label('serial', 'Serial:') !!}
    <p>{!! $sling->serial !!}</p>
</div>

<!-- Pdf Path Field -->
<div class="form-group">
    {!! Form::label('pdf_path', 'Pdf Path:') !!}
    <p>{!! $sling->pdf_path !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $sling->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $sling->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $sling->updated_at !!}</p>
</div>

