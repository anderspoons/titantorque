<!-- Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quantity', 'Quantity:') !!}
    {!! Form::number('quantity', null, ['class' => 'form-control', 'required'=>'required']) !!}
</div>

<!-- Serial Field -->
<div class="form-group col-sm-6">
    {!! Form::label('serial', 'Serial:') !!}
    {!! Form::text('serial', null, ['class' => 'form-control', 'required'=>'required']) !!}
</div>

<!-- Pdf Path Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pdf', 'PDF Certificate:') !!}
    {!! Form::file('pdf', null, ['class' => 'form-control']) !!}
</div>

<!-- Capacity Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('capacity_id', 'Capacity:') !!}
    <br>
    @foreach($capacity as $cap)
        @if(isset($sling))
            @if($sling->capacity_id == $cap->id)
                <label>{{ Form::radio('capacity_id', $sling->capacity_id, true, ["required"=>"required"]) }} {{ $cap->capacity }}</label>&nbsp;&nbsp;
            @else
                <label>{{ Form::radio('capacity_id', $cap->id, false, ["required"=>"required"]) }} {{ $cap->capacity }}</label>&nbsp;&nbsp;
            @endif
        @else
            <label>{{ Form::radio('capacity_id', $cap->id, false, ["required"=>"required"]) }} {{ $cap->capacity }}</label>&nbsp;&nbsp;
        @endif
    @endforeach

    @if(isset($sling))
        @if($sling->bespoke == 1)
            <label>{{ Form::radio('capacity_id', $sling->capacity_id, true, ['class' => 'bespoke']) }} Bespoke</label>&nbsp;&nbsp;
            <div class="bespoke_text form-group">
                {!! Form::label('capacity_text', 'Bespoke Capacity:') !!}
                {!! Form::text('capacity_text', $sling->capacity->capacity, ['class' => 'form-control']) !!}
            </div>
        @else
            <label>{{ Form::radio('cap', null, false, ['class' => 'bespoke']) }} Bespoke</label>&nbsp;&nbsp;
            <div class="bespoke_text hidden form-group">
                {!! Form::label('capacity_text', 'Bespoke Capacity:') !!}
                {!! Form::text('capacity_text', null, ['class' => 'form-control']) !!}
            </div>
        @endif
    @else
        <label>{{ Form::radio('cap', null, false, ['class' => 'bespoke']) }} Bespoke</label>&nbsp;&nbsp;
        <div class="bespoke_text hidden form-group">
            {!! Form::label('capacity_text', 'Bespoke Capacity:') !!}
            {!! Form::text('capacity_text', null, ['class' => 'form-control']) !!}
        </div>
    @endif

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('slings.index') !!}" class="btn btn-default">Cancel</a>
</div>


@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('body').on('click', 'input[type=radio]', function(e){
            if($(this).hasClass('bespoke')){
                $('.bespoke_text').removeClass('hidden').find("input").prop("required",true);
                $('[name=capacity_id]').prop("required",false).prop('checked', false);
            } else {
                $('.bespoke_text').addClass('hidden').find("input").prop("required",false);
                $("input.bespoke").prop('checked', false);
                $('[name=capacity_id]').prop("required",true);
            }
        });
    });
</script>
@endpush