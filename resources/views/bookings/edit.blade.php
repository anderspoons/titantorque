@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Edit Booking
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::model($booking, ['route' => ['bookings.update', $booking->id], 'method' => 'patch']) !!}

                        @include('bookings.fields')

                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('contacts.index') !!}" class="btn btn-default">Cancel</a>
                            <div class="pull-right">
                                {!! Form::model($booking, ['route' => ['bookings.destroy', $booking->id], 'method' => 'DELETE']) !!}
                                    {!! Form::submit('Delete Booking', ['class' => 'btn btn-danger', 'onclick' => 'return confirm(\'Are you sure you want to delete this booking?\')']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
