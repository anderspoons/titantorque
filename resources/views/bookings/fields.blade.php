<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('machine_id', 'Machine:') !!}
    @include('machines.select2')
</div>

<!-- Customer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customer_id', 'Customer:') !!}
    @include('customers.select2')
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('note', 'Note:') !!}
    {!! Form::text('note', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Start Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_start', 'Start Time / Date:') !!}
    @if(isset($booking->date_start))
        {!! Form::text('', \Carbon\Carbon::parse($booking->date_start)->format('d/m/y H:i'), ['class' => 'form-control datetimepicker', 'data-feild' => 'date_start']) !!}
    @else
        {!! Form::text('', null, ['class' => 'form-control datetimepicker', 'data-feild' => 'date_start']) !!}
    @endif
    {!! Form::hidden('date_start', null) !!}
</div>

<!-- Date End Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_end', 'End Time / Date:') !!}
    @if(isset($booking->date_end))
        {!! Form::text('', \Carbon\Carbon::parse($booking->date_end)->format('d/m/y H:i'), ['class' => 'form-control datetimepicker', 'data-feild' => 'date_end']) !!}
    @else
        {!! Form::text('', null, ['class' => 'form-control datetimepicker', 'data-feild' => 'date_end']) !!}
    @endif
    {!! Form::hidden('date_end', null) !!}
</div>
