<!-- Length Field -->
<div class="form-group col-sm-6">
    {!! Form::label('length', 'Length (Dimensions in feet):') !!}
    {!! Form::number('length', null, ['class' => 'form-control', 'max' => 99, 'min' => 1]) !!}
</div>

<!-- Asset Num Field -->
<div class="form-group col-sm-6">
    {!! Form::label('asset_num', 'Asset Number:') !!}
    {!! Form::text('asset_num', null, ['class' => 'form-control']) !!}
</div>

<!-- Max Gross Field -->
<div class="form-group col-sm-6">
    {!! Form::label('max_gross', 'Max Gross Weight:') !!}
    {!! Form::text('max_gross', null, ['class' => 'form-control']) !!}
</div>

<!-- Cert Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cert_date', 'Cert Date:') !!}
    {!! Form::date('cert_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Pdf Path Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pdf', 'PDF Certificate:') !!}
    {!! Form::file('pdf', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('baskets.index') !!}" class="btn btn-default">Cancel</a>
</div>
