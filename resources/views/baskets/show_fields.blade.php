<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $basket->id !!}</p>
</div>

<!-- Length Field -->
<div class="form-group">
    {!! Form::label('length', 'Length:') !!}
    <p>{!! $basket->length !!}</p>
</div>

<!-- Asset Num Field -->
<div class="form-group">
    {!! Form::label('asset_num', 'Asset Num:') !!}
    <p>{!! $basket->asset_num !!}</p>
</div>

<!-- Pdf Path Field -->
<div class="form-group">
    {!! Form::label('pdf_path', 'Pdf Path:') !!}
    <p>{!! $basket->pdf_path !!}</p>
</div>

<!-- Cert Date Field -->
<div class="form-group">
    {!! Form::label('cert_date', 'Cert Date:') !!}
    <p>{!! $basket->cert_date !!}</p>
</div>

<!-- Deleted At Field -->
<div class="form-group">
    {!! Form::label('deleted_at', 'Deleted At:') !!}
    <p>{!! $basket->deleted_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $basket->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $basket->updated_at !!}</p>
</div>

