<select name="" class="form-control" id="basket-select">
</select>

@push('scripts')
<script type="text/javascript">
    (function (window, $) {
        jQuery(document).ready(function ($) {

            init_baskets();

            $('.add-basket-btn').click(function(e){
                console.log('Clicked');
                disable_add(false);
            });

            function init_baskets(){
                $("#basket-select").select2({
                    ajax: {
                        url: "{!! url('basket/select2') !!}",
                        dataType: "json",
                        data: function (term) {
                            return {
                                job_id: "{{ $job ? $job->id : '' }}",
                                query: term.term
                            }
                        },
                        results: function (data) {
                            return {
                                results: data.results
                            };
                        }
                    },
                    createSearchChoice: function (term, data) {
                        if ($(data.results).filter(function () {
                                return this.text.localeCompare(term) === 0;
                            }).length === 0) {
                            return {
                                id: term,
                                text: term
                            };
                        }
                    },
                    width: '100%',
                });
            }

            $("#basket-select").on('select2:select', function (evt) {
                var cert_date = evt.params.data.cert;
                if (cert_date !== true) {
                    $("#basket-select").html('').select2({data: [{id: '', text: ''}]});
                    init_baskets();
                    warning('The cert for this basket is out of date so cannot be added until the cert is renewed.')
                    disable_add(true);
                } else {
                    disable_add(false);
                }
            })

            function warning(text) {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }

                toastr["warning"](text, "Warning");
            }

            function disable_add(yes_no) {
                if ($('.add-basket-confirm').length > 0) {
                    var attr = $('.add-basket-confirm').attr('disabled');
                    if (yes_no) {
                        //disable on true;
                        // For some browsers, `attr` is undefined; for others,
                        // `attr` is false.  Check for both.
                        if (typeof attr == typeof undefined || attr == false) {
                            $('.add-basket-confirm').attr('disabled', 'disabled');
                        }
                    } else {
                        if (typeof attr !== typeof undefined || attr !== false) {
                            $('.add-basket-confirm').removeAttr('disabled');
                        }
                    }
                }
            }

        });
    })(window, jQuery);
</script>
@endpush