@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Reports</h1>
        <h1 class="pull-right">
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                {{ Form::open(['url' => 'reports/generate', 'method' => 'POST']) }}
                <div class="form-group">
                    {{ Form::label('report_id', 'Select Report') }}
                    <select name="report-select" class="form-control" id="report-select">
                        <option value="well">Well Report</option>
                        <option value="job">Job Report</option>
                    </select>
                </div>

                <div class="form-group job-selection hidden">
                    {{ Form::label('job_id', 'Start at Job:') }}
                    @include('jobs.select2 ')
                </div>

                <div class="form-group">
                    {{ Form::submit('Submit', array('class' => 'btn btn-default')) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection

@include('reports.script')