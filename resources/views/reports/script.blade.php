@push('scripts')
<script type="text/javascript">
    (function (window, $) {

        $('body').on('change', '#report-select', function(){
            console.log('Select Changed');
            if($(this).find('option:selected').val() === 'job'){
                $('.job-selection').removeClass('hidden');
            } else {
                $('.job-selection').addClass('hidden');
            }
        })

    })(window, jQuery);
</script>
@endpush