<table>
    <thead>
    <tr>
        <th>Operator</th>
        <th>DD</th>
        <th>Rig</th>
        <th>Well</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Total BHA's</th>
        <th>Conns Out</th>
        <th>Conns In</th>
        <th>Conns Total</th>
        <th>3T</th>
        <th>6T</th>
        <th>8T</th>
        <th>10T</th>
        <th>Bespoke Slings</th>
        <th>&lt;46 ft</th>
        <th>&gt;46 ft</th>
        <th>Total Jobs</th>
    </tr>
    </thead>

    <tbody>
    @foreach($lines as $line)
        <tr>
            <td>{{ $line['operator'] }}</td>
            <td>{{ $line['dd'] }}</td>
            <td>{{ $line['rig'] }}</td>
            <td>{{ $line['well'] }}</td>
            <td>{{ $line['start_date'] }}</td>
            <td>{{ $line['end_date'] }}</td>
            <td>{{ $line['total_bhas'] }}</td>
            <td>{{ $line['cons_out'] }}</td>
            <td>{{ $line['cons_in'] }}</td>
            <td>{{ $line['cons_total'] }}</td>
            <td>{{ $line['3_ton'] }}</td>
            <td>{{ $line['6_ton'] }}</td>
            <td>{{ $line['8_ton'] }}</td>
            <td>{{ $line['10_ton'] }}</td>
            <td>{{ $line['custom_ton'] }}</td>
            <td>{{ $line['under_46'] }}</td>
            <td>{{ $line['over_46'] }}</td>
            <td>{{ $line['total_jobs'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

