<table>
    <thead>
    <tr>
        <th>Contact</th>
        <th>Job Number</th>
        <th>Quote Number</th>
        <th>Job Due Date</th>
        <th>Customer</th>
        <th>Operator</th>
        <th>Rig</th>
        <th>Well</th>
        <th>Assembly Type</th>
        <th>Invoice Number</th>
        <th>Job Value</th>
        <th>Connections</th>
        <th>Make / Break</th>
        <th>PO Number</th>
        <th>Comments / Note</th>
    </tr>
    </thead>

    <tbody>
    @foreach($jobs as $job)
        <tr>
            <td>{{ $job->contact_name }}</td>
            <td>{{ $job->number_torque }}</td>
            <td>{{ $job->quote_number }}</td>
            <td>{{ Date('d/m/y', strtotime($job->due_date)) }}</td>
            <td>{{ $job->customer_name }}</td>
            <td>{{ $job->operator_name }}</td>
            <td>{{ $job->rig_name }}</td>
            <td>{{ $job->well_name }}</td>
            <td>{{ $job->assembly }}</td>
            <td>{{ $job->invoice_number }}</td>
            <td>{{ $job->value }}</td>
            <td>{{ $job->total_connections }}</td>
            <td>
                @php
                $text = '';
                    if($job->makes_api > 0 || $job->makes_premium > 0){
                        $text .= 'Makes, ';
                    }
                    if($job->breaks_api > 0 || $job->breaks_premium > 0){
                        $text .= 'Breaks, ';
                    }

                    if(strlen($text) > 0){
                        echo rtrim($text, ',');
                    } else {
                        echo $job->make_break_custom;
                    }
                @endphp
            </td>
            <td>{{ $job->po_number }}</td>
            <td>{{ $job->note }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
