@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Job Statistics</h1>
        <h1 class="pull-right">
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @if(isset($months))
                    @foreach($months as $month)
                    <div class="col-sm-12 form-group">
                        <div class="col-sm-12">
                            <div class="pull-left col-sm-9">
                                <div class="col-sm-3">
                                    <b>Year:</b><span>{{ $month->year }}</span>
                                </div>
                                <div class="col-sm-3">
                                    <b>Month:</b><span>{{ date('F', mktime(0, 0, 0, $month->month)) }}</span>
                                </div>
                                <div class="col-sm-3">
                                    <b>Jobs Added:</b><span>{{ $month->total }}</span>
                                </div>
                                <div class="col-sm-3">
                                    @if(Auth::user()->finance == 1)
                                        <b>Value:</b><span>£ {{ number_format($month->value) }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="pull-right col-sm-3">
                                <div class='btn btn-default'>
                                    <i class="expander fa fa-plus"></i>
                                </div>
                            </div>
                        </div>

                        @if(isset($jobs))
                            <div class="breakdown-details col-sm-12">
                                @foreach($jobs as $job)
                                    @if($job->year == $month->year && $job->month == $month->month)
                                        <div class="col-sm-12">
                                            <div class="col-sm-3">
                                                <b>Job Number:</b> {{  $job->number_torque }}
                                            </div>
                                            <div class="col-sm-3">
                                                @if(strlen($job->invoice_number) > 1)
                                                    <b>Invoice:</b> {{ $job->invoice_number }}
                                                @endif
                                            </div>
                                            <div class="col-sm-3">
                                                @if(Auth::user()->finance == 1 && strlen($job->value) > 0)
                                                    <b>Value:</b> {{ $job->value }}
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    @endforeach
                @else
                    Somthing went wrong the jobs couldn't be fetched, Arrrrhhh!?!?
                @endif
            </div>
        </div>
    </div>
@endsection
