@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Rig
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($rig, ['route' => ['rigs.update', $rig->id], 'method' => 'patch']) !!}

                        @include('rigs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection