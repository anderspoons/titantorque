<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()){
            if(intval(Auth::user()->active) == 0) {
                Auth::logout();
                return redirect( 'login' )->withErrors( 'sorry, this user account is deactivated' );
            }
        }

        return $next($request);
    }
}
