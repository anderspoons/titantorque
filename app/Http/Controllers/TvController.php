<?php

namespace App\Http\Controllers;

use Flash;
use Response;
use App\Models\Job;
use App\Models\Options;
use Auth;

class TvController extends Controller
{
    public function index(){
        $colors = [
            "makes" => Options::where('option_name', '=', 'colour_make')->first()->option_value,
            "breaks" => Options::where('option_name', '=', 'colour_break')->first()->option_value,
            "both" => Options::where('option_name', '=', 'colour_both')->first()->option_value
        ];

        $jobs = Job::with('customer')
            ->with('rig')
            ->with('well')
            ->with('contact')
            ->with('statuses')
            ->with('baskets')
            ->leftJoin( 'job_status', 'job.id', '=', 'job_status.job_id')
            ->where('job_status.status_id', '=', '1')
            ->orderby('due_date', 'ASC')
            ->get();

        return view('tv.index')->with(['jobs'=> $jobs, 'colors'=>$colors]);
    }
}
