<?php

namespace App\Http\Controllers;

use App\DataTables\RigDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateRigRequest;
use App\Http\Requests\UpdateRigRequest;
use App\Repositories\RigRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;
use App\Models\Rig;

class RigController extends AppBaseController {
	/** @var  RigRepository */
	private $rigRepository;

	public function __construct( RigRepository $rigRepo ) {
		$this->rigRepository = $rigRepo;
	}

	/**
	 * Display a listing of the Rig.
	 *
	 * @param RigDataTable $rigDataTable
	 *
	 * @return Response
	 */
	public function index( RigDataTable $rigDataTable ) {
		return $rigDataTable->render( 'rigs.index' );
	}

	/**
	 * Show the form for creating a new Rig.
	 *
	 * @return Response
	 */
	public function create() {
		return view( 'rigs.create' );
	}

	/**
	 * Store a newly created Rig in storage.
	 *
	 * @param CreateRigRequest $request
	 *
	 * @return Response
	 */
	public function store( CreateRigRequest $request ) {
		$input = $request->all();

		$rig = $this->rigRepository->create( $input );

		Flash::success( 'Rig saved successfully.' );

		return redirect( route( 'rigs.index' ) );
	}

	/**
	 * Display the specified Rig.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show( $id ) {
		$rig = $this->rigRepository->findWithoutFail( $id );

		if ( empty( $rig ) ) {
			Flash::error( 'Rig not found' );

			return redirect( route( 'rigs.index' ) );
		}

		return view( 'rigs.show' )->with( 'rig', $rig );
	}

	/**
	 * Show the form for editing the specified Rig.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit( $id ) {
		$rig = $this->rigRepository->findWithoutFail( $id );
		if ( empty( $rig ) ) {
			Flash::error( 'Rig not found' );
			return redirect( route( 'rigs.index' ) );
		}
		return view( 'rigs.edit' )->with( 'rig', $rig );
	}

	/**
	 * Update the specified Rig in storage.
	 *
	 * @param  int             $id
	 * @param UpdateRigRequest $request
	 *
	 * @return Response
	 */
	public function update( $id, UpdateRigRequest $request ) {
		$rig = $this->rigRepository->findWithoutFail( $id );

		if ( empty( $rig ) ) {
			Flash::error( 'Rig not found' );

			return redirect( route( 'rigs.index' ) );
		}
		$rig = $this->rigRepository->update( $request->all(), $id );
		Flash::success( 'Rig updated successfully.' );
		return redirect( route( 'rigs.index' ) );
	}

	/**
	 * Remove the specified Rig from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy( $id ) {
		$rig = $this->rigRepository->findWithoutFail( $id );

		if ( empty( $rig ) ) {
			Flash::error( 'Rig not found' );

			return redirect( route( 'rigs.index' ) );
		}

		$this->rigRepository->delete( $id );

		Flash::success( 'Rig deleted successfully.' );

		return redirect( route( 'rigs.index' ) );
	}


    public function select2(Request $request) {
        $rigs = Rig::orderBy('name', 'ASC');
        if($request->has('query')){
            $rigs->where('name', 'like', '%'.$request->input('query').'%');
        }
        $results = $rigs->get();
        $formatted_list = array();
        foreach($results as $rig){
            $formatted_list[] = array(
                'id' => $rig->id,
                'text' =>  $rig->name
            );
        }
        return Response::json(array('results' => $formatted_list));
    }

}