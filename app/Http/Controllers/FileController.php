<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class FileController extends Controller
{
    public function serve($dir, $name){
	    $ext = pathinfo($dir.'/'.$name, PATHINFO_EXTENSION);
	    switch($ext){
		    case 'pdf':
			    header("Content-type:application/pdf");
			    header("Content-Disposition:attachment;filename='".$name."'");
		    	break;
	    }
    	return readfile(__DIR__.'/../../../storage/app/public/'.$dir.'/'.$name);
    }
}
