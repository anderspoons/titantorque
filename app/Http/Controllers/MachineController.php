<?php

namespace App\Http\Controllers;

use App\DataTables\MachineDataTable;
use Illuminate\Http\Request;
use App\Http\Requests\CreateMachineRequest;
use App\Http\Requests\UpdateMachineRequest;
use App\Repositories\MachineRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Machine;

class MachineController extends AppBaseController
{
    /** @var  MachineRepository */
    private $machineRepository;

    public function __construct(MachineRepository $machineRepo)
    {
        $this->machineRepository = $machineRepo;
    }

    /**
     * Display a listing of the Machine.
     *
     * @param MachineDataTable $machineDataTable
     * @return Response
     */
    public function index(MachineDataTable $machineDataTable)
    {
        return $machineDataTable->render('machines.index');
    }

    /**
     * Show the form for creating a new Machine.
     *
     * @return Response
     */
    public function create()
    {
        return view('machines.create');
    }

    /**
     * Store a newly created Machine in storage.
     *
     * @param CreateMachineRequest $request
     *
     * @return Response
     */
    public function store(CreateMachineRequest $request)
    {
        $input = $request->all();

        $machine = $this->machineRepository->create($input);

        Flash::success('Machine saved successfully.');

        return redirect(route('machines.index'));
    }

    /**
     * Display the specified Machine.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $machine = $this->machineRepository->findWithoutFail($id);

        if (empty($machine)) {
            Flash::error('Machine not found');

            return redirect(route('machines.index'));
        }

        return view('machines.show')->with('machine', $machine);
    }

    /**
     * Show the form for editing the specified Machine.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $machine = $this->machineRepository->findWithoutFail($id);

        if (empty($machine)) {
            Flash::error('Machine not found');

            return redirect(route('machines.index'));
        }

        return view('machines.edit')->with('machine', $machine);
    }

    /**
     * Update the specified Machine in storage.
     *
     * @param  int              $id
     * @param UpdateMachineRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMachineRequest $request)
    {
        $machine = $this->machineRepository->findWithoutFail($id);

        if (empty($machine)) {
            Flash::error('Machine not found');

            return redirect(route('machines.index'));
        }

        $machine = $this->machineRepository->update($request->all(), $id);

        Flash::success('Machine updated successfully.');

        return redirect(route('machines.index'));
    }

    /**
     * Remove the specified Machine from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $machine = $this->machineRepository->findWithoutFail($id);

        if (empty($machine)) {
            Flash::error('Machine not found');

            return redirect(route('machines.index'));
        }

        $this->machineRepository->delete($id);

        Flash::success('Machine deleted successfully.');

        return redirect(route('machines.index'));
    }



    public function select2(Request $request) {
        $machines = Machine::orderBy('name', 'ASC');
        if($request->has('query')){
            $machines->where('name', 'like', '%'.$request->input('query').'%');
        }
        $results = $machines->get();
        $formatted_list = array();
        foreach($results as $machine){
            $formatted_list[] = array(
                'id' => $machine->id,
                'text' =>  $machine->name
            );
        }
        return Response::json(array('results' => $formatted_list));
    }
}
