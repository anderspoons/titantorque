<?php

namespace App\Http\Controllers;

use App\DataTables\AssemblyDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAssemblyRequest;
use App\Http\Requests\UpdateAssemblyRequest;
use App\Repositories\AssemblyRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class AssemblyController extends AppBaseController
{
    /** @var  AssemblyRepository */
    private $assemblyRepository;

    public function __construct(AssemblyRepository $assemblyRepo)
    {
        $this->assemblyRepository = $assemblyRepo;
    }

    /**
     * Display a listing of the Assembly.
     *
     * @param AssemblyDataTable $assemblyDataTable
     * @return Response
     */
    public function index(AssemblyDataTable $assemblyDataTable)
    {
        return $assemblyDataTable->render('assemblies.index');
    }

    /**
     * Show the form for creating a new Assembly.
     *
     * @return Response
     */
    public function create()
    {
        return view('assemblies.create');
    }

    /**
     * Store a newly created Assembly in storage.
     *
     * @param CreateAssemblyRequest $request
     *
     * @return Response
     */
    public function store(CreateAssemblyRequest $request)
    {
        $input = $request->all();

        $assembly = $this->assemblyRepository->create($input);

        Flash::success('Assembly saved successfully.');

        return redirect(route('assemblies.index'));
    }

    /**
     * Display the specified Assembly.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $assembly = $this->assemblyRepository->findWithoutFail($id);

        if (empty($assembly)) {
            Flash::error('Assembly not found');

            return redirect(route('assemblies.index'));
        }

        return view('assemblies.show')->with('assembly', $assembly);
    }

    /**
     * Show the form for editing the specified Assembly.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $assembly = $this->assemblyRepository->findWithoutFail($id);

        if (empty($assembly)) {
            Flash::error('Assembly not found');

            return redirect(route('assemblies.index'));
        }

        return view('assemblies.edit')->with('assembly', $assembly);
    }

    /**
     * Update the specified Assembly in storage.
     *
     * @param  int              $id
     * @param UpdateAssemblyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAssemblyRequest $request)
    {
        $assembly = $this->assemblyRepository->findWithoutFail($id);

        if (empty($assembly)) {
            Flash::error('Assembly not found');

            return redirect(route('assemblies.index'));
        }

        $assembly = $this->assemblyRepository->update($request->all(), $id);

        Flash::success('Assembly updated successfully.');

        return redirect(route('assemblies.index'));
    }

    /**
     * Remove the specified Assembly from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $assembly = $this->assemblyRepository->findWithoutFail($id);

        if (empty($assembly)) {
            Flash::error('Assembly not found');

            return redirect(route('assemblies.index'));
        }

        $this->assemblyRepository->delete($id);

        Flash::success('Assembly deleted successfully.');

        return redirect(route('assemblies.index'));
    }
}
