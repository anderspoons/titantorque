<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Response;

class UserController extends Controller
{


    public function select2(Request $request) {
        $users = User::orderBy('name', 'ASC');
        if($request->has('query')){
            $users->where('name', 'like', '%'.$request->input('query').'%');
            $users->orWhere('email', 'like', '%'.$request->input('query').'%');
        }
        $results = $users->get();
        $formatted_list = array();
        foreach($results as $user){
            $formatted_list[] = array(
                'id' => $user->id,
                'text' =>  $user->name
            );
        }
        return Response::json(array('results' => $formatted_list));
    }
}
