<?php

namespace App\Http\Controllers;

use App\DataTables\BasketDataTable;
use Illuminate\Http\Request;
use App\Http\Requests\CreateBasketRequest;
use App\Http\Requests\UpdateBasketRequest;
use App\Repositories\BasketRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Support\Facades\Storage;
use App\Models\Basket;

class BasketController extends AppBaseController
{
    /** @var  BasketRepository */
    private $basketRepository;

    public function __construct(BasketRepository $basketRepo)
    {
        $this->basketRepository = $basketRepo;
    }

    /**
     * Display a listing of the Basket.
     *
     * @param BasketDataTable $basketDataTable
     * @return Response
     */
    public function index(BasketDataTable $basketDataTable)
    {
        return $basketDataTable->render('baskets.index');
    }

    /**
     * Show the form for creating a new Basket.
     *
     * @return Response
     */
    public function create()
    {
        return view('baskets.create');
    }

    /**
     * Store a newly created Basket in storage.
     *
     * @param CreateBasketRequest $request
     *
     * @return Response
     */
    public function store(CreateBasketRequest $request)
    {
        $input = $request->all();
        $basket = $this->basketRepository->create($input);

	    if($request->has('pdf')){
		    $path = Storage::putFile('uploads/certs_basket', $request->file('pdf'));
		    $basket->pdf_path = $path;
		    $basket->save();
	    }

	    Flash::success('Basket saved successfully.');
	    return redirect(route('baskets.index'));
    }

    /**
     * Display the specified Basket.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $basket = $this->basketRepository->findWithoutFail($id);

        if (empty($basket)) {
            Flash::error('Basket not found');

            return redirect(route('baskets.index'));
        }

        return view('baskets.show')->with('basket', $basket);
    }

    /**
     * Show the form for editing the specified Basket.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $basket = $this->basketRepository->findWithoutFail($id);

        if (empty($basket)) {
            Flash::error('Basket not found');
            return redirect(route('baskets.index'));
        }

        return view('baskets.edit')->with('basket', $basket);
    }

    /**
     * Update the specified Basket in storage.
     *
     * @param  int              $id
     * @param UpdateBasketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBasketRequest $request)
    {
        $basket = $this->basketRepository->findWithoutFail($id);
        if (empty($basket)) {
            Flash::error('Basket not found');
            return redirect(route('baskets.index'));
        }
        $basket = $this->basketRepository->update($request->all(), $id);

	    if($request->file('pdf')){
		    $path = $request->file('pdf')->storeAs(
			    '/certs_basket', $basket->asset_num.'_'.date('d-m-y').'.pdf'
		    );
		    $basket->pdf_path = $path;
		    $basket->save();
	    }

        Flash::success('Basket updated successfully.');
        return redirect(route('baskets.index'));
    }

    /**
     * Remove the specified Basket from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $basket = $this->basketRepository->findWithoutFail($id);

        if (empty($basket)) {
            Flash::error('Basket not found');

            return redirect(route('baskets.index'));
        }

        $this->basketRepository->delete($id);

        Flash::success('Basket deleted successfully.');

        return redirect(route('baskets.index'));
    }


    public function select2(Request $request) {
        $baskets = Basket::where('available', '=', 1)->orderBy('asset_num', 'ASC');
        if($request->has('query')){
            $baskets->where('identifier', 'like', '%'.$request->input('query').'%');
        }
        $results = $baskets->get();
        $formatted_list = array();
        foreach($results as $basket){
            $cert = false;
            if(strtotime($basket->cert_date) > strtotime('-6 months')){
                $cert = true;
            }

            $formatted_list[] = array(
                'id' => $basket->id,
                'text' =>  'Number: ' . $basket->asset_num.', Length: '.$basket->length,
                'basket_length' =>  $basket->length,
                'asset' =>  $basket->asset_num,
                'cert' => $cert
            );
        }
        if(count($formatted_list) == 0){
            $formatted_list[] = array(
                'id' => '',
                'text' =>  'No free baskets are available!',
            );
        }
        return Response::json(array('results' => $formatted_list));
    }
}
