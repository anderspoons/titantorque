<?php

namespace App\Http\Controllers;

use App\DataTables\WellDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateWellRequest;
use App\Http\Requests\UpdateWellRequest;
use App\Repositories\WellRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;
use App\Models\Well;

class WellController extends AppBaseController {
	/** @var  WellRepository */
	private $wellRepository;

	public function __construct( WellRepository $wellRepo ) {
		$this->wellRepository = $wellRepo;
	}

	/**
	 * Display a listing of the Well.
	 *
	 * @param WellDataTable $wellDataTable
	 *
	 * @return Response
	 */
	public function index( WellDataTable $wellDataTable ) {
		return $wellDataTable->render( 'wells.index' );
	}

	/**
	 * Show the form for creating a new Well.
	 *
	 * @return Response
	 */
	public function create() {
		return view( 'wells.create' );
	}

	/**
	 * Store a newly created Well in storage.
	 *
	 * @param CreateWellRequest $request
	 *
	 * @return Response
	 */
	public function store( CreateWellRequest $request ) {
		$input = $request->all();
		$well = $this->wellRepository->create( $input );
		Flash::success( 'Well saved successfully.' );
		return redirect( route( 'wells.index' ) );
	}

	/**
	 * Display the specified Well.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show( $id ) {
		$well = $this->wellRepository->findWithoutFail( $id );

		if ( empty( $well ) ) {
			Flash::error( 'Well not found' );

			return redirect( route( 'wells.index' ) );
		}

		return view( 'wells.show' )->with( 'rig', $well );
	}

	/**
	 * Show the form for editing the specified Well.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit( $id ) {
        $well = $this->wellRepository->findWithoutFail( $id );
		if ( empty( $well ) ) {
			Flash::error( 'Well not found' );
			return redirect( route( 'wells.index' ) );
		}
		return view( 'wells.edit' )->with( 'well', $well );
	}

	/**
	 * Update the specified Well in storage.
	 *
	 * @param  int             $id
	 * @param UpdateWellRequest $request
	 *
	 * @return Response
	 */
	public function update( $id, UpdateWellRequest $request ) {
		$well = $this->wellRepository->findWithoutFail( $id );

		if ( empty( $well ) ) {
			Flash::error( 'Well not found' );

			return redirect( route( 'wells.index' ) );
		}
		$well = $this->wellRepository->update( $request->all(), $id );
		Flash::success( 'Well updated successfully.' );
		return redirect( route( 'wells.index' ) );
	}

	/**
	 * Remove the specified Well from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy( $id ) {
		$well = $this->wellRepository->findWithoutFail( $id );

		if ( empty( $well ) ) {
			Flash::error( 'Well not found' );

			return redirect( route( 'wells.index' ) );
		}

		$this->wellRepository->delete( $id );

		Flash::success( 'Well deleted successfully.' );

		return redirect( route( 'wells.index' ) );
	}


    public function select2(Request $request) {
        $wells = Well::orderBy('name', 'ASC');
        if($request->has('query')){
            $wells->where('name', 'like', '%'.$request->input('query').'%');
        }
        $results = $wells->get();
        $formatted_list = array();
        foreach($results as $well){
            $formatted_list[] = array(
                'id' => $well->id,
                'text' =>  $well->name
            );
        }
        return Response::json(array('results' => $formatted_list));
    }

}