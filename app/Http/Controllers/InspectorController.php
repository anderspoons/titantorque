<?php

namespace App\Http\Controllers;

use App\DataTables\InspectorDataTable;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\CreateInspectorRequest;
use App\Http\Requests\UpdateInspectorRequest;
use App\Repositories\InspectorRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Inspector;

class InspectorController extends AppBaseController
{
    /** @var  InspectorRepository */
    private $inspectorRepository;

    public function __construct(InspectorRepository $inspectorRepo)
    {
        $this->inspectorRepository = $inspectorRepo;
    }

    /**
     * Display a listing of the Inspector.
     *
     * @param InspectorDataTable $inspectorDataTable
     * @return Response
     */
    public function index(InspectorDataTable $inspectorDataTable)
    {
        return $inspectorDataTable->render('inspectors.index');
    }

    /**
     * Show the form for creating a new Inspector.
     *
     * @return Response
     */
    public function create()
    {
        return view('inspectors.create');
    }

    /**
     * Store a newly created Inspector in storage.
     *
     * @param CreateInspectorRequest $request
     *
     * @return Response
     */
    public function store(CreateInspectorRequest $request)
    {
        $input = $request->all();

        $inspector = $this->inspectorRepository->create($input);

        Flash::success('Inspector saved successfully.');

        return redirect(route('inspectors.index'));
    }

    /**
     * Display the specified Inspector.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $inspector = $this->inspectorRepository->findWithoutFail($id);

        if (empty($inspector)) {
            Flash::error('Inspector not found');

            return redirect(route('inspectors.index'));
        }

        return view('inspectors.show')->with('inspector', $inspector);
    }

    /**
     * Show the form for editing the specified Inspector.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $inspector = $this->inspectorRepository->findWithoutFail($id);

        if (empty($inspector)) {
            Flash::error('Inspector not found');

            return redirect(route('inspectors.index'));
        }

        return view('inspectors.edit')->with('inspector', $inspector);
    }

    /**
     * Update the specified Inspector in storage.
     *
     * @param  int              $id
     * @param UpdateInspectorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInspectorRequest $request)
    {
        $inspector = $this->inspectorRepository->findWithoutFail($id);

        if (empty($inspector)) {
            Flash::error('Inspector not found');

            return redirect(route('inspectors.index'));
        }

        $inspector = $this->inspectorRepository->update($request->all(), $id);

        Flash::success('Inspector updated successfully.');

        return redirect(route('inspectors.index'));
    }

    /**
     * Remove the specified Inspector from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $inspector = $this->inspectorRepository->findWithoutFail($id);

        if (empty($inspector)) {
            Flash::error('Inspector not found');

            return redirect(route('inspectors.index'));
        }

        $this->inspectorRepository->delete($id);

        Flash::success('Inspector deleted successfully.');

        return redirect(route('inspectors.index'));
    }




    public function select2(Request $request) {
        $inspectors = Inspector::orderBy('name', 'ASC');
        if($request->has('query')){
            $inspectors->where('name', 'like', '%'.$request->input('query').'%');
            $inspectors->orWhere('email', 'like', '%'.$request->input('query').'%');
            $inspectors->orWhere('phone', 'like', '%'.$request->input('query').'%');
        }
        $results = $inspectors->get();
        $formatted_list = array();
        foreach($results as $inspector){
            $formatted_list[] = array(
                'id' => $inspector->id,
                'text' =>  $inspector->name
            );
        }
        return Response::json(array('results' => $formatted_list));
    }
}
