<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Models\Job;

class CronController extends Controller
{
    public function index(){
        $jobs = Job::all();
        $this->setDirectories($jobs);
        $this->setStatus($jobs);
    }

    public function test(){
    }

    public function setStatus($jobs){
        foreach ($jobs as $job){
            $invoice = str_replace(' ', '', trim($job->invoice_number));
            $invoice = preg_replace("/[^A-Za-z0-9 ]/", '', $invoice);
            $date = date("Y-m-d H:i:s");
            if(date($job->duedate) < $date && !$job->statuses->contains(2)){
                $job->statuses()->syncWithoutDetaching([6]);
            } else {
                if($job->statuses->contains(6)){
                    $job->statuses()->syncWithoutDetaching([4]);
                }
            }

            if(strlen($invoice) > 0){
                $job->statuses()->detach(4);
                $job->statuses()->syncWithoutDetaching([5]);
            } else {
                if($job->statuses->contains(2)){
                    $job->statuses()->syncWithoutDetaching([4]);
                }
            }
        }
    }


    public function setDirectories($jobs){
        $folders = array();
        for($i = 2013; $i < date('Y')+1; $i++){
            $tmp = Storage::disk('titan')->allDirectories('/'.$i);
            if(count($tmp) > 0){
                $folders[] = $tmp;
            }
        }

        foreach($jobs as $job){
            $job->folder_path = null;
            foreach ($folders as $list){
                foreach ($list as $folder){
                    if(strpos($folder, $job->number_torque)){
                        $job->folder_path = $folder;
                        break 2;
                    }
                }
            }
            $job->save();
        }
    }
}
