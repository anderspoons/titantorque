<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBookingRequest;
use App\Http\Requests\UpdateBookingRequest;
use App\Repositories\BookingRepository;
use Illuminate\Http\Request;
use App\Models\Booking;
use Calendar;
use Laracasts\Flash\Flash;

class BookingController extends Controller
{
    /** @var  JobRepository */
    private $bookingRepository;

    public function __construct(BookingRepository $bookingRepo)
    {
        $this->bookingRepository = $bookingRepo;
    }


    public function index()
    {
        $eloquentEvent = Booking::all();
        $calendar      = Calendar::addEvents($eloquentEvent, array())
                         ->setOptions(array(
                             'header'       => array(
                                 'left'   => 'prev,next today',
                                 'center' => 'title',
                                 'right'  => 'month, agendaWeek, agendaDay'
                             ),
                             'firstDay'     => 1,
                             'hiddenDays'   => [0],
                             'minTime'      => "08:00:00",
                             'maxTime'      => "20:00:00",
                             'allDaySlot'   => false,
                             'editable'     => false,
                             'defaultView'  => 'agendaWeek',
                             'selectable'   => false,
                             'selectHelper' => false,
                             'droppable'    => false,
                             'timeFormat'   => array(
                                 'month' => 'H:mm',
                                 ''      => 'H:mm-{H:mm}'
                             ),
                         ))
                         ->setCallbacks(array(
                                 'viewRender'  => 'function() { try { setTimeline(); } catch(err) {}}',
                                 'eventRender' => 'function(event, element) { element.find(".fc-content").append("<br/>Notes: " + event.note); element.find(".fc-content").parent().attr("data-toggle", "tooltip"); element.find(".fc-content").parent().attr("title", event.note); }',
                                 'eventClick' => 'function(event, element) { window.open("http://operations.titantorque.co.uk/bookings/"+event.id+"/edit/"); }'
                             )
                         );

        return view('bookings.index')->with('calendar', $calendar);
    }

    public function create()
    {
        return view('bookings.create');
    }

    public function store(CreateBookingRequest $request)
    {
        $bopoking = $this->bookingRepository->create($request->all());

        Flash::success('Booking saved successfully.');

        return redirect(url('bookings'));
    }

    public function update($id, UpdateBookingRequest $request) {
        $booking = $this->bookingRepository->findWithoutFail( $id );

        if ( empty( $booking ) ) {
            Flash::error( 'Booking not found' );
            return redirect(url('bookings'));
        }
        $booking = $this->bookingRepository->update( $request->all(), $id );
        Flash::success( 'Booking updated successfully.' );
        return redirect(url('bookings'));
    }

    /**
     * Remove the specified Well from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy( $id ) {
        $booking = $this->bookingRepository->findWithoutFail( $id );

        if ( empty( $booking ) ) {
            Flash::error( 'Booking not found' );
            return redirect(url('bookings'));
        }

        $this->bookingRepository->delete( $id );
        Flash::success( 'Booking deleted successfully.' );
        return redirect(url('bookings'));
    }

    /**
     * Show the form for editing the specified Well.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit( $id ) {
        $booking = $this->bookingRepository->findWithoutFail( $id );
        if ( empty( $booking ) ) {
            Flash::error( 'Booking not found' );
            return redirect(url('bookings'));
        }
        return view( 'bookings.edit' )->with( 'booking', $booking );
    }
}
