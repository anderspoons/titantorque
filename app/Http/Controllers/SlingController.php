<?php

namespace App\Http\Controllers;

use App\DataTables\SlingDataTable;
use Illuminate\Http\Request;
use App\Http\Requests\CreateSlingRequest;
use App\Http\Requests\UpdateSlingRequest;
use App\Repositories\SlingRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Capacity;
use App\Models\Sling;

class SlingController extends AppBaseController
{
    /** @var  SlingRepository */
    private $slingRepository;

    public function __construct(SlingRepository $slingRepo)
    {
        $this->slingRepository = $slingRepo;
    }

    /**
     * Display a listing of the Sling.
     *
     * @param SlingDataTable $slingDataTable
     * @return Response
     */
    public function index(SlingDataTable $slingDataTable)
    {
        return $slingDataTable->render('slings.index');
    }

    /**
     * Show the form for creating a new Sling.
     *
     * @return Response
     */
    public function create()
    {
    	$capacity = Capacity::where('custom', '=', '0')->get();
        return view('slings.create')->with('capacity', $capacity);
    }

    /**
     * Store a newly created Sling in storage.
     *
     * @param CreateSlingRequest $request
     *
     * @return Response
     */
    public function store(CreateSlingRequest $request)
    {
        $input = $request->all();
        $sling = $this->slingRepository->create($input);
	    if($request->file('pdf')){
		    $path = $request->file('pdf')->storeAs(
			    '/certs_sling', $sling->serial.'_'.date('d-m-y').'.pdf'
		    );
		    $sling->pdf_path = $path;
		    $sling->save();
	    }

	    //bespoke capcity
	    if($request->has('cap') && $request->has('capacity_text')){
			$capacity_check = Capacity::where('capacity', $request->input('capacity_text'))->first();

		    if($capacity_check){
		    	//exists
			    $sling->capacity_id = $capacity_check->id;
			    if($capacity_check-> custom == 1){
				    $sling->bespoke = 1;
			    }
			    $sling->save();
		    } else {
			    $capacity = new Capacity();
			    $capacity->capacity = $request->input('capacity_text');
			    $capacity->custom = 1;
			    $capacity->save();
			    $sling->capacity_id = $capacity->id;
			    $sling->bespoke = 1;
			    $sling->save();
		    }
	    }
        Flash::success('Sling saved successfully.');
        return redirect(route('slings.index'));
    }

    /**
     * Display the specified Sling.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $sling = $this->slingRepository->findWithoutFail($id);

        if (empty($sling)) {
            Flash::error('Sling not found');

            return redirect(route('slings.index'));
        }

        return view('slings.show')->with('sling', $sling);
    }

    /**
     * Show the form for editing the specified Sling.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
	    $capacity = Capacity::where('custom', '=', '0')->get();
        $sling = $this->slingRepository->findWithoutFail($id);
        if (empty($sling)) {
            Flash::error('Sling not found');
            return redirect(route('slings.index'));
        }

        return view('slings.edit')->with('sling', $sling)->with('capacity', $capacity);
    }

    /**
     * Update the specified Sling in storage.
     *
     * @param  int              $id
     * @param UpdateSlingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSlingRequest $request)
    {
        $sling = $this->slingRepository->findWithoutFail($id);
        if (empty($sling)) {
            Flash::error('Sling not found');
            return redirect(route('slings.index'));
        }

        $sling = $this->slingRepository->update($request->all(), $id);

	    if($request->file('pdf')){
		    $path = $request->file('pdf')->storeAs(
			    '/certs_sling', $sling->serial.'_'.date('d-m-y').'.pdf'
		    );
		    $sling->pdf_path = $path;
		    $sling->save();
	    }
	    //bespoke capcity
	    if($request->has('cap') && $request->has('capacity_text')){
		    $capacity_check = Capacity::where('capacity', $request->input('capacity_text'))->first();

		    if($capacity_check){
			    //exists
			    $sling->capacity_id = $capacity_check->id;
			    $sling->save();
		    } else {
			    $capacity = new Capacity();
			    $capacity->capacity = $request->input('capacity_text');
			    $capacity->custom = 1;
			    $capacity->save();
			    $sling->capacity_id = $capacity_check->id;
			    $sling->save();
		    }
	    }
        Flash::success('Sling updated successfully.');
        return redirect(route('slings.index'));
    }

    /**
     * Remove the specified Sling from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sling = $this->slingRepository->findWithoutFail($id);

        if (empty($sling)) {
            Flash::error('Sling not found');

            return redirect(route('slings.index'));
        }

        $this->slingRepository->delete($id);

        Flash::success('Sling deleted successfully.');

        return redirect(route('slings.index'));
    }



    public function select2(Request $request) {
        $slings = Sling::where('quantity', '>', 1)->with('capacity')->orderBy('serial', 'ASC');
        if($request->has('query')){
            $slings->where('serial', 'like', '%'.$request->input('query').'%');
        }
        $results = $slings->get();
        $formatted_list = array();
        foreach($results as $sling){
            $expired = false;
            if(strtotime($sling->created_at) > strtotime('-6 months')){
                $expired = true;
            }
            $capacity = (isset($sling->capacity->capacity)) ? $sling->capacity->capacity : NULL;
            $formatted_list[] = array(
                'id' => $sling->id,
                'text' =>  $sling->serial.' Capacity: '.$capacity.' Quantity: '.$sling->quantity,
                'quantity' =>  $sling->quantity,
                'capacity' =>  $capacity,
                'serial' =>  $sling->serial,
                'expired' => $expired
            );
        }
        return Response::json(array('results' => $formatted_list));
    }


}
