<?php

namespace App\Http\Controllers;

use App\DataTables\OptionsDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateOptionsRequest;
use App\Http\Requests\UpdateOptionsRequest;
use App\Repositories\OptionsRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class OptionsController extends AppBaseController
{
    /** @var  OptionsRepository */
    private $OptionsRepository;

    public function __construct(OptionsRepository $optionsRepo)
    {
        $this->OptionsRepository = $optionsRepo;
    }

    /**
     * Display a listing of the options.
     *
     * @param optionsDataTable $optionsDataTable
     * @return Response
     */
    public function index(OptionsDataTable $optionsDataTable)
    {
        return $optionsDataTable->render('options.index');
    }

    /**
     * Show the form for creating a new options.
     *
     * @return Response
     */
    public function create()
    {
        return view('options.create');
    }

    /**
     * Store a newly created options in storage.
     *
     * @param CreateOptionsRequest $request
     *
     * @return Response
     */
    public function store(CreateOptionsRequest $request)
    {
        $input = $request->all();

        $options = $this->OptionsRepository->create($input);

        Flash::success('Options saved successfully.');

        return redirect(route('options.index'));
    }

    /**
     * Display the specified options.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $options = $this->OptionsRepository->findWithoutFail($id);

        if (empty($options)) {
            Flash::error('Options not found');

            return redirect(route('options.index'));
        }

        return view('options.show')->with('options', $options);
    }

    /**
     * Show the form for editing the specified options.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $options = $this->OptionsRepository->findWithoutFail($id);

        if (empty($options)) {
            Flash::error('Options not found');

            return redirect(route('options.index'));
        }

        return view('options.edit')->with('options', $options);
    }

    /**
     * Update the specified options in storage.
     *
     * @param  int              $id
     * @param UpdateoptionsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateoptionsRequest $request)
    {
        $options = $this->OptionsRepository->findWithoutFail($id);

        if (empty($options)) {
            Flash::error('Options not found');

            return redirect(route('options.index'));
        }

        $options = $this->OptionsRepository->update($request->all(), $id);

        Flash::success('Options updated successfully.');

        return redirect(route('options.index'));
    }

    /**
     * Remove the specified options from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $options = $this->OptionsRepository->findWithoutFail($id);

        if (empty($options)) {
            Flash::error('Options not found');

            return redirect(route('options.index'));
        }

        $this->OptionsRepository->delete($id);

        Flash::success('Options deleted successfully.');

        return redirect(route('options.index'));
    }
}
