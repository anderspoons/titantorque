<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\Job;
use App\Models\Well;
use DB;
use Excel;
use Laracasts\Flash\Flash;

class ReportController extends Controller
{
    public function index()
    {
        return view('reports.index');
    }

    public function statistics()
    {
        $months = DB::table('job')
                    ->select(DB::raw("COUNT(*) AS total, SUM(value) AS value, MONTH(created_at) AS month, YEAR(created_at) AS year"))
                    ->groupby(DB::raw('MONTH(created_at), YEAR(created_at)'))
                    ->orderby('year', 'DESC')
                    ->orderby('month', 'DESC')
                    ->get();

        $jobs = Job::select(DB::raw("number_torque, invoice_number, value, MONTH(created_at) AS month, YEAR(created_at) AS year"))->get();

        return view('reports.statistics')->with('months', $months)->with('jobs', $jobs);
    }

    public function generate(Request $request) {
        $report = $request->input('report-select');
        switch($report) {
            case 'well':
                $this->well_report();
                break;
            case 'job':
                $this->job_report($request);
	            if(!$request->has('job_id')) {
		            Flash::error('You need to select a job to run the report from.');
		            return back()->withInput();
	            }
                break;
            default:
                return 'Stop breaking stuff';
                break;
        }
    }

    public function job_report(Request $request){
	    $jobs = Job::select('*')
		    ->offset($request->input('job_id'))
		    ->limit('99999999')
		    ->get();



	    Excel::create('Job Report - '.Date('dmy'), function ($excel) use ($jobs) {
		    $excel->sheet('jobs', function($sheet) use ($jobs) {
			    $sheet->loadView('reports.export.excel-jobs', array(
			    	'jobs' => $jobs
			    ));
		    });
	    })->download('xlsx');
    }

    public function well_report(){
        $directional_companies = Well::select('directional_id')->where('directional_id', '!=', null)->distinct()->get();
        $data = array();
        foreach($directional_companies as $company){
        	//Get wells assigned to this company
            $wells = $this->wells_by_directional_company($company->directional_id);
	        //Customer name
	        $customer = $this->get_customer_name($company->directional_id);
	        //Loop each well
	        foreach($wells as $well){
		        //Jobs with these wells on them
		        $jobs = $this->jobs_by_well($well);

		        //The array which will hold the row information
		        $line = array();
		        $line['dd'] = $customer;
		        $line['well'] = $this->get_well_name($well);

		        $start_date = time();
		        $end_date = strtotime('1989/02/12');
		        $total_bhas = 0;
		        $total_cons_out = 0;
		        $total_cons_in = 0;
		        $three_total = 0;
		        $six_total = 0;
		        $eight_total = 0;
		        $ten_total = 0;
		        $custom_total = 0;
		        $under_46 = 0;
		        $over_46 = 0;
		        $total_jobs = 0;
		        $rigs = array();
		        $customers = array();

		        foreach($jobs as $job){
			        $total_cons_in = $total_cons_in + $job->connections_in;
			        $total_cons_out = $total_cons_out + $job->connections_out;

			        if(strtotime($job->due_date) < $start_date){
				        $start_date = strtotime($job->due_date);
			        }

			        if(strtotime($job->due_date) > $end_date){
				        $end_date = strtotime($job->due_date);
			        }

			        if(!in_array($job->rig->name, $rigs)){
				        $rigs[] .= $job->rig->name;
			        }
			        if(!in_array($job->customer->name, $customers)){
				        $customers[] .= $job->customer->name;
			        }
			        $total_jobs++;
					$total_bhas = $total_bhas + $job->bhas;


			        $three_total = $three_total + $job->three_slings;
			        $six_total = $six_total + $job->six_slings;
			        $eight_total = $eight_total + $job->eight_slings;
			        $ten_total = $ten_total + $job->ten_slings;
			        $custom_total = $custom_total + $job->bespoke_slings;
		        }

		        $line['operator'] = implode(',', $customers);
		        $line['rig'] = implode(',', $rigs);

		        $line['3_ton'] = $three_total;
		        $line['6_ton'] = $six_total;
		        $line['8_ton'] = $eight_total;
		        $line['10_ton'] = $ten_total;
		        $line['custom_ton'] = $custom_total;
		        $line['under_46'] = $under_46;
		        $line['over_46'] = $over_46;
		        $line['total_bhas'] = $total_bhas;
		        $line['total_jobs'] = $total_jobs;
		        $line['cons_out'] = $total_cons_out;
		        $line['cons_in'] = $total_cons_in;
		        $line['cons_total'] = $total_cons_out + $total_cons_in;
		        $line['start_date'] = Date('d/m/Y', $start_date);
		        $line['end_date'] = Date('d/m/Y', $end_date);
		        $data[] = $line;
	        }
        }

	    Excel::create('Well Report - '.Date('dmy'), function ($excel) use ($data) {
		    $excel->sheet('jobs', function($sheet) use ($data) {
			    $sheet->loadView('reports.export.excel-wells', array(
				    'lines' => $data
			    ));
		    });
	    })->download('xlsx');
    }
    private function get_customer_name($id){
        $customer = Customer::where('id', '=', $id)->first();
        if($customer){
            return $customer->name;
        }
        return false;
    }

    private function get_well_name($id){
        $well = Well::where('id', '=', $id)->first();
        if($well){
            return $well->name;
        }
        return false;
    }

    private function jobs_by_well($well){
        $jobs = Job::where('well_id', '=', $well)->orderBy('created_at')->get();
        if($jobs){
            return $jobs;
        }
        return false;
    }

    private function wells_by_directional_company($dir_comp){
        $wells = Well::select('id')->where('directional_id', '=', $dir_comp)->get();
        if($wells){
            $well_ids = [];
            foreach($wells as $well){
                $well_ids[] = $well->id;
            }
            return $well_ids;
        }
        return false;
    }
}
