<?php

namespace App\Http\Controllers;

use App\DataTables\ProtectorDataTable;
use Illuminate\Http\Request;
use App\Http\Requests\CreateProtectorRequest;
use App\Http\Requests\UpdateProtectorRequest;
use App\Repositories\ProtectorRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use App\Models\Protector;


class ProtectorController extends AppBaseController
{
    /** @var  ProtectorRepository */
    private $protectorRepository;

    public function __construct(ProtectorRepository $protectorRepo)
    {
        $this->protectorRepository = $protectorRepo;
    }

    /**
     * Display a listing of the Protector.
     *
     * @param ProtectorDataTable $protectorDataTable
     * @return Response
     */
    public function index(ProtectorDataTable $protectorDataTable)
    {
        return $protectorDataTable->render('protectors.index');
    }

    /**
     * Show the form for creating a new Protector.
     *
     * @return Response
     */
    public function create()
    {
        return view('protectors.create');
    }

    /**
     * Store a newly created Protector in storage.
     *
     * @param CreateProtectorRequest $request
     *
     * @return Response
     */
    public function store(CreateProtectorRequest $request)
    {
        $input = $request->all();

        $protector = $this->protectorRepository->create($input);

        Flash::success('Protector saved successfully.');

        return redirect(route('protectors.index'));
    }

    /**
     * Display the specified Protector.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $protector = $this->protectorRepository->findWithoutFail($id);

        if (empty($protector)) {
            Flash::error('Protector not found');

            return redirect(route('protectors.index'));
        }

        return view('protectors.show')->with('protector', $protector);
    }

    /**
     * Show the form for editing the specified Protector.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $protector = $this->protectorRepository->findWithoutFail($id);

        if (empty($protector)) {
            Flash::error('Protector not found');

            return redirect(route('protectors.index'));
        }

        return view('protectors.edit')->with('protector', $protector);
    }

    /**
     * Update the specified Protector in storage.
     *
     * @param  int              $id
     * @param UpdateProtectorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProtectorRequest $request)
    {
        $protector = $this->protectorRepository->findWithoutFail($id);

        if (empty($protector)) {
            Flash::error('Protector not found');
            return redirect(route('protectors.index'));
        }

        $protector = $this->protectorRepository->update($request->all(), $id);
        Flash::success('Protector updated successfully.');
        return redirect(route('protectors.index'));
    }

    /**
     * Remove the specified Protector from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $protector = $this->protectorRepository->findWithoutFail($id);

        if (empty($protector)) {
            Flash::error('Protector not found');
            return redirect(route('protectors.index'));
        }

        $this->protectorRepository->delete($id);
        Flash::success('Protector deleted successfully.');
        return redirect(route('protectors.index'));
    }

    public function select2(Request $request) {
        $protectors = Protector::where('available', '=', 1)->orderBy('identifier', 'ASC');
        if($request->has('query')){
            $protectors->where('identifier', 'like', '%'.$request->input('query').'%');
        }
        $results = $protectors->get();
        $formatted_list = array();
        foreach($results as $protector){
            $formatted_list[] = array(
                'id' => $protector->id,
                'text' =>  $protector->identifier.' : '.$protector->sizing,
                'sizing' =>  $protector->sizing,
                'identifier' =>  $protector->identifier,
            );
        }
        return Response::json(array('results' => $formatted_list));
    }
}
