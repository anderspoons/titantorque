<?php

namespace App\Http\Controllers;

use App\DataTables\JobDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateJobRequest;
use App\Http\Requests\UpdateJobRequest;
use App\Repositories\JobRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;
use App\Models\Job;
use App\User;
use Auth;
use App\Models\Document;
use Carbon\Carbon;
use App\Models\Customer;
use App\Models\Well;
use App\Models\Rig;
use App\Models\Status;
use App\Models\Contact;
use App\Models\Inspector;
use App\Models\Sling;
use App\Models\Protector;
use App\Models\Basket;
use Illuminate\Support\Facades\Log;

class JobController extends AppBaseController
{
    /** @var  JobRepository */
    private $jobRepository;

    public function __construct(JobRepository $jobRepo)
    {
        $this->jobRepository = $jobRepo;
    }

    /**
     * Display a listing of the Job.
     *
     * @param JobDataTable $jobDataTable
     *
     * @return Response
     */
    public function index(JobDataTable $jobDataTable)
    {
        // check if user has any checked out jobs and delete them
        $user = $this->getUser();
        $checked_out_jobs = Job::where('checked_out_by',$user->id)->forceDelete();

        $customers  = Customer::all();
        $wells      = Well::all();
        $rigs       = Rig::all();
        $statuses   = Status::where('job', '=', 1)->get();
        $contacts   = Contact::all();
        $inspectors = Inspector::all();

        return $jobDataTable->render('jobs.index', array(
            'statuses'  => $statuses,
            'customers'  => $customers,
            'wells'  => $wells,
            'rigs'  => $rigs,
            'contacts'  => $contacts,
            'inspectors'  => $inspectors,
        ));
    }

    /**
     * Show the form for creating a new Job.
     *
     * @return Response
     */
    public function create()
    {
        $user = $this->getUser();
        $job_number = $this->getJobNumber();
        $quote_number = $this->getQuoteNumber();
        $job = $this->jobRepository->create([
            'number_torque'=>$job_number,
            'quote_number'=>$quote_number,
            'checked_out_by'=>$user->id

        ]);
        // $job = new Job;
        $job->number_torque = $job_number;
        $job->quote_number = $quote_number;
        $job->save();

        return view('jobs.create')
            ->with('job_number', $job_number)
            ->with('quote_number', $quote_number)
            ->with('user', $user);
    }

    /**
     * Show the form for creating a new Job.
     *
     * @return Response
     */
    public function cancelJob(Request $request)
    {
        Job::where('number_torque', $request->input('number_torque'))->forceDelete();
        return redirect(route('jobs.index'));
    }

    public function closeJob(Request $request)
    {
        $job = $this->jobRepository->findWithoutFail($request->input('job_id'));
        $job->statuses()->sync([2]);
        $job->save();
        return Response::json(array(
            'success' => true,
            'message' => $job
        ));
    }

    public function getJobNumber()
    {
        $job_last = Job::orderby('number_torque', 'desc')->first();
        if ($job_last) {
            $str = intval(substr($job_last->number_torque, 5)) + 1;

            return 'TTS' . date('y') . sprintf('%04d', $str);
        } else {
            return 'TTS' . date('y') . '0001';
        }
    }

    public function getQuoteNumber()
    {
        $job_last = Job::orderby('quote_number', 'desc')->first();
        if ($job_last) {
            $str = intval(substr($job_last->number_torque, 5)) + 1;
            return 'TTQ' . date('y') . sprintf('%04d', $str);
        } else {
            return 'TTQ' . date('y') . '0001';
        }
    }

    public function getUser()
    {
        return User::where('id', Auth::user()->id)->first();
    }

    /**
     * Display the specified Job.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $job = Job::where('id', $id)
                  ->with('rig')
                  ->with('well')
                  ->with('operator')
                  ->with('manager')
                  ->with('payee')
                  ->with('baskets')
                  ->with('slings')
                  ->with('protectors')
                  ->with('customer')->get();

        if (empty($job)) {
            Flash::error('Job not found');

            return redirect(route('jobs.index'));
        }

        return view('jobs.show')->with('job', $job[0]);
    }

    /**
     * Show the form for editing the specified Job.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $job = $this->jobRepository->findWithoutFail($id);
        if (empty($job)) {
            Flash::error('Job not found');
            return redirect(route('jobs.index'));
        }
        Log::info(print_r($job, true));

        return view('jobs.edit')->with('job', $job);
    }

    /**
     * Update the specified Job in storage.
     *
     * @param  int             $id
     * @param UpdateJobRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJobRequest $request) {
        $job = $this->jobRepository->findWithoutFail($id);
        if (empty($job)) {
            Flash::error('Job not found');
            return redirect(route('jobs.index'));
        }
        $input = $request->all();

        $job   = $this->jobRepository->update($input, $id);
        $baskets = $this->process_baskets($input);
        $protectors = $this->process_protectors($input);

        if(isset($input['status'])&&count($input['status']) > 0){
            $job->statuses()->sync($input['status']);
        }

        if(count($baskets) > 0){
            $job->baskets()->sync($baskets, true);
        } else {
        	foreach($job->baskets() as $basket){
		        $basket->available = 1;
		        $basket->save();
	        }
	        $job->baskets()->detach();
        }

        if(count($protectors) > 0){
            $job->protectors()->sync($protectors);
        } else {
	        foreach($job->protectors() as $protector){
		        $protector->available = 1;
		        $protector->save();
	        }
	        $job->protectors()->detach();
        }

        $this->move_pdfs($id);

        //We need to check if these records have a
        $this->check_available($job);

        if($job->imported == 0 && $job->folder_path != null){
            $documents = new Document();
            if(isset($input['cargo_summary'])){
                $documents->updateDocument('CargoSummary', $job->id);
            }
            if(isset($input['commercial_invoice'])){
                $documents->updateDocument('CommercialInvoice', $job->id);
            }
            $documents->updateDocument('WorkTicket', $job->id);
            $documents->updateDocument('JobTracker', $job->id);
            $documents->updateDocument('BreakoutChecklist', $job->id);
            $documents->updateDocument('MakeupChecklist', $job->id);
            $documents->updateDocument('ManagerChecklist', $job->id);
        }
        $job->save();
        Flash::success('Job updated successfully.');
        return view('jobs.edit')->with('job', $job);
    }

	public function check_available($job){
        //Baskets
        foreach($job->baskets()->get() as $basket){
            if($basket->pivot->date_in != null){
                $available = Basket::find($basket->id)->first();
                $available->available = 0;
                if($basket->pivot->date_in <= date('Y-m-d H:i:s')){
                    $available->available = 1;
                }
                $available->save();
            }
        }

        //Protectors
        foreach($job->protectors()->get() as $protector){
            if($protector->pivot->date_in != null){
                $available = Protector::find($protector->id)->first();
                $available->available = 0;
                if($protector->pivot->date_in <= date('Y-m-d H:i:s')){
                    $available->available = 1;
                }
                $available->save();
            }
        }
    }

    public function process_baskets($data)
    {
        $processed = array();
        if (isset($data['basket'])) {
            foreach ($data['basket'] as $basket) {
	            if(isset($basket['id'])){
	                $tmp = array(
	                    'loaded'   => 0,
	                    'unloaded' => 0,
	                );
	                if (isset($basket['loaded'])) {
	                    $tmp['loaded'] = 1;
	                }
	                if (isset($basket['unloaded'])) {
	                    $tmp['unloaded'] = 1;
	                }
	                if (isset($basket['days_out'])) {
	                    $tmp['days_out'] = $basket['days_out'];
	                } else {
		                $tmp['days_out'] = 0;
	                }
	                if (isset($basket['date_out']) && strlen($basket['date_out']) > 0) {
	                    $tmp['date_out'] = $basket['date_out'];
	                } else {
		                $tmp['date_out'] = null;
	                }
	                if (isset($basket['date_in']) && strlen($basket['date_in']) > 0) {
	                    $tmp['date_in'] = $basket['date_in'];
	                } else {
		                $tmp['date_out'] = null;
	                }
	                if (isset($basket['note'])) {
	                    $tmp['note'] = $basket['note'];
	                }
	                $processed[$basket['id']] = $tmp;
                }
            }
        }
        return $processed;
    }

    public function move_pdfs($job_id){
        $job = Job::where('id', '=', $job_id)->with('baskets')->first();

        foreach($job->baskets as $basket){
            $pdf = new Document();
            if(strlen($basket->pdf_path) > 0){
            	if(!$pdf->movePdf($basket->pdf_path, $job->number_torque)){
		            Flash::error('Saved but failed to move a PDF certificate.');
	            }
            }
        }
    }

    public function process_protectors($data)
    {
        $processed = array();
        if (isset($data['protector'])) {
            foreach ($data['protector'] as $protector) {
            	if(isset($protector['id'])){
		            $tmp = array();
		            $tmp['days_out'] = $protector['days_out'];
		            if (isset($protector['date_out']) && strlen($protector['date_out']) > 0) {
			            $tmp['date_out'] = $protector['date_out'];
		            } else {
			            $tmp['date_out'] = null;
		            }
		            if (isset($protector['date_in']) && strlen($protector['date_in']) > 0) {
			            $tmp['date_in'] = $protector['date_in'];
		            } else {
			            $tmp['date_out'] = null;
		            }
		            $processed[$protector['id']] = $tmp;
	            }
            }
        }

        return $processed;
    }
    /**
     * Store a newly created Job in storage.
     *
     * @param CreateJobRequest $requestd
     *
     * @return Response
     */
    public function store(CreateJobRequest $request)
    {
        $input = $request->all();
        $makes = (int)$input['makes_api'] + (int)$input['makes_premium'];
        $breaks = (int)$input['breaks_api'] + (int)$input['breaks_premium'];
        $id = Job::where('number_torque',$input['number_torque'])->first()->id;
        $job = $this->jobRepository->update($input, $id);
        $job->checked_out_by = NULL;
        // $job->checked_out_at = NULL;
        $job->statuses()->syncWithoutDetaching([1]);
        $job->baskets()->sync($this->process_baskets($input));
        $job->protectors()->sync($this->process_protectors($input));

        $documents = new Document();
        if(isset($input['cargo_summary'])){
            $documents->createDocument('CargoSummary', $job->id);
        }
        if(isset($input['commercial_invoice'])){
            $documents->createDocument('CommercialInvoice', $job->id);
        }
        $documents->createDocument('WorkTicket', $job->id);
        $documents->createDocument('JobTracker', $job->id);
        if ($makes>0) {
            $documents->createDocument('MakeupChecklist', $job->id);
        }
        if ($breaks>0) {
            $documents->createDocument('BreakoutChecklist', $job->id);
        }
        $documents->createDocument('ManagerChecklist', $job->id);
        $this->move_pdfs($job->id);

        $job->folder_path = date('Y').'/'.$job->number_torque;
        $job->save();
        Flash::success('Job saved successfully.');

        return redirect(route('jobs.index'));
    }

    /**
     * Remove the specified Job from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $job = $this->jobRepository->findWithoutFail($id);

        if (empty($job)) {
            Flash::error('Job not found');

            return redirect(route('jobs.index'));
        }

        $this->jobRepository->delete($id);
        Flash::success('Job deleted successfully.');

        return redirect(route('jobs.index'));
    }

    public function documents($id)
    {
        $job = Job::where('id', '=', $id)->first();

        $documents = new Document();
        if(strlen($job->folder_path) > 0 && strpos($job->folder_path, $job->number_torque) !== false){

            if ($job->cargo_summary == 1) {
                $documents->updateDocument('CargoSummary', $job->id);
            }
            if ($job->commercial_invoice == 1) {
                $documents->updateDocument('CommercialInvoice', $job->id);
            }
            $documents->updateDocument('WorkTicket', $job->id);
            $documents->updateDocument('JobTracker', $job->id);
            $documents->updateDocument('BreakoutChecklist', $job->id);
            $documents->updateDocument('MakeupChecklist', $job->id);
            $documents->updateDocument('ManagerChecklist', $job->id);

            Flash::success('Documents regenerated successfully.');
            return redirect(route('jobs.index'));
        }

        if ($job->cargo_summary == 1) {
            $documents->createDocument('CargoSummary', $job->id);
        }
        if ($job->commercial_invoice == 1) {
            $documents->createDocument('CommercialInvoice', $job->id);
        }
        $documents->createDocument('WorkTicket', $job->id);
        $documents->createDocument('JobTracker', $job->id);
        $documents->createDocument('BreakoutChecklist', $job->id);
        $documents->createDocument('MakeupChecklist', $job->id);
        $documents->createDocument('ManagerChecklist', $job->id);

        Flash::success('Documents created successfully.');
        return redirect(route('jobs.index'));
    }

    public function remove_basket(Request $request){
        $job = Job::where('id', '=', $request->input('job_id'))->with('baskets')->first();
        $basket = Basket::where('id', '=', $request->input('id'))->first();
        foreach($job->baskets as $Jbasket){
            if($Jbasket->id == $request->input('id')){
                $job->baskets()->detach($request->input('id'));
                $basket->available = 1;
                if($basket->save()){
                    return Response::json(array(
                        'success' => true,
                        'message' => 'Removed basket from job.'
                    ));
                }
            }
        }
        //Probably only going to get to this point when you add one without saving it.
        return Response::json(array(
            'success' => true,
            'message' => 'Removed basket from job.'
        ));
    }

    public function due_date(Request $request){
        $job = Job::where('id', '=', $request->input('job_id'))->first();
        $job->due_date = Carbon::createFromFormat('d/m/Y', $request->input('due_date'))->format('Y-m-d');
        $job->save();
        
        return Response::json(array(
            'success' => true,
            'message' => 'Due date successfully updated.',
            'data' => ["date"=>$request->input('due_date'),"job_id"=>$request->input('job_id')]
        ));
    }

    public function remove_protector(Request $request){
        $job = Job::where('id', '=', $request->input('job_id'))->with('protectors')->first();
        $protector = Protector::where('id', '=', $request->input('id'))->first();
        foreach($job->protectors as $Jprotector){
            if($Jprotector->id == $request->input('id')){
                $job->protectors()->detach($request->input('id'));
                $protector->available = 1;
                if($protector->save()){
                    return Response::json(array(
                        'success' => true,
                        'message' => 'Removed protector from job.'
                    ));
                }
            }
        }

        //Probably only going to get to this point when you add one without saving it.
        return Response::json(array(
            'success' => true,
            'message' => 'Removed protector from job.'
        ));
    }

    public function remove_sling(Request $request){
        $job = Job::where('id', '=', $request->input('job_id'))->with('slings')->first();
        $sling = Sling::where('id', '=', $request->input('id'))->first();
        foreach($job->slings as $Jsling){
            if($Jsling->id == $request->input('id')){
                $job->slings()->detach($request->input('id'));
                $sling->quantity = $sling->quantity + $Jsling->pivot->quantity;
                if($sling->save()){
                    return Response::json(array(
                        'success' => true,
                        'message' => 'Removed slings from job.'
                    ));
                }
            }
        }
    }

    public function add_sling(Request $request){
        $job = Job::where('id', '=', $request->input('job_id'))->with('slings')->first();
        $sling = Sling::where('id', '=', $request->input('id'))->first();

        if($job->slings->contains($request->input('id'))){
            $quantity = $job->slings->find($request->input('id'));
            $job->slings()->updateExistingPivot($request->input('id'), array('quantity' => $quantity->pivot->quantity + $request->input('quantity')));
            //Remove quantity from sling
            $sling->quantity = $sling->quantity - $request->input('quantity');
            $sling->save();

            return Response::json(array(
                'success' => true,
                'message' => 'Added sling quantity to existing.'
            ));
        }

        $job->slings()->syncWithoutDetaching(array(
            $request->input('id') => array(
                'quantity' => $request->input('quantity')
            )
        ));

        //Remove quantity from sling
        $sling->quantity = $sling->quantity - $request->input('quantity');
        $sling->save();

        $document = new Document();
        if(strlen($sling->pdf_path) > 0){
	        if($document->movePdf($sling->pdf_path, $job->number_torque)){
		        return Response::json(array(
			        'success' => true,
			        'message' => 'Added new slings to job.'
		        ));
	        }
        }

        return Response::json(array(
            'success' => false,
            'message' => 'Problem moving the sling\'s PDF cert to the job folder, check there is one.'
        ));
    }



	public function select2(Request $request) {
		$jobs = Job::orderBy('number_torque', 'DESC');
		if($request->has('query')){
			$jobs->where('number_torque', 'like', '%'.$request->input('query').'%');
		}
		$results = $jobs->get();
		$formatted_list = array();
		foreach($results as $job){
			$formatted_list[] = array(
				'id' => $job->id,
				'text' =>  $job->number_torque,
			);
		}
		return Response::json(array('results' => $formatted_list));
	}

}
