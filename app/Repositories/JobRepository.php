<?php

namespace App\Repositories;

use App\Models\Job;
use InfyOm\Generator\Common\BaseRepository;

class JobRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'number_torque',
        'number_tools',
        'assembly',
        'quote_number',
        'invoice_number',
        'value',
        'po_number',
        'email_contact',
        'cargo_summary',
        'cargo_export_temporary',
        'commercial_invoice',
        'ratchet_used',
        'ratchet_returned',
        'blocks_used',
        'blocks_returned',
        'basket_loaded',
        'basket_unloaded',
        'stenciled_connections',
        'drift_connections',
        'bakerlocked_connections',
        'centralizers_fitted',
        'customer_id',
        'contact_id',
        'operator_id',
        'manager_id',
        'rig_id',
        'inspector_id',
        'well_id',
        'payee_id',
        'due_date',
        'note',
        'custom_connections',
        'make_break_custom',
        'makes_api',
        'breaks_api',
        'makes_premium',
        'breaks_premium',
        'bhas',
        'hl_pennant',
        'insertion',
        'extraction',
        'witnesses',
        'folder_path',
        'hours_workshop',
        'hours_out_of'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Job::class;
    }
}
