<?php

namespace App\Repositories;

use App\Models\Sling;
use InfyOm\Generator\Common\BaseRepository;

class SlingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'capacity_id',
        'bespoke',
        'quantity',
        'serial',
        'pdf_path'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Sling::class;
    }
}
