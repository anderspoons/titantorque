<?php

namespace App\Repositories;

use App\Models\Inspector;
use InfyOm\Generator\Common\BaseRepository;

class InspectorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'phone',
        'email'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Inspector::class;
    }
}
