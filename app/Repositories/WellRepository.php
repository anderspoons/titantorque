<?php

namespace App\Repositories;

use App\Models\Well;
use InfyOm\Generator\Common\BaseRepository;

class WellRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'directional_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Well::class;
    }
}
