<?php

namespace App\Repositories;

use App\Models\Assembly;
use InfyOm\Generator\Common\BaseRepository;

class AssemblyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Assembly::class;
    }
}
