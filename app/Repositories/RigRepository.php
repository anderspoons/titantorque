<?php

namespace App\Repositories;

use App\Models\Rig;
use InfyOm\Generator\Common\BaseRepository;

class RigRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Rig::class;
    }
}
