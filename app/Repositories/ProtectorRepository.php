<?php

namespace App\Repositories;

use App\Models\Protector;
use InfyOm\Generator\Common\BaseRepository;

class ProtectorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'identifier',
        'sizing',
        'available'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Protector::class;
    }
}
