<?php

namespace App\Repositories;

use App\Models\Basket;
use InfyOm\Generator\Common\BaseRepository;

class BasketRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'length',
        'asset_num',
        'pdf_path',
        'cert_date'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Basket::class;
    }
}
