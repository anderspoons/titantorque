<?php

namespace App\DataTables;

use App\Models\Well;
use Form;
use Yajra\Datatables\Services\DataTable;
use App\Models\Customer;

class WellDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('customer_name', function($data){
                // if(isset($data->directional_id)){
                //     $comp = Customer::where('id', '=', $data->directional_id)->first();
                //     return $comp->name;
                // }
                // return '';
                if(isset($data->customer->name)){
                    return $data->customer->name;
                }
                return '';
            })
            ->addColumn('action', 'wells.datatables_actions')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $wells = Well::query()->select("well.*");
        $wells->leftJoin('customer', 'well.directional_id', '=', 'customer.id');
        $wells->orderby('well.name', 'asc');
        return $this->applyScopes($wells);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ],
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'name' => ['name' => 'name', 'data' => 'name'],
            'directional_company' => ['name' => 'customer.name', 'data' => 'customer_name', 'orderable' => true]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'wells';
    }
}
