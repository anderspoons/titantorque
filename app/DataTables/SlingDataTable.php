<?php

namespace App\DataTables;

use App\Models\Sling;
use Form;
use Yajra\Datatables\Services\DataTable;

class SlingDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'slings.datatables_actions')
	        ->addColumn('capacity', function($data) {
		        if($data->capacity){
			        return $data->capacity->capacity;
		        } else {
			        return '';
		        }
	        })
	        ->addColumn('bespoke_tick', function($data) {
		        if($data->bespoke == 1){
			        return '<i style="color:#00a157" class="fa fa-check"></i>';
		        } else {
			        return '<i style="color:#CC0000" class="fa fa-times"></i>';
		        }
	        })
            ->addColumn('pdf_cert', function($data) {
	            if(strlen($data->pdf_path) > 0){
		            return '<a class="btn btn-default btn-xs" href="'.url('file-serve').'/'.$data->pdf_path.'"><i class="fa fa-file-pdf-o"></i></a>';
	            } else {
		            return '';
	            }
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $slings = Sling::query();

        return $this->applyScopes($slings);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ],
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'capacity' => ['name' => 'capacity', 'data' => 'capacity', 'orderable' => false],
            'pdf_certificate' => ['name' => 'pdf_cert', 'data' => 'pdf_cert', 'orderable' => false],
            'bespoke' => ['name' => 'bespoke_tick', 'data' => 'bespoke_tick', 'orderable' => false],
            'quantity' => ['name' => 'quantity', 'data' => 'quantity'],
            'serial' => ['name' => 'serial', 'data' => 'serial'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'slings';
    }
}
