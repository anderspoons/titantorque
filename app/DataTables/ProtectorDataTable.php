<?php

namespace App\DataTables;

use App\Models\Protector;
use Form;
use Yajra\Datatables\Services\DataTable;

class ProtectorDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('is_available', function($data) {
                if($data->available == 1){
                    return '<i style="color:#00a157" class="fa fa-check"></i>';
                } else {
                    return '<i style="color:#CC0000" class="fa fa-times"></i>';
                }
            })
            ->addColumn('action', 'protectors.datatables_actions')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $protectors = Protector::query();

        return $this->applyScopes($protectors);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ],
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'identifier' => ['name' => 'identifier', 'data' => 'identifier'],
            'sizing' => ['name' => 'sizing', 'data' => 'sizing'],
            'available' => ['name' => 'is_available', 'data' => 'is_available']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'protectors';
    }
}
