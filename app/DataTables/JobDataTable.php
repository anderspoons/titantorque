<?php

namespace App\DataTables;

use App\Models\Job;
use App\Models\Options;
use Form;
use Yajra\Datatables\Services\DataTable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class JobDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        $urlquery = $this->proper_parse_str(urldecode($_SERVER['QUERY_STRING']));

        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', function($data){
                $link = '';
                if(strlen($data->folder_path) > 0){
                    $link = '<a href="file://\\\\TIT01\\Titan Torque\\Jobs\\'.$data->folder_path.'" class="btn btn-default btn-xs">
                        <i class="fa fa-folder-open"></i>
                    </a>';
                }

                return '<div class="btn-group">
                    <a href="'.route('jobs.show', $data->id).'" class="btn btn-default btn-xs">
                        <i class="glyphicon glyphicon-eye-open"></i>
                    </a>
                    <a href="'.route('jobs.edit', $data->id).'" class="btn btn-default btn-xs">
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>
                    '.$link.'
                    <a href="#" class="btn btn-default btn-xs jobs-close-job-button" data-job-id="'.$data->id.'" title="Close this job">
                        <i class="glyphicon glyphicon-ok"></i>
                    </a>
                </div>';

            })
            ->addColumn('colour', function($data){
                //        job.makes_api,
                //        job.makes_premium,
                //        job.breaks_api,
                //        job.breaks_premium,
	            //$colours

	            $colour_make = Options::where('option_name', '=', 'colour_make')->first();
	            $colour_break = Options::where('option_name', '=', 'colour_break')->first();
	            $colour_both = Options::where('option_name', '=', 'colour_both')->first();

	            $makes = false;
	            if($data->makes_api > 0 || $data->makes_premium > 0){
	            	$makes = true;
	            }
	            $breaks = false;
	            if($data->breaks_api > 0 || $data->breaks_premium > 0){
		            $breaks = true;
	            }
	            if($breaks && $makes){
					return $colour_both->option_value;
	            }
	            if($breaks && !$makes){
	            	return $colour_break->option_value;
	            }
	            if(!$breaks && $makes){
		            return $colour_make->option_value;
	            }
	            return '#FFF';
            })
            ->addColumn('date_format', function($data){
                return "<span class=\"due_date_datepicker\" id=\"job_{$data->id}\">".Carbon::createFromFormat('Y-m-d', $data->due_date)->format('d/m/Y')."</span>";
            })
            ->addColumn('rig_name', function($data){
                if(isset($data->rig->name)){
                    return $data->rig->name;
                }
                return '';
            })
            ->addColumn('status', function($data){
                $str = '';
                foreach($data->statuses as $status){
                    $str .= '<span class="btn-xs '.$status->style.'">'.$status->name.'</span><br/>';
                }
                return $str;
            })->filter(function($query) use ($urlquery){

                if (!empty($urlquery['customer_filter[]'] ) ) {
                    $customers = (array)$urlquery['customer_filter[]'];
                    $query->whereIn( 'job.customer_id', $customers);
                }
                if (!empty($urlquery['inspector_filter[]'] ) ) {
                    $inspectors = (array)$urlquery['inspector_filter[]'];
                    $query->whereIn( 'job.inspector_id', $inspectors);
                }
                if (!empty($urlquery['rig_filter[]'] ) ) {
                    $rigs = (array)$urlquery['rig_filter[]'];
                    $query->whereIn( 'job.rig_id', $rigs);
                }
                if (!empty($urlquery['well_filter[]'] ) ) {
                    $wells = (array)$urlquery['well_filter[]'];
                    $query->whereIn( 'job.well_id', $wells);
                }
                if (!empty($urlquery['contact_filter[]'] ) ) {
                    $contacts = (array)$urlquery['contact_filter[]'];
                    $query->whereIn( 'job.contact_id', $contacts);
                }
                if (!empty($urlquery['status_filter[]'] ) ) {
                    $statuses = (array)$urlquery['status_filter[]'];
                    $query->leftJoin( 'job_status', 'job.id', '=', 'job_status.job_id' )
                          ->whereIn( 'job_status.status_id', $statuses)
                          ->leftJoin( 'status', 'job_status.status_id', '=', 'status.id' );
                } else {
                    if(empty($urlquery['search[value]'])){
                        $query->leftJoin( 'job_status', 'job.id', '=', 'job_status.job_id')
                              ->where('job_status.status_id', '=', '1');
                    }
                }

                if(!empty($urlquery['search[value]'])){
                    $query->where(function ($q) use ($urlquery){
                        $q->whereRaw('(REPLACE(`job`.`number_torque`, \' \', \'\') LIKE REPLACE(\'%'.$urlquery['search[value]'].'%\', \' \', \'\') or 
	                    REPLACE(`job`.`number_tools`, \' \', \'\') LIKE REPLACE(\'%'.$urlquery['search[value]'].'%\', \' \', \'\') or 
	                    REPLACE(`job`.`quote_number`, \' \', \'\') LIKE REPLACE(\'%'.$urlquery['search[value]'].'%\', \' \', \'\'))');
                    });
                }
            })
            ->order(function($query) {
                $query->orders = null;
                $dir = request()->get('order')[0]['dir'];
                switch (request()->get('order')[0]['column']) {
                    case '0':
                        $col = 'job.number_torque';
                        break;
                    
                    case '1':
                        $query->leftJoin('customer as cust', 'job.customer_id', '=', 'cust.id');
                        $col = 'cust.name';
                        break;
                    
                    case '2':
                        $query->leftJoin('rig', 'job.rig_id', '=', 'rig.id');
                        $col = 'rig.name';
                        break;
                    
                    case '3':
                        $col = 'job.assembly';
                        break;
                    
                    case '4':
                        $query->leftJoin('well', 'job.well_id', '=', 'well.id');
                        $col = 'well.name';
                        break;
                    
                    case '6':
                        $col = 'job.due_date';
                        break;
                    
                    default:
                        $col = 'job.number_torque';
                        break;
                }
                $query->orderBy($col, $dir);
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {

        $jobs = Job::query()->select(
        'job.id',
        'job.number_torque',
        'job.number_tools',
        'job.quote_number',
        'job.note',
        'job.assembly',
        'job.well_id',
        'job.customer_id',
        'job.rig_id',
        'job.folder_path',
        'job.due_date',
        'job.makes_api',
        'job.makes_premium',
        'job.breaks_api',
        'job.breaks_premium',
        'job.po_number'
        // 'cust.name'
        )->distinct();

        // $jobs->leftJoin('customer as cust', 'job.customer_id', '=', 'cust.id');
        
        $jobs->with('customer');
        $jobs->with('rig');
        $jobs->with('well');
        $jobs->with('statuses');
        $jobs->orderby('due_date', 'asc');

        // $jobs->get();

        return $this->applyScopes($jobs);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax('')
                    ->parameters([
                        'dom'     => 'Bfrtip',
                        'scrollX' => false,
                        'buttons' => [
                            'print',
                            'reset',
                            'reload',
                            [
                                'extend'  => 'collection',
                                'text'    => '<i class="fa fa-download"></i> Export',
                                'buttons' => [
                                    'csv',
                                    'excel',
                                    'pdf',
                                ],
                            ],
                            'colvis'
                        ]
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'colour' => ['name' => 'colour', 'data' => 'colour', 'visible' => false],
            'number_torque' => ['name' => 'number_torque', 'data' => 'number_torque'],
            'customer' => ['name' => 'customer.name', 'data' => 'customer.name', 'defaultContent' => ' ', 'searchable' => false],
            'rig' => ['name' => 'rig.name', 'data' => 'rig.name', 'defaultContent' => ' ', 'searchable' => false],
            'assembly' => ['name' => 'assembly', 'data' => 'assembly'],
            'well' => ['name' => 'well.name', 'data' => 'well.name', 'defaultContent' => ' ', 'searchable' => false],
            'status' => ['name' => 'status', 'data' => 'status', 'defaultContent' => ' ', 'orderable' => false, 'searchable' => false],
            'due_date' => ['name' => 'ordering', 'data' => 'due_date'],
            'note' => ['name' => 'note', 'data' => 'note']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'jobs';
    }

    private function proper_parse_str($str) {
        # result array
        $arr = array();
        # split on outer delimiter
        $pairs = explode('&', $str);
        # loop through each pair
        foreach ($pairs as $i) {
            # split into name and value
            list($name,$value) = explode('=', $i, 2);
            # if name already exists
            if( isset($arr[$name]) ) {
                # stick multiple values into an array
                if( is_array($arr[$name]) ) {
                    $arr[$name][] = $value;
                }
                else {
                    $arr[$name] = array($arr[$name], $value);
                }
            }
            # otherwise, simply stick it in a scalar
            else {
                $arr[$name] = $value;
            }
        }
        # return result array
        return $arr;
    }
}
