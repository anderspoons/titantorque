<?php

namespace App\DataTables;

use App\Models\Basket;
use Form;
use Carbon\Carbon;
use Yajra\Datatables\Services\DataTable;

class BasketDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('length', function($data){
                return $data->length."ft";
            })
            ->addColumn('action', 'baskets.datatables_actions')
            ->addColumn('is_available', function($data){
                if($data->available == '1'){
                    return '<i style="color:#00a157" class="fa fa-check"></i>';
                } else {
                    return '<i style="color:#CC0000" class="fa fa-times"></i>';
                }
            })
            ->addColumn('pdf_cert', function($data) {
                if(strlen($data->pdf_path) > 0){
                    return '<a class="btn btn-default btn-xs" href="'.url('file-serve').'/'.$data->pdf_path.'"><i class="fa fa-file-pdf-o"></i></a>';
                } else {
                    return '';
                }
            })
            ->addColumn('cert_date', function($data) {
                return Carbon::createFromFormat('m/d/Y', $data->cert_date)->format('d/m/Y');
            	// return date("d/m/Y", strtotime($data->cert_date));
            })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $baskets = Basket::query();

        return $this->applyScopes($baskets);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ],
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'length' => ['name' => 'length', 'data' => 'length'],
            'max_gross' => ['name' => 'max_gross', 'data' => 'max_gross'],
            'asset_num' => ['name' => 'asset_num', 'data' => 'asset_num'],
            'pdf_certificate' => ['name' => 'pdf_cert', 'data' => 'pdf_cert', 'orderable' => false],
            'available' => ['name' => 'is_available', 'data' => 'is_available', 'orderable' => false],
            'cert_date' => ['name' => 'cert_date', 'data' => 'cert_date']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'baskets';
    }
}
