<?php

namespace App\DataTables;

use App\Models\Status;
use Form;
use Yajra\Datatables\Services\DataTable;

class StatusDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('is_job', function ($data) {
                return $this->get_tick($data->job);
            })
            ->addColumn('is_basket', function ($data) {
                return $this->get_tick($data->basket);
            })
            ->addColumn('is_protector', function ($data) {
                return $this->get_tick($data->protector);
            })
            ->addColumn('is_automatic', function ($data) {
                return $this->get_tick($data->automatic);
            })
            ->addColumn('action', 'statuses.datatables_actions')
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $statuses = Status::query();

        return $this->applyScopes($statuses);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%'])
            ->ajax('')
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'buttons' => [
                    'print',
                    'reset',
                    'reload',
                    [
                         'extend'  => 'collection',
                         'text'    => '<i class="fa fa-download"></i> Export',
                         'buttons' => [
                             'csv',
                             'excel',
                             'pdf',
                         ],
                    ],
                    'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'name' => ['name' => 'name', 'data' => 'name'],
            'job' => ['name' => 'is_job', 'data' => 'is_job'],
            'basket' => ['name' => 'is_basket', 'data' => 'is_basket'],
            'protector' => ['name' => 'is_protector', 'data' => 'is_protector'],
            'automatic' => ['name' => 'is_automatic', 'data' => 'is_automatic']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'statuses';
    }


    private function  get_tick($val){
        if ($val == 1) {
            return '<i style="color:#00a157" class="fa fa-check"></i>';
        } else {
            return '<i style="color:#CC0000" class="fa fa-times"></i>';
        }
    }
}
