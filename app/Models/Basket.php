<?php

namespace App\Models;

use Eloquent as Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Collective\Html\Eloquent\FormAccessible;

/**
 * Class Basket
 * @package App\Models
 * @version October 13, 2016, 7:58 am UTC
 */
class Basket extends Model
{
    use SoftDeletes;
	use FormAccessible;

    public $table = 'basket';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'length',
        'max_gross',
        'asset_num',
        'pdf_path',
        'cert_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'length' => 'string',
        'max_gross' => 'string',
        'asset_num' => 'string',
        'pdf_path' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

	public function getPdfPathAttribute($value)
	{
		if(!empty($value)){
			return $value;
		} else {
			return '';
		}
	}

	public function getCertDateAttribute($value)
	{
		return Carbon::parse($value)->format('m/d/Y');
	}

	public function formCertDateAttribute($value)
	{
		return Carbon::parse($value)->format('Y-m-d');
	}
}
