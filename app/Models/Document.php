<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Excel;
use App\Models\Job;
use Response;
use Carbon\Carbon;

class Document extends Model
{
    protected $names = array(
        'WorkTicket'        => 'work_ticket.xlsx',
        'JobTracker'        => 'job_tracker.xlsx',
        'BreakoutChecklist' => 'breakout_checklist.xlsx',
        'MakeupChecklist'   => 'makeup_checklist.xlsx',
        'ManagerChecklist'  => 'project_manager_checklist.xlsx',
        'CommercialInvoice' => 'commercial_invoice.xlsx',
        'CargoSummary'      => 'cargo_summary.xlsx'
    );

    protected $cell_values = array(
        //manager_name = manager_id / user.name by id
        'WorkTicket' => array(
            array(
                'col'   => 'D',
                'row'   => 1,
                'value' => 'number_torque'
            ),
            array(
                'col'      => 'B',
                'row'      => 6,
                'value'    => 'manager_name',
            ),
            array(
                'col'   => 'B',
                'row'   => 7,
                'value' => 'created_at'
            ),
            array(
                'col'   => 'B',
                'row'   => 8,
                'value' => 'quote_number'
            ),
            array(
                'col'   => 'B',
                'row'   => 9,
                'value' => 'eu_number'
            ),
            array(
                'col'      => 'E',
                'row'      => 6,
                'value'    => 'customer_name',
            ),
            array(
                'col'      => 'E',
                'row'      => 7,
                'value'    => 'rig_name',
            ),
            array(
                'col'      => 'E',
                'row'      => 8,
                'value'    => 'well_name',
            ),
            array(
                'col'   => 'E',
                'row'   => 9,
                'value' => 'assembly'
            ),
            array(
                'col'      => 'G',
                'row'      => 6,
                'value'    => 'contact_name',
            ),
            array(
                'col'   => 'G',
                'row'   => 7,
                'value' => 'po_number'
            ),
            array(
                'col'      => 'G',
                'row'      => 8,
                'value'    => 'payee_name',
            ),
            array(
                'col'   => 'F',
                'row'   => 12,
                'value' => 'witnesses'
            ),
            array(
                'col'   => 'F',
                'row'   => 32,
                'value' => 'total_connections',
            ),
            array(
                'col'   => 'F',
                'row'   => 33,
                'value' => 'total_slings',
            ),
            array(
                'col'   => 'F',
                'row'   => 34,
                'value' => 'total_baskets'
            ),

            array(
                'col'   => 'H',
                'row'   => 31,
                'value' => 'total_hours'
            ),
            array(
                'col'   => 'H',
                'row'   => 32,
                'value' => 'ratchet_used'
            ),
            array(
                'col'   => 'H',
                'row'   => 33,
                'value' => 'blocks_used'
            ),
            array(
                'col'   => 'H',
                'row'   => 34,
                'value' => 'insertion'
            ),
        ),


        'JobTracker' => array(
            array(
                'col'   => 'A',
                'row'   => 4,
                'value' => 'number_torque'
            ),
            array(
                'col'   => 'B',
                'row'   => 7,
                'value' => 'created_at'
            ),
            array(
                'col'   => 'B',
                'row'   => 8,
                'value' => 'manager_name'
            ),
            array(
                'col'   => 'B',
                'row'   => 9,
                'value' => 'customer_name'
            ),
            array(
                'col'   => 'B',
                'row'   => 10,
                'value' => 'rig_name'
            ),
            array(
                'col'   => 'B',
                'row'   => 11,
                'value' => 'assembly'
            ),
            array(
                'col'   => 'E',
                'row'   => 7,
                'value' => 'number_tools'
            ),
            array(
                'col'   => 'E',
                'row'   => 9,
                'value' => 'operator_name'
            ),
            array(
                'col'   => 'E',
                'row'   => 10,
                'value' => 'well_name'
            ),
            array(
                'col'   => 'E',
                'row'   => 11,
                'value' => 'bhas'
            ),
            array(
                'col'   => 'B',
                'row'   => 14,
                'value' => 'api_connections'
            ),
            array(
                'col'   => 'B',
                'row'   => 15,
                'value' => 'makes_api'
            ),
            array(
                'col'   => 'B',
                'row'   => 16,
                'value' => 'breaks_api'
            ),
            array(
                'col'   => 'E',
                'row'   => 14,
                'value' => 'premium_connections'
            ),
            array(
                'col'   => 'E',
                'row'   => 15,
                'value' => 'makes_premium'
            ),
            array(
                'col'   => 'E',
                'row'   => 16,
                'value' => 'breaks_premium'
            ),
            array(
                'col'   => 'B',
                'row'   => 17,
                'value' => 'insertion'
            ),
            array(
                'col'   => 'E',
                'row'   => 17,
                'value' => 'extraction'
            ),
            array(
                'col'   => 'B',
                'row'   => 18,
                'value' => 'three_slings'
            ),
            array(
                'col'   => 'B',
                'row'   => 19,
                'value' => 'eight_slings'
            ),
            array(
                'col'   => 'B',
                'row'   => 20,
                'value' => 'bespoke_slings'
            ),
            array(
                'col'   => 'E',
                'row'   => 18,
                'value' => 'six_slings'
            ),
            array(
                'col'   => 'E',
                'row'   => 19,
                'value' => 'ten_slings'
            ),
            array(
                'col'   => 'B',
                'row'   => 22,
                'value' => 'small_basket_total'
            ),
            array(
                'col'   => 'B',
                'row'   => 23,
                'value' => 'large_basket_total'
            ),
            array(
                'col'   => 'B',
                'row'   => 24,
                'value' => 'protector_total'
            ),
            array(
                'col'   => 'B',
                'row'   => 25,
                'value' => 'ratchet_used'
            ),
            array(
                'col'   => 'B',
                'row'   => 26,
                'value' => 'blocks_used'
            ),
            array(
                'col'   => 'B',
                'row'   => 27,
                'value' => 'basket_load_total'
            ),
            array(
                'col'   => 'B',
                'row'   => 28,
                'value' => 'hl_pennant'
            ),
            array(
                'col'   => 'B',
                'row'   => 29,
                'value' => 'stenciled_connections'
            ),
            array(
                'col'   => 'E',
                'row'   => 30,
                'value' => 'drift_connections'
            ),
            array(
                'col'   => 'E',
                'row'   => 29,
                'value' => 'bakerlocked_connections'
            ),
            array(
                'col'   => 'B',
                'row'   => 30,
                'value' => 'centralizers_fitted'
            ),
            array(
                'col'   => 'F',
                'row'   => 34,
                'value' => 'total_hours'
            ),
            array(
                'col'   => 'B',
                'row'   => 32,
                'value' => 'hours_workshop'
            ),
            array(
                'col'   => 'E',
                'row'   => 32,
                'value' => 'total_hours_out_of'
            ),
            array(
                'col'   => 'E',
                'row'   => 22,
                'value' => 'small_basket_days'
            ),
            array(
                'col'   => 'E',
                'row'   => 23,
                'value' => 'large_basket_days'
            ),
            array(
                'col'   => 'E',
                'row'   => 24,
                'value' => 'protector_days'
            ),
            array(
                'col'   => 'E',
                'row'   => 25,
                'value' => 'ratchet_returned'
            ),
            array(
                'col'   => 'E',
                'row'   => 26,
                'value' => 'blocks_returned'
            ),
            array(
                'col'   => 'E',
                'row'   => 27,
                'value' => 'basket_unload_total'
            ),
            array(
                'col'   => 'B',
                'row'   => 36,
                'value' => 'quote_number'
            ),
            array(
                'col'   => 'B',
                'row'   => 37,
                'value' => 'invoice_number'
            ),
            array(
                'col'   => 'E',
                'row'   => 36,
                'value' => 'po_number'
            ),
            array(
                'col'   => 'E',
                'row'   => 37,
                'value' => 'payee_name'
            ),
        ),


        'BreakoutChecklist' => array(
            array(
                'col'  => 'A',
                'row'  => 3,
                'value' => 'number_torque'
            ),
            array(
                'col'  => 'B',
                'row'  => 40,
                'value' => 'manager_name'
            ),
            array(
                'col'  => 'E',
                'row'  => 40,
                'value' => 'due_date',
                'date' => true
            ),
        ),

        'MakeupChecklist'   => array(
            array(
                'col'  => 'A',
                'row'  => 3,
                'value' => 'number_torque'
            ),
            array(
                'col'  => 'B',
                'row'  => 36,
                'value' => 'manager_name'
            ),
            array(
                'col'  => 'F',
                'row'  => 36,
                'value' => 'due_date',
                'date' => true
            ),
        ),


        'ManagerChecklist'  => array(
            array(
                'col'  => 'A',
                'row'  => 3,
                'value' => 'number_torque'
            ),
            array(
                'col'  => 'B',
                'row'  => 10,
                'value' => 'manager_name'
            ),
            array(
                'col'  => 'B',
                'row'  => 12,
                'value' => 'po_number'
            ),
            array(
                'col'  => 'B',
                'row'  => 30,
                'value' => 'payee_name'
            ),
            array(
                'col'  => 'E',
                'row'  => 30,
                'value' => 'due_date',
                'date' => true
            ),
        ),


        'CargoSummary'      => array(
            array(
                'col'  => 'I',
                'row'  => 5,
                'value' => 'due_date',
                'date' => true
            ),
            array(
                'col'  => 'E',
                'row'  => 10,
                'value' => 'rig_name'
            ),
            array(
                'col'  => 'A',
                'row'  => 44,
                'value' => 'manager_name'
            ),
            array(
                'col'  => 'D',
                'row'  => 44,
                'value' => 'manager_name'
            ),
            array (
                'col'  => 'E',
                'row'  => 15,
                'value' => 'basket_one_size'
            ),
            array (
                'col'  => 'G',
                'row'  => 15,
                'value' => 'basket_one_weight'
            ),
            array (
                'col'  => 'E',
                'row'  => 16,
                'value' => 'basket_two_size'
            ),
            array (
                'col'  => 'G',
                'row'  => 16,
                'value' => 'basket_two_weight'
            ),
            array (
                'col'  => 'E',
                'row'  => 17,
                'value' => 'basket_three_size'
            ),
            array (
                'col'  => 'G',
                'row'  => 17,
                'value' => 'basket_three_weight'
            ),
            array (
                'col'  => 'E',
                'row'  => 18,
                'value' => 'basket_four_size'
            ),
            array (
                'col'  => 'G',
                'row'  => 18,
                'value' => 'basket_four_weight'
            ),
            array (
                'col'  => 'E',
                'row'  => 19,
                'value' => 'basket_five_size'
            ),
            array (
                'col'  => 'G',
                'row'  => 19,
                'value' => 'basket_five_weight'
            ),
            array (
                'col'  => 'E',
                'row'  => 20,
                'value' => 'basket_six_size'
            ),
            array (
                'col'  => 'G',
                'row'  => 20,
                'value' => 'basket_six_weight'
            )
        ),


        'CommercialInvoice' => array(
            array(
                'col'  => 'A',
                'row'  => 20,
                'value' => 'cargo_export_type'
            ),
            array(
                'col'  => 'B',
                'row'  => 15,
                'value' => 'invoice_number'
            ),
            array(
                'col'  => 'B',
                'row'  => 16,
                'value' => 'customer_name'
            ),
            array(
                'col'  => 'B',
                'row'  => 17,
                'value' => 'number_torque'
            ),
            array(
                'col'  => 'B',
                'row'  => 18,
                'value' => 'due_date',
                'date' => true
            ),
            array(
                'col'  => 'F',
                'row'  => 16,
                'value' => 'vat_number'
            ),
            array(
                'col'  => 'F',
                'row'  => 17,
                'value' => 'eu_number'
            ),
            array(
                'col'  => 'F',
                'row'  => 18,
                'value' => 'po_number'
            ),
        )
    );

    public function movePdf($path, $number){
	    if(Storage::disk('local')->exists($path)){
		    $pdf = Storage::disk('local')->get($path);
		    $year = substr(str_replace('TTS', '', $number), 0, 2);
		    if(Storage::disk('titan')->put('/20' . $year . '/' . $number . '/' . basename($path), $pdf, 'public')){
			    return true;
		    }
	    }
	    return false;
    }

    public function updateDocument($name, $job_id)
    {
        $job = Job::where('id', '=', $job_id)->first();
        $locations = $this->cell_values[$name];
        $year = substr(str_replace('TTS', '', $job->number_torque), 0, 2);

        $file = $this->getDocument('/20'.$year.'/'.$job->number_torque.'/', $this->names[$name]);
        if(!$file){
	        return false;
        }
        $document = Excel::load($file, function ($file) use ($job, $locations) {
            $sheet = $file->getExcel()->getActiveSheet();
            foreach ($locations as $item) {
                $data = $job[$item['value']];

                if(isset($item['date'])){
                    $date = new Carbon($job[$item['value']]);
                    $data = $date->format('d/m/y');
                }

                $sheet->setCellValue($item['col'] . $item['row'], $data);
            }
        })->store('xlsx', storage_path('app/public/excel/exports', false, true));
        if ($this->storeDocument($job->number_torque, $document->filename . '.' . $document->ext)) {
            return true;
        }
        return false;
    }

    private function getDocument($path, $name)
    {
    	if(Storage::disk('titan')->exists($path . $name)) {
	        $document = Storage::disk('titan')->get($path . $name);
	    } else {
		    return false;
	    }
        $path = Storage::put('temp/' . $name, $document);
        return 'storage/app/public/temp/' . $name;
    }

    private function storeDocument($number, $name)
    {
        $file = Storage::disk('local')->get('/excel/exports/' . $name);
        $year = substr(str_replace('TTS', '', $number), 0, 2);
        $document = Storage::disk('titan')->put('/20' .$year . '/' . $number . '/' . $name, $file, 'public');
        if ($document) {
            return true;
        }
//        dd('failed to store file, path used: /20' .$year . '/' . $number . '/' . $name );
        return false;
    }

    public function createDocument($name, $job_id)
    {
        $job       = Job::where('id', '=', $job_id)->first();
        $locations = $this->cell_values[$name];

        $file     = $this->getDocument('/Templates/job/', $this->names[$name]);
        $document = Excel::load($file, function ($file) use ($job, $locations) {
            $sheet = $file->getExcel()->getActiveSheet();
            foreach ($locations as $item) {
                $data = $job[$item['value']];

                if(isset($item['date'])){
                    $date = new Carbon($job[$item['value']]);
                    $data = $date->format('d/m/y');
                }
                if (!($data==null||$data==="0")) {
                    $sheet->setCellValue($item['col'] . $item['row'], $data);
                }
            }
        })->store('xlsx', storage_path('app/public/excel/exports', false, true));
        if ($this->storeDocument($job->number_torque, $document->filename . '.' . $document->ext)) {
            return true;
        }
        return false;
    }
}
