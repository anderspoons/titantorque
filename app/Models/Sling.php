<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Sling
 * @package App\Models
 * @version October 13, 2016, 10:16 am UTC
 */
class Sling extends Model
{
    use SoftDeletes;

    public $table = 'sling';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'capacity_id',
        'bespoke',
        'quantity',
        'serial',
        'pdf_path'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'capacity_id' => 'integer',
        'quantity' => 'integer',
        'serial' => 'string',
        'pdf_path' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'serial' => 'unique:sling'
    ];

	public function capacity(){
		return $this->hasOne('App\Models\Capacity', 'id', 'capacity_id');
	}

    
}
