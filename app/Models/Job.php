<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Models\Options;
use Collective\Html\Eloquent\FormAccessible;

/**
 * Class Job
 * @package App\Models
 * @version October 14, 2016, 1:26 pm UTC
 */
class Job extends Model
{
    use SoftDeletes;
    use FormAccessible;

    public $table = 'job';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function nullIfBlank($field)
    {
        return trim($field) !== '' ? $field : null;
    }

    protected $dates = ['deleted_at'];

    public function getDates()
    {
        return ['created_at', 'updated_at', 'deleted_at', 'due_date', 'due_out', 'due_in'];
    }


    //Custom stuff for document generation
    protected $appends = array(
        'connections_in',
        'connections_out',
        'total_hours',
        'eu_number',
        'vat_number',
        'total_slings',
        'total_baskets',
        'total_connections',
        'customer_name',
        'rig_name',
        'well_name',
        'manager_name',
        'contact_name',
        'operator_name',
        'payee_name',
        'three_slings',
        'six_slings',
        'eight_slings',
        'ten_slings',
        'bespoke_slings',
        'small_basket_total',
        'small_basket_days',
        'large_basket_total',
        'large_basket_days',
        'protector_total',
        'protector_days',
        'basket_load_total',
        'basket_unload_total',
        'api_connections',
        'premium_connections',
        'cargo_export_type',
        'basket_one_size',
        'basket_one_weight',
        'basket_two_size',
        'basket_two_weight',
        'basket_three_size',
        'basket_three_weight',
        'basket_four_size',
        'basket_four_weight',
        'basket_five_size',
        'basket_five_weight',
    );


    public function getBasketOneSizeAttribute(){
        if($this->baskets->get(0)){
            return $this->baskets->get(0)->length;
        } else {
            return '';
        }
    }

    public function getBasketOneWeightAttribute(){
        if($this->baskets->get(0)){
            return $this->baskets->get(0)->max_gross;
        } else {
            return '';
        }
    }

    public function getBasketTwoSizeAttribute(){
        if($this->baskets->get(1)){
            return $this->baskets->get(1)->length;
        } else {
            return '';
        }
    }

    public function getBasketTwoWeightAttribute(){
        if($this->baskets->get(1)){
            return $this->baskets->get(1)->max_gross;
        } else {
            return '';
        }
    }

    public function getBasketThreeSizeAttribute(){
        if($this->baskets->get(2)){
            return $this->baskets->get(2)->length;
        } else {
            return '';
        }
    }

    public function getBasketThreeWeightAttribute(){
        if($this->baskets->get(2)){
            return $this->baskets->get(2)->max_gross;
        } else {
            return '';
        }
    }

    public function getBasketFourSizeAttribute(){
        if($this->baskets->get(3)){
            return $this->baskets->get(3)->length;
        } else {
            return '';
        }
    }

    public function getBasketFourWeightAttribute(){
        if($this->baskets->get(3)){
            return $this->baskets->get(3)->max_gross;
        } else {
            return '';
        }
    }

    public function getBasketFiveSizeAttribute(){
        if($this->baskets->get(4)){
            return $this->baskets->get(4)->length;
        } else {
            return '';
        }
    }

    public function getBasketFiveWeightAttribute(){
        if($this->baskets->get(4)){
        } else {
            return '';
        }
    }

    public function getApiConnectionsAttribute()
    {
        return $this->makes_api + $this->breaks_api;
    }

    public function getCargoExportTypeAttribute()
    {
        if ($this->cargo_export_temporary == 1) {
            return 'Temporary Export';
        }
        return 'Permanent Export';
    }

    public function getPremiumConnectionsAttribute()
    {
        return $this->makes_premium + $this->breaks_premium;
    }

    public function getTotalHoursAttribute()
    {
        return $this->hours_workshop + $this->hours_out_of;
    }

    public function getSmallBasketTotalAttribute()
    {
        $count = 0;
        foreach ($this->baskets as $basket) {
            if (intval($basket->length) < 46) {
                $count = $count + 1;
            }
        }
        return $count;
    }

    public function getSmallBasketDaysAttribute()
    {
        $count = 0;
        foreach ($this->baskets as $basket) {
            if (intval($basket->length) < 46) {
                $count = $count + $basket->pivot->days_out;
            }
        }
        return $count;
    }

    public function getLargeBasketTotalAttribute()
    {
        $count = 0;
        foreach ($this->baskets as $basket) {
            if (intval($basket->length) >= 46) {
                $count = $count + 1;
            }
        }
        return $count;
    }

    public function getLargeBasketDaysAttribute()
    {
        $count = 0;
        foreach ($this->baskets as $basket) {
            if (intval($basket->length) >= 46) {
                $count = $count + $basket->pivot->days_out;
            }
        }
        return $count;
    }

    public function getProtectorTotalAttribute()
    {
        $count = 0;
        foreach ($this->protectors as $protector) {
            $count = $count + 1;
        }
        return $count;
    }

    public function getProtectorDaysAttribute()
    {
        $count = 0;
        foreach ($this->protectors as $protector) {
            $count = $count + $protector->pivot->days_out;
        }
        return $count;
    }

    public function getBasketLoadTotalAttribute()
    {
        $count = 0;
        foreach ($this->baskets as $basket) {
            if ($basket->pivot->loaded == 1) {
                $count = $count + 1;
            }
        }
        return $count;
    }

    public function getBasketUnloadTotalAttribute()
    {
        $count = 0;
        foreach ($this->baskets as $basket) {
            if ($basket->pivot->unloaded == 1) {
                $count = $count + 1;
            }
        }
        return $count;
    }

    public function getTotalSlingsAttribute()
    {
        $count = 0;
        foreach ($this->slings as $sling) {
            $count = $count + $sling->pivot->quantity;
        }
        return $count;
    }

    public function getTotalBasketsAttribute()
    {
        return $this->baskets->count();
    }

    public function getTotalConnectionsAttribute()
    {
        $count = 0;
        $count = $count + $this->custom_connections;
        $count = $count + $this->stenciled_connections;
        $count = $count + $this->api_connections;
        $count = $count + $this->premium_connections;
        return $count;
    }

    public function getThreeSlingsAttribute()
    {
        $count = 0;
        foreach ($this->slings as $sling) {
            if ($sling->capacity->capacity == '3T') {
                $count = $count + $sling->pivot->quantity;
            }
        }
        return $count;
    }

    public function getSixSlingsAttribute()
    {
        $count = 0;
        foreach ($this->slings as $sling) {
            if ($sling->capacity->capacity == '6T') {
                $count = $count + $sling->pivot->quantity;
            }
        }
        return $count;
    }

    public function getEightSlingsAttribute()
    {
        $count = 0;
        foreach ($this->slings as $sling) {
            if ($sling->capacity->capacity == '8T') {
                $count = $count + $sling->pivot->quantity;
            }
        }
        return $count;
    }

    public function getTenSlingsAttribute()
    {
        $count = 0;
        foreach ($this->slings as $sling) {
            if ($sling->capacity->capacity == '10T') {
                $count = $count + $sling->pivot->quantity;
            }
        }
        return $count;
    }

    public function getBespokeSlingsAttribute()
    {
        $count = 0;
        foreach ($this->slings as $sling) {
            if ($sling->bespoke == '1') {
                $count = $count + $sling->pivot->quantity;
            }
        }
        return $count;
    }

    public function getEuNumberAttribute()
    {
        $option = Options::select('option_value')->where('option_name', 'EU_Number')->first();
        return $option->option_value;
    }

    public function getVatNumberAttribute()
    {
        $option = Options::select('option_value')->where('option_name', 'VAT_Number')->first();
        return $option->option_value;
    }

    public function getCustomerNameAttribute()
    {
	    if(isset($this->customer->name)){
		    return $this->customer->name;
	    }
	    return '';
    }

    public function getWellNameAttribute()
    {
	    if(isset($this->well->name)){
		    return $this->well->name;
	    }
	    return '';
    }

    public function getRigNameAttribute()
    {
	    if(isset($this->rig->name)){
		    return $this->rig->name;
	    }
	    return '';
    }

    public function getPayeeNameAttribute()
    {
	    if(isset($this->payee->name)){
		    return $this->payee->name;
	    }
	    return '';
    }

    public function getContactNameAttribute()
    {
    	if(isset($this->contact->name)){
		    return $this->contact->name;
	    }
	    return '';
    }

    public function getManagerNameAttribute()
    {
    	if(isset($this->manager->name)){
    		return $this->manager->name;
	    }
        return '';
    }

    public function getOperatorNameAttribute()
    {
	    if(isset($this->operator->name)){
		    return $this->operator->name;
	    }
	    return '';
    }


    public $fillable = [
        'number_torque',
        'number_tools',
        'assembly',
        'quote_number',
        'invoice_number',
        'value',
        'po_number',
        'email_contact',
        'cargo_summary',
        'cargo_export_temporary',
        'commercial_invoice',
        'ratchet_used',
        'ratchet_returned',
        'blocks_used',
        'blocks_returned',
        'basket_loaded',
        'basket_unloaded',
        'stenciled_connections',
        'drift_connections',
        'bakerlocked_connections',
        'centralizers_fitted',
        'customer_id',
        'contact_id',
        'operator_id',
        'manager_id',
        'rig_id',
        'inspector_id',
        'well_id',
        'payee_id',
        'due_date',
        'note',
        'custom_connections',
        'make_break_custom',
        'makes_api',
        'breaks_api',
        'makes_premium',
        'breaks_premium',
        'bhas',
        'hl_pennant',
        'insertion',
        'extraction',
        'witnesses',
        'folder_path',
        'hours_workshop',
        'hours_out_of',
        'checked_out_at',
        'checked_out_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'number_torque' => 'string',
        'number_tools' => 'string',
        'assembly' => 'string',
        'quote_number' => 'string',
        'invoice_number' => 'string',
        'value' => 'string',
        'po_number' => 'string',
        'ratchet_used' => 'integer',
        'ratchet_returned' => 'integer',
        'cargo_export_temporary' => 'integer',
        'cargo_summary' => 'integer',
        'blocks_used' => 'integer',
        'blocks_returned' => 'integer',
        'basket_loaded' => 'integer',
        'basket_unloaded' => 'integer',
        'stenciled_connections' => 'integer',
        'drift_connections' => 'integer',
        'bakerlocked_connections' => 'integer',
        'centralizers_fitted' => 'integer',
        'customer_id' => 'integer',
        'contact_id' => 'integer',
        'operator_id' => 'integer',
        'rig_id' => 'integer',
        'well_id' => 'integer',
        'inspector_id' => 'integer',
        'manager_id' => 'integer',
        'payee_id' => 'integer',
        'note' => 'string',
        'custom_connections' => 'string',
        'make_break_custom' => 'string',
        'makes_api' => 'integer',
        'breaks_api' => 'integer',
        'makes_premium' => 'integer',
        'breaks_premium' => 'integer',
        'bhas' => 'integer',
        'hl_pennant' => 'integer',
        'insertion' => 'integer',
        'extraction' => 'integer',
        'hours_workshop' => 'string',
        'hours_out_of' => 'string',
        'witnesses' => 'string',
        'folder_path' => 'string',
        'due_date' => 'timestamp',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


    public function customer()
    {
        return $this->hasOne('\App\Models\Customer', 'id', 'customer_id');
    }

    public function operator()
    {
        return $this->hasOne('\App\Models\Customer', 'id', 'customer_id');
    }

    public function rig()
    {
        return $this->hasOne('\App\Models\Rig', 'id', 'rig_id');
    }

    public function well()
    {
        return $this->hasOne('\App\Models\Well', 'id', 'well_id');
    }

    public function contact()
    {
        return $this->hasOne('\App\Models\Contact', 'id', 'contact_id');
    }

    public function statuses()
    {
        return $this->belongsToMany('App\Models\Status', 'job_status', 'job_id', 'status_id');
    }

    public function manager()
    {
        return $this->hasOne('App\User', 'id', 'manager_id');
    }

    public function inspector()
    {
        return $this->hasOne('App\Models\Inspector', 'id', 'inspector_id');
    }

    public function payee()
    {
        return $this->hasOne('App\Models\Customer', 'id', 'payee_id');
    }

    public function baskets()
    {
        return $this->belongsToMany('App\Models\Basket', 'job_basket', 'job_id', 'basket_id')->withPivot('loaded', 'unloaded', 'note', 'date_in', 'date_out', 'days_out');
    }

    public function slings()
    {
        return $this->belongsToMany('App\Models\Sling', 'job_sling', 'job_id', 'sling_id')->withPivot('quantity');
    }

    public function protectors()
    {
        return $this->belongsToMany('App\Models\Protector', 'job_protector', 'job_id', 'protector_id')->withPivot('date_in', 'date_out', 'days_out');
    }

    public function getDueDateAttribute($date)
    {
        $date = new Carbon($date);
        return $date->toDateString();
    }


//    public function getDueDateStringAttribute($date)
//    {
//        $date = new Carbon($date);
//        return $date->format('Y-m-d')->toString();
//    }

    public function getDateOutAttribute($date)
    {
        $date = new Carbon($date);
        return $date->format('Y-m-d');
    }

    public function getDateInAttribute($date)
    {
        $date = new Carbon($date);
        return $date->format('Y-m-d');
    }

    public function getCreatedAtAttribute($date)
    {
        $date = new Carbon($date);
        return $date->format('d/m/Y');
    }

    public function getConnectionsInAttribute()
    {
	    if($this->breaks_api > 0 || $this->breaks_premium > 0){
		    return $this->total_connections;
	    }
	    return 0;
    }

    public function getConnectionsOutAttribute()
    {
	    if($this->makes_api > 0 || $this->makes_premium > 0){
		    return $this->total_connections;
	    }
	    return 0;
    }

}
