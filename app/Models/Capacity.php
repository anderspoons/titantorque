<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Capacity extends Model
{
	use SoftDeletes;

	public $table = 'capacity';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	protected $dates = ['deleted_at'];


	public $fillable = [
		'capacity',
		'custom',
	];

	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'id' => 'integer',
		'capacity' => 'string',
		'bespoke' => 'integer',
	];

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public static $rules = [

	];

	public function slings(){
		return $this->hasMany('App\Models\Sling', 'capacity_id', 'id');
	}
}
