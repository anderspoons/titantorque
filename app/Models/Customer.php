<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Customer
 * @package App\Models
 * @version October 12, 2016, 4:29 pm UTC
 */
class Customer extends Model
{
    use SoftDeletes;

    public $table = 'customer';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'email',
        'phone',
        'operator'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'note' => 'string',
        'operator' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function setPhoneAttribute($data){
	    if(strlen($data) > 0){
	    	return $data;
	    } else {
	    	return ' ';
	    }
	}
    public function setEmailAttribute($data){
	    if(strlen($data) > 0){
	    	return $data;
	    } else {
	    	return ' ';
	    }
	}
}
