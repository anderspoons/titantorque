<?php

namespace App\Models;

use Eloquent as Model;
use MaddHatter\LaravelFullcalendar\Calendar;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model implements \MaddHatter\LaravelFullcalendar\Event
{
    use SoftDeletes;

    public $table = 'booking';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public function getDates()
    {
        return ['created_at', 'updated_at', 'deleted_at', 'date_start', 'date_end'];
    }

    public $fillable = [
        'customer_id',
        'machine_id',
        'date_start',
        'note',
        'date_end'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'machine_id' => 'integer',
        'note' => 'string',
        'date_start' => 'datetime',
        'date_end' => 'datetime',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'date_end' => 'date|after:date_start'
    ];

    public function customer(){
        return $this->hasOne('App\Models\Customer', 'id', 'customer_id');
    }

    public function machine(){
        return $this->hasOne('App\Models\Machine', 'id', 'machine_id');
    }

    /**
     * Get the event's id number
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the event's title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->customer->name;
    }

    /**
     * Is it an all day event?
     *
     * @return bool
     */
    public function isAllDay()
    {
        return false;
    }

    /**
     * Get the start time
     *
     * @return DateTime
     */
    public function getStart()
    {
        return $this->date_start;
    }

    /**
     * Get the end time
     *
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->date_end;
    }

    /**
     * Optional FullCalendar.io settings for this event
     *
     * @return array
     */
    public function getEventOptions()
    {
        return [
            'id' => $this->id,
            'color' => '#'.$this->machine->colour,
            'machine' => $this->machine->name,
            'note' => $this->note,
        ];
    }

    //Was using the below for the edit page but it fucked over the booking page so I have added these to the feilds view


//    public function getDateStartFormattedAttribute($date)
//    {
//        $date = new Carbon($date);
//        return $date->format('d/m/y H:i');
//    }
//
//    public function getDateEndFormattedAttribute($date)
//    {
//        $date = new Carbon($date);
//        return $date->format('d/m/y H:i');
//    }
//
//    public function getDateEndAttribute($date)
//    {
//        $date = new Carbon($date);
//        return $date->format('Y-m-d');
//    }
}
