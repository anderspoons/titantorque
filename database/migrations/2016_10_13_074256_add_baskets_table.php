<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBasketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('basket', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('length');
		    $table->string('asset_num');
		    $table->string('pdf_path');
		    $table->timestamp('cert_date');
		    $table->softDeletes();
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::drop('basket');
    }
}
