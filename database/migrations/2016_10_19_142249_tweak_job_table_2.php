<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TweakJobTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job', function($table){
            $table->integer('api_connections')->default(0);
            $table->integer('premium_connections')->default(0);
            $table->integer('bakerlocked_connections')->change()->default(0);
            $table->integer('stenciled_connections')->change()->default(0);
            $table->integer('basket_unloaded')->change()->default(0);
            $table->integer('basket_loaded')->change()->default(0);
            $table->integer('ratchet_used')->change()->default(0);
            $table->integer('ratchet_returned')->change()->default(0);
            $table->integer('blocks_used')->change()->default(0);
            $table->integer('blocks_returned')->change()->default(0);
            $table->integer('hours_out_of')->change()->default(0);
            $table->integer('hours_workshop')->change()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
