<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('job', function (Blueprint $table) {
		    //Auto generated unique fields
		    $table->increments('id');
		    $table->string('number_torque')->unique();

		    //Strings
		    $table->string('number_tools')->nullable();
		    $table->string('assembly')->nullable();
		    $table->string('quote_number')->nullable();
		    $table->string('invoice_number')->nullable();
		    $table->string('value')->nullable();
		    $table->string('po_number')->nullable();

		    //Booleans
		    $table->boolean('tools')->default(0);
		    $table->boolean('email_contact')->default(0);
		    $table->boolean('cargo_summary')->default(0);
		    $table->boolean('commercial_invoice')->default(0);

			//Links to other tables
		    $table->integer('ratchet_used')->nullable();
		    $table->integer('ratchet_returned')->nullable();

		    $table->integer('blocks_used')->nullable();
		    $table->integer('blocks_returned')->nullable();

		    $table->integer('basket_loaded')->nullable();
		    $table->integer('basket_unloaded')->nullable();

		    $table->integer('stenciled_connections')->nullable();
		    $table->integer('bakerlocked_connections')->nullable();
		    $table->integer('centralizers_fitted')->nullable();

			//Links to other tables
		    $table->integer('customer_id')->nullable();
		    $table->integer('contact_id')->nullable();
		    $table->integer('operator_id')->nullable();
		    $table->integer('rig_id')->nullable();
		    $table->integer('inspector_id')->nullable();
		    $table->integer('payee_id')->nullable();

		    //Timestamp or date feilds
		    $table->timestamp('due_date');
		    $table->timestamps();
		    $table->softDeletes();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::drop('job');
    }
}
