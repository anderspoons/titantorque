<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReturnedJobProtectors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_protector', function($table){
            $table->integer('days_out')->defaul(0);
            $table->timestamp('date_out')->nullable();
            $table->timestamp('date_in')->nullable();
        });

        Schema::table('job', function($table){
            $table->string('witnesses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
