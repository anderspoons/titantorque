<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyJobTableAddCustom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('job', function (Blueprint $table) {
		    $table->string('custom_connections');
		    $table->string('basket_note')->nullable();
		    $table->string('make_break_custom')->nullable();
		    $table->integer('makes')->default(0);
		    $table->integer('breaks')->default(0);
		    $table->integer('bhas')->default(0);
		    $table->integer('insertion')->default(0);
		    $table->integer('extraction')->default(0);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('job', function (Blueprint $table) {
		    $table->dropColumn('custom_connections');
		    $table->dropColumn('basket_note');
		    $table->dropColumn('make_break_custom');
		    $table->dropColumn('makes');
		    $table->dropColumn('breaks');
		    $table->dropColumn('bhas');
		    $table->dropColumn('insertion');
		    $table->dropColumn('extraction');
	    });
    }
}
