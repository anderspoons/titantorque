<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSlingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('sling', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('capacity_id');
		    $table->boolean('bespoke');
		    $table->integer('quantity');
		    $table->string('serial');
		    $table->string('pdf_path');
		    $table->softDeletes();
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::drop('sling');
    }
}
