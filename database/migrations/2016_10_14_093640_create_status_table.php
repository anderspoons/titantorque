<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	public function up()
	{
		Schema::create('status', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->boolean('job')->default(0);
			$table->boolean('basket')->default(0);
			$table->boolean('sling')->default(0);
			$table->boolean('protector')->default(0);
			$table->boolean('automatic')->default(0);
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('status');
	}
}
