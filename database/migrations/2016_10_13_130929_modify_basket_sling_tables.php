<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyBasketSlingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
	    Schema::table('basket', function (Blueprint $table) {
		    $table->string('asset_num')->change()->unique();
	    });
	    Schema::table('sling', function (Blueprint $table) {
	    	$table->string('serial')->change()->unique();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
