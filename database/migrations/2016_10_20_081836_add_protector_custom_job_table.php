<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProtectorCustomJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job', function($table){
            $table->string('protector_custom')->nullable();
            $table->string('lift_subs')->nullable();
            $table->string('basket_custom')->nullable();
            $table->string('bhas_custom')->nullable();
            $table->boolean('imported')->default(0);
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
