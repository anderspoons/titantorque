<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtrasJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job', function($table){
            $table->integer('manager_id');
            $table->integer('hours_workshop');
            $table->integer('hours_out_of');
            $table->dropColumn('basket_note');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job', function($table){
            $table->dropColumn('manager_id');
            $table->dropColumn('hours_workshop');
            $table->dropColumn('hours_out_of');
            $table->dropColumn('cargo_summary');
            $table->dropColumn('commercial_invoice');
        });
    }
}
